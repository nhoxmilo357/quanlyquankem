USE [master]
GO
/****** Object:  Database [DACNPM]    Script Date: 1/16/2018 04:30:43 PM ******/
CREATE DATABASE [DACNPM]
 CONTAINMENT = NONE
 ON  PRIMARY 
( NAME = N'DACNPM', FILENAME = N'C:\Program Files\Microsoft SQL Server\MSSQL11.SQLEXPRESS\MSSQL\DATA\DACNPM.mdf' , SIZE = 4096KB , MAXSIZE = UNLIMITED, FILEGROWTH = 1024KB )
 LOG ON 
( NAME = N'DACNPM_log', FILENAME = N'C:\Program Files\Microsoft SQL Server\MSSQL11.SQLEXPRESS\MSSQL\DATA\DACNPM_log.ldf' , SIZE = 1024KB , MAXSIZE = 2048GB , FILEGROWTH = 10%)
GO
ALTER DATABASE [DACNPM] SET COMPATIBILITY_LEVEL = 110
GO
IF (1 = FULLTEXTSERVICEPROPERTY('IsFullTextInstalled'))
begin
EXEC [DACNPM].[dbo].[sp_fulltext_database] @action = 'enable'
end
GO
ALTER DATABASE [DACNPM] SET ANSI_NULL_DEFAULT OFF 
GO
ALTER DATABASE [DACNPM] SET ANSI_NULLS OFF 
GO
ALTER DATABASE [DACNPM] SET ANSI_PADDING OFF 
GO
ALTER DATABASE [DACNPM] SET ANSI_WARNINGS OFF 
GO
ALTER DATABASE [DACNPM] SET ARITHABORT OFF 
GO
ALTER DATABASE [DACNPM] SET AUTO_CLOSE OFF 
GO
ALTER DATABASE [DACNPM] SET AUTO_CREATE_STATISTICS ON 
GO
ALTER DATABASE [DACNPM] SET AUTO_SHRINK OFF 
GO
ALTER DATABASE [DACNPM] SET AUTO_UPDATE_STATISTICS ON 
GO
ALTER DATABASE [DACNPM] SET CURSOR_CLOSE_ON_COMMIT OFF 
GO
ALTER DATABASE [DACNPM] SET CURSOR_DEFAULT  GLOBAL 
GO
ALTER DATABASE [DACNPM] SET CONCAT_NULL_YIELDS_NULL OFF 
GO
ALTER DATABASE [DACNPM] SET NUMERIC_ROUNDABORT OFF 
GO
ALTER DATABASE [DACNPM] SET QUOTED_IDENTIFIER OFF 
GO
ALTER DATABASE [DACNPM] SET RECURSIVE_TRIGGERS OFF 
GO
ALTER DATABASE [DACNPM] SET  DISABLE_BROKER 
GO
ALTER DATABASE [DACNPM] SET AUTO_UPDATE_STATISTICS_ASYNC OFF 
GO
ALTER DATABASE [DACNPM] SET DATE_CORRELATION_OPTIMIZATION OFF 
GO
ALTER DATABASE [DACNPM] SET TRUSTWORTHY OFF 
GO
ALTER DATABASE [DACNPM] SET ALLOW_SNAPSHOT_ISOLATION OFF 
GO
ALTER DATABASE [DACNPM] SET PARAMETERIZATION SIMPLE 
GO
ALTER DATABASE [DACNPM] SET READ_COMMITTED_SNAPSHOT OFF 
GO
ALTER DATABASE [DACNPM] SET HONOR_BROKER_PRIORITY OFF 
GO
ALTER DATABASE [DACNPM] SET RECOVERY SIMPLE 
GO
ALTER DATABASE [DACNPM] SET  MULTI_USER 
GO
ALTER DATABASE [DACNPM] SET PAGE_VERIFY CHECKSUM  
GO
ALTER DATABASE [DACNPM] SET DB_CHAINING OFF 
GO
ALTER DATABASE [DACNPM] SET FILESTREAM( NON_TRANSACTED_ACCESS = OFF ) 
GO
ALTER DATABASE [DACNPM] SET TARGET_RECOVERY_TIME = 0 SECONDS 
GO
USE [DACNPM]
GO
/****** Object:  Table [dbo].[BAN]    Script Date: 1/16/2018 04:30:44 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[BAN](
	[MaBan] [tinyint] NOT NULL,
	[TenBan] [nvarchar](7) NOT NULL CONSTRAINT [DF_BAN_TenBan]  DEFAULT (N'Trống'),
	[TrangThai] [nvarchar](10) NOT NULL CONSTRAINT [DF_BAN_TrangThai]  DEFAULT (N'Trống'),
	[Phut] [varchar](2) NULL,
 CONSTRAINT [PK_BAN] PRIMARY KEY CLUSTERED 
(
	[MaBan] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[ChiTietHoaDon]    Script Date: 1/16/2018 04:30:45 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[ChiTietHoaDon](
	[MaHD] [tinyint] NOT NULL,
	[TenBan] [nvarchar](7) NOT NULL,
	[MaMH] [tinyint] NOT NULL,
	[TenMH] [nvarchar](50) NOT NULL,
	[SoLuong] [tinyint] NOT NULL,
	[Gia] [float] NOT NULL,
	[ThanhTien] [float] NOT NULL,
	[Uudai] [varchar](3) NOT NULL,
	[TongTien] [float] NOT NULL,
	[Khachtra] [float] NOT NULL,
	[Tienthua] [float] NOT NULL,
	[TenNV] [nvarchar](40) NOT NULL,
	[Ngaylap] [smalldatetime] NOT NULL
) ON [PRIMARY]

GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[DICHVU]    Script Date: 1/16/2018 04:30:45 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[DICHVU](
	[TenDV] [nvarchar](10) NOT NULL,
	[NgayBD] [smalldatetime] NULL,
	[NgayKT] [smalldatetime] NULL,
	[UuDai] [tinyint] NOT NULL,
 CONSTRAINT [PK_DICHVU] PRIMARY KEY CLUSTERED 
(
	[TenDV] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
/****** Object:  Table [dbo].[DOANHTHUTHEOMHANG]    Script Date: 1/16/2018 04:30:45 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[DOANHTHUTHEOMHANG](
	[STT] [tinyint] IDENTITY(1,1) NOT NULL,
	[MaMH] [tinyint] NULL,
	[TenMH] [nvarchar](50) NULL,
	[SLBAN] [tinyint] NULL,
	[DonGiaTB] [float] NULL,
	[Loai] [nvarchar](20) NULL,
 CONSTRAINT [PK_DOANHTHUTHEOMHANG] PRIMARY KEY CLUSTERED 
(
	[STT] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
/****** Object:  Table [dbo].[HOADON]    Script Date: 1/16/2018 04:30:45 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[HOADON](
	[MaHD] [tinyint] IDENTITY(1,1) NOT NULL,
	[MaBan] [tinyint] NULL,
	[NgayLap] [smalldatetime] NULL,
	[MaNV] [varchar](10) NULL,
 CONSTRAINT [PK_HOADON] PRIMARY KEY CLUSTERED 
(
	[MaHD] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[LoaiMonAn]    Script Date: 1/16/2018 04:30:45 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[LoaiMonAn](
	[MaLoai] [tinyint] IDENTITY(1,1) NOT NULL,
	[TenLoai] [nvarchar](20) NOT NULL,
 CONSTRAINT [PK__LoaiMonA__730A5759931AF968] PRIMARY KEY CLUSTERED 
(
	[MaLoai] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
/****** Object:  Table [dbo].[MonAn]    Script Date: 1/16/2018 04:30:45 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[MonAn](
	[MaMH] [tinyint] IDENTITY(1,1) NOT NULL,
	[TenMH] [nvarchar](30) NOT NULL,
	[DonGia] [int] NOT NULL,
	[MaLoai] [tinyint] NOT NULL,
 CONSTRAINT [PK_MonAn_1] PRIMARY KEY CLUSTERED 
(
	[MaMH] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
/****** Object:  Table [dbo].[NGUOIDUNG]    Script Date: 1/16/2018 04:30:45 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[NGUOIDUNG](
	[Username] [varchar](10) NOT NULL,
	[Pass] [varchar](10) NOT NULL,
	[MaNV] [varchar](10) NOT NULL,
	[Mavaitro] [tinyint] NULL,
 CONSTRAINT [PK_NGUOIDUNG_1] PRIMARY KEY CLUSTERED 
(
	[Username] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[NHANVIEN]    Script Date: 1/16/2018 04:30:45 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[NHANVIEN](
	[MaNV] [varchar](10) NOT NULL,
	[TenNV] [nvarchar](40) NOT NULL,
	[GioiTinh] [nvarchar](3) NOT NULL,
	[NgaySinh] [smalldatetime] NULL,
	[CMND] [char](9) NULL,
	[SDT] [char](11) NOT NULL,
	[DiaChi] [nvarchar](100) NULL,
 CONSTRAINT [PK_NHANVIEN] PRIMARY KEY CLUSTERED 
(
	[MaNV] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[THEKH]    Script Date: 1/16/2018 04:30:45 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[THEKH](
	[MaThe] [smallint] IDENTITY(1,1) NOT NULL,
	[TenKH] [nvarchar](40) NOT NULL,
	[SDT] [char](11) NULL,
	[Ngaylapthe] [smalldatetime] NULL,
	[TenDV] [nvarchar](10) NOT NULL CONSTRAINT [DF_THEKH_TenDV]  DEFAULT (N'Thẻ KH'),
 CONSTRAINT [PK_THEKH] PRIMARY KEY CLUSTERED 
(
	[MaThe] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[VaiTro]    Script Date: 1/16/2018 04:30:45 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[VaiTro](
	[Mavaitro] [tinyint] NOT NULL,
	[Vaitro] [nvarchar](10) NOT NULL,
 CONSTRAINT [PK_VaiTro] PRIMARY KEY CLUSTERED 
(
	[Mavaitro] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
INSERT [dbo].[BAN] ([MaBan], [TenBan], [TrangThai], [Phut]) VALUES (13, N'Bàn 1', N'Trống', N'1')
INSERT [dbo].[BAN] ([MaBan], [TenBan], [TrangThai], [Phut]) VALUES (14, N'Bàn 2', N'Trống', N'1')
INSERT [dbo].[BAN] ([MaBan], [TenBan], [TrangThai], [Phut]) VALUES (15, N'Bàn 3', N'Đang dùng', N'1')
INSERT [dbo].[BAN] ([MaBan], [TenBan], [TrangThai], [Phut]) VALUES (16, N'Bàn 4', N'Trống', N'1')
INSERT [dbo].[BAN] ([MaBan], [TenBan], [TrangThai], [Phut]) VALUES (17, N'Bàn 5', N'Trống', N'1')
INSERT [dbo].[BAN] ([MaBan], [TenBan], [TrangThai], [Phut]) VALUES (18, N'Bàn 6', N'Trống', N'1')
INSERT [dbo].[BAN] ([MaBan], [TenBan], [TrangThai], [Phut]) VALUES (19, N'Bàn 7', N'Trống', N'1')
INSERT [dbo].[BAN] ([MaBan], [TenBan], [TrangThai], [Phut]) VALUES (20, N'Bàn 8', N'Trống', N'1')
INSERT [dbo].[BAN] ([MaBan], [TenBan], [TrangThai], [Phut]) VALUES (21, N'Bàn 9', N'Trống', N'1')
INSERT [dbo].[BAN] ([MaBan], [TenBan], [TrangThai], [Phut]) VALUES (22, N'Bàn 10', N'Trống', N'1')
INSERT [dbo].[BAN] ([MaBan], [TenBan], [TrangThai], [Phut]) VALUES (23, N'Bàn 11', N'Trống', N'1')
INSERT [dbo].[BAN] ([MaBan], [TenBan], [TrangThai], [Phut]) VALUES (24, N'Bàn 12', N'Trống', N'1')
INSERT [dbo].[ChiTietHoaDon] ([MaHD], [TenBan], [MaMH], [TenMH], [SoLuong], [Gia], [ThanhTien], [Uudai], [TongTien], [Khachtra], [Tienthua], [TenNV], [Ngaylap]) VALUES (73, N'Bàn 3', 32, N'Combo Đảo kem', 1, 100000, 238000, N'0%', 238000, 300000, 62000, N'Kiều Trinh', CAST(N'2018-01-15 13:00:00' AS SmallDateTime))
INSERT [dbo].[ChiTietHoaDon] ([MaHD], [TenBan], [MaMH], [TenMH], [SoLuong], [Gia], [ThanhTien], [Uudai], [TongTien], [Khachtra], [Tienthua], [TenNV], [Ngaylap]) VALUES (73, N'Bàn 3', 8, N'Kem dâu', 1, 8000, 238000, N'0%', 238000, 300000, 62000, N'Kiều Trinh', CAST(N'2018-01-15 13:00:00' AS SmallDateTime))
INSERT [dbo].[ChiTietHoaDon] ([MaHD], [TenBan], [MaMH], [TenMH], [SoLuong], [Gia], [ThanhTien], [Uudai], [TongTien], [Khachtra], [Tienthua], [TenNV], [Ngaylap]) VALUES (73, N'Bàn 3', 40, N'Bingsu Chè Đậu Đỏ', 1, 80000, 238000, N'0%', 238000, 300000, 62000, N'Kiều Trinh', CAST(N'2018-01-15 13:00:00' AS SmallDateTime))
INSERT [dbo].[ChiTietHoaDon] ([MaHD], [TenBan], [MaMH], [TenMH], [SoLuong], [Gia], [ThanhTien], [Uudai], [TongTien], [Khachtra], [Tienthua], [TenNV], [Ngaylap]) VALUES (73, N'Bàn 3', 29, N'Combo T-S-Dâu', 1, 50000, 238000, N'0%', 238000, 300000, 62000, N'Kiều Trinh', CAST(N'2018-01-15 13:00:00' AS SmallDateTime))
INSERT [dbo].[ChiTietHoaDon] ([MaHD], [TenBan], [MaMH], [TenMH], [SoLuong], [Gia], [ThanhTien], [Uudai], [TongTien], [Khachtra], [Tienthua], [TenNV], [Ngaylap]) VALUES (74, N'Bàn 3', 37, N'Bingsu Xoài', 1, 80000, 158000, N'0%', 158000, 160000, 2000, N'Kiều Trinh', CAST(N'2018-01-16 16:21:00' AS SmallDateTime))
INSERT [dbo].[ChiTietHoaDon] ([MaHD], [TenBan], [MaMH], [TenMH], [SoLuong], [Gia], [ThanhTien], [Uudai], [TongTien], [Khachtra], [Tienthua], [TenNV], [Ngaylap]) VALUES (74, N'Bàn 3', 10, N'Kem việt quất', 1, 8000, 158000, N'0%', 158000, 160000, 2000, N'Kiều Trinh', CAST(N'2018-01-16 16:21:00' AS SmallDateTime))
INSERT [dbo].[ChiTietHoaDon] ([MaHD], [TenBan], [MaMH], [TenMH], [SoLuong], [Gia], [ThanhTien], [Uudai], [TongTien], [Khachtra], [Tienthua], [TenNV], [Ngaylap]) VALUES (74, N'Bàn 3', 28, N'Combo thất vị hương', 1, 70000, 158000, N'0%', 158000, 160000, 2000, N'Kiều Trinh', CAST(N'2018-01-16 16:21:00' AS SmallDateTime))
INSERT [dbo].[DICHVU] ([TenDV], [NgayBD], [NgayKT], [UuDai]) VALUES (N'Lễ tình nh', CAST(N'2017-02-14 00:00:00' AS SmallDateTime), NULL, 5)
INSERT [dbo].[DICHVU] ([TenDV], [NgayBD], [NgayKT], [UuDai]) VALUES (N'Noel', CAST(N'2017-12-24 00:00:00' AS SmallDateTime), CAST(N'2017-12-25 00:00:00' AS SmallDateTime), 10)
INSERT [dbo].[DICHVU] ([TenDV], [NgayBD], [NgayKT], [UuDai]) VALUES (N'Quốc tế th', CAST(N'2017-06-01 00:00:00' AS SmallDateTime), NULL, 7)
INSERT [dbo].[DICHVU] ([TenDV], [NgayBD], [NgayKT], [UuDai]) VALUES (N'Thẻ KH', CAST(N'2017-12-23 00:00:00' AS SmallDateTime), NULL, 10)
SET IDENTITY_INSERT [dbo].[DOANHTHUTHEOMHANG] ON 

INSERT [dbo].[DOANHTHUTHEOMHANG] ([STT], [MaMH], [TenMH], [SLBAN], [DonGiaTB], [Loai]) VALUES (231, 32, N'Combo Đảo kem', 1, 100000, N'Kem Combo')
INSERT [dbo].[DOANHTHUTHEOMHANG] ([STT], [MaMH], [TenMH], [SLBAN], [DonGiaTB], [Loai]) VALUES (232, 8, N'Kem dâu', 1, 8000, N'Kem cây')
INSERT [dbo].[DOANHTHUTHEOMHANG] ([STT], [MaMH], [TenMH], [SLBAN], [DonGiaTB], [Loai]) VALUES (233, 40, N'Bingsu Chè Đậu Đỏ', 1, 80000, N'Bingsu')
INSERT [dbo].[DOANHTHUTHEOMHANG] ([STT], [MaMH], [TenMH], [SLBAN], [DonGiaTB], [Loai]) VALUES (234, 29, N'Combo T-S-Dâu', 1, 50000, N'Kem Combo')
INSERT [dbo].[DOANHTHUTHEOMHANG] ([STT], [MaMH], [TenMH], [SLBAN], [DonGiaTB], [Loai]) VALUES (235, 37, N'Bingsu Xoài', 1, 80000, N'Bingsu')
INSERT [dbo].[DOANHTHUTHEOMHANG] ([STT], [MaMH], [TenMH], [SLBAN], [DonGiaTB], [Loai]) VALUES (236, 10, N'Kem việt quất', 1, 8000, N'Kem cây')
INSERT [dbo].[DOANHTHUTHEOMHANG] ([STT], [MaMH], [TenMH], [SLBAN], [DonGiaTB], [Loai]) VALUES (237, 28, N'Combo thất vị hương', 1, 70000, N'Kem Combo')
SET IDENTITY_INSERT [dbo].[DOANHTHUTHEOMHANG] OFF
SET IDENTITY_INSERT [dbo].[HOADON] ON 

INSERT [dbo].[HOADON] ([MaHD], [MaBan], [NgayLap], [MaNV]) VALUES (73, 15, CAST(N'2018-01-15 13:00:00' AS SmallDateTime), N'TN1')
INSERT [dbo].[HOADON] ([MaHD], [MaBan], [NgayLap], [MaNV]) VALUES (74, 15, CAST(N'2018-01-16 16:21:00' AS SmallDateTime), N'TN1')
SET IDENTITY_INSERT [dbo].[HOADON] OFF
SET IDENTITY_INSERT [dbo].[LoaiMonAn] ON 

INSERT [dbo].[LoaiMonAn] ([MaLoai], [TenLoai]) VALUES (1, N'Kem cây')
INSERT [dbo].[LoaiMonAn] ([MaLoai], [TenLoai]) VALUES (2, N'Kem ly - Kem dĩa')
INSERT [dbo].[LoaiMonAn] ([MaLoai], [TenLoai]) VALUES (3, N'Kem Combo')
INSERT [dbo].[LoaiMonAn] ([MaLoai], [TenLoai]) VALUES (4, N'Bingsu')
SET IDENTITY_INSERT [dbo].[LoaiMonAn] OFF
SET IDENTITY_INSERT [dbo].[MonAn] ON 

INSERT [dbo].[MonAn] ([MaMH], [TenMH], [DonGia], [MaLoai]) VALUES (1, N'Kem tươi Vani', 5000, 1)
INSERT [dbo].[MonAn] ([MaMH], [TenMH], [DonGia], [MaLoai]) VALUES (4, N'Kem tươi Vani-Chocolate', 7000, 1)
INSERT [dbo].[MonAn] ([MaMH], [TenMH], [DonGia], [MaLoai]) VALUES (5, N'Kem tươi Dâu-Vani', 7000, 1)
INSERT [dbo].[MonAn] ([MaMH], [TenMH], [DonGia], [MaLoai]) VALUES (6, N'Kem tươi Cam', 5000, 1)
INSERT [dbo].[MonAn] ([MaMH], [TenMH], [DonGia], [MaLoai]) VALUES (7, N'Kem tươi Cam-Trà Xanh', 7000, 1)
INSERT [dbo].[MonAn] ([MaMH], [TenMH], [DonGia], [MaLoai]) VALUES (8, N'Kem dâu', 8000, 1)
INSERT [dbo].[MonAn] ([MaMH], [TenMH], [DonGia], [MaLoai]) VALUES (9, N'Kem 3 màu', 8000, 1)
INSERT [dbo].[MonAn] ([MaMH], [TenMH], [DonGia], [MaLoai]) VALUES (10, N'Kem việt quất', 8000, 1)
INSERT [dbo].[MonAn] ([MaMH], [TenMH], [DonGia], [MaLoai]) VALUES (11, N'Kem cam', 8000, 1)
INSERT [dbo].[MonAn] ([MaMH], [TenMH], [DonGia], [MaLoai]) VALUES (12, N'Kem kiwi', 8000, 1)
INSERT [dbo].[MonAn] ([MaMH], [TenMH], [DonGia], [MaLoai]) VALUES (13, N'Ly Chocolate', 10000, 2)
INSERT [dbo].[MonAn] ([MaMH], [TenMH], [DonGia], [MaLoai]) VALUES (14, N'Ly Dâu', 10000, 2)
INSERT [dbo].[MonAn] ([MaMH], [TenMH], [DonGia], [MaLoai]) VALUES (15, N'Ly Việt quất', 10000, 2)
INSERT [dbo].[MonAn] ([MaMH], [TenMH], [DonGia], [MaLoai]) VALUES (16, N'Ly Trái cây', 15000, 2)
INSERT [dbo].[MonAn] ([MaMH], [TenMH], [DonGia], [MaLoai]) VALUES (17, N'Ly Táo', 10000, 2)
INSERT [dbo].[MonAn] ([MaMH], [TenMH], [DonGia], [MaLoai]) VALUES (19, N'Dĩa Bánh Chuối', 30000, 2)
INSERT [dbo].[MonAn] ([MaMH], [TenMH], [DonGia], [MaLoai]) VALUES (20, N'Kem dừa', 20000, 2)
INSERT [dbo].[MonAn] ([MaMH], [TenMH], [DonGia], [MaLoai]) VALUES (21, N'Dĩa Bánh Dâu', 30000, 2)
INSERT [dbo].[MonAn] ([MaMH], [TenMH], [DonGia], [MaLoai]) VALUES (22, N'Kem dừa orio', 30000, 2)
INSERT [dbo].[MonAn] ([MaMH], [TenMH], [DonGia], [MaLoai]) VALUES (23, N'Kem 4 viên', 20000, 2)
INSERT [dbo].[MonAn] ([MaMH], [TenMH], [DonGia], [MaLoai]) VALUES (24, N'Kem phi thuyền', 30000, 2)
INSERT [dbo].[MonAn] ([MaMH], [TenMH], [DonGia], [MaLoai]) VALUES (25, N'Ly trà xanh', 10000, 2)
INSERT [dbo].[MonAn] ([MaMH], [TenMH], [DonGia], [MaLoai]) VALUES (26, N'Ly xoài', 10000, 2)
INSERT [dbo].[MonAn] ([MaMH], [TenMH], [DonGia], [MaLoai]) VALUES (27, N'Combo S-V-Đậu phộng', 50000, 3)
INSERT [dbo].[MonAn] ([MaMH], [TenMH], [DonGia], [MaLoai]) VALUES (28, N'Combo thất vị hương', 70000, 3)
INSERT [dbo].[MonAn] ([MaMH], [TenMH], [DonGia], [MaLoai]) VALUES (29, N'Combo T-S-Dâu', 50000, 3)
INSERT [dbo].[MonAn] ([MaMH], [TenMH], [DonGia], [MaLoai]) VALUES (30, N'Combo S-V-Dâu', 50000, 3)
INSERT [dbo].[MonAn] ([MaMH], [TenMH], [DonGia], [MaLoai]) VALUES (31, N'Combo ngũ vị hương', 60000, 3)
INSERT [dbo].[MonAn] ([MaMH], [TenMH], [DonGia], [MaLoai]) VALUES (32, N'Combo Đảo kem', 100000, 3)
INSERT [dbo].[MonAn] ([MaMH], [TenMH], [DonGia], [MaLoai]) VALUES (33, N'Bingsu Dâu', 90000, 4)
INSERT [dbo].[MonAn] ([MaMH], [TenMH], [DonGia], [MaLoai]) VALUES (34, N'Bingsu Việt Quất', 100000, 4)
INSERT [dbo].[MonAn] ([MaMH], [TenMH], [DonGia], [MaLoai]) VALUES (35, N'Bingsu Sầu Riêng', 90000, 4)
INSERT [dbo].[MonAn] ([MaMH], [TenMH], [DonGia], [MaLoai]) VALUES (36, N'Bingsu Dưa Gang', 80000, 4)
INSERT [dbo].[MonAn] ([MaMH], [TenMH], [DonGia], [MaLoai]) VALUES (37, N'Bingsu Xoài', 80000, 4)
INSERT [dbo].[MonAn] ([MaMH], [TenMH], [DonGia], [MaLoai]) VALUES (38, N'Bingsu Chuối', 80000, 4)
INSERT [dbo].[MonAn] ([MaMH], [TenMH], [DonGia], [MaLoai]) VALUES (39, N'Bingsu Matcha', 850000, 4)
INSERT [dbo].[MonAn] ([MaMH], [TenMH], [DonGia], [MaLoai]) VALUES (40, N'Bingsu Chè Đậu Đỏ', 80000, 4)
SET IDENTITY_INSERT [dbo].[MonAn] OFF
INSERT [dbo].[NGUOIDUNG] ([Username], [Pass], [MaNV], [Mavaitro]) VALUES (N'quanly', N'000', N'QL1', 2)
INSERT [dbo].[NGUOIDUNG] ([Username], [Pass], [MaNV], [Mavaitro]) VALUES (N'thungan', N'111', N'TN1', 1)
INSERT [dbo].[NHANVIEN] ([MaNV], [TenNV], [GioiTinh], [NgaySinh], [CMND], [SDT], [DiaChi]) VALUES (N'PV1', N'Trương Nhung', N'Nữ', CAST(N'2018-01-16 16:23:00' AS SmallDateTime), N'326157963', N'0168546987 ', N'33 Phan Đăng Lưu,q.Bình Thạnh')
INSERT [dbo].[NHANVIEN] ([MaNV], [TenNV], [GioiTinh], [NgaySinh], [CMND], [SDT], [DiaChi]) VALUES (N'QL1', N'Điền Quân', N'Nam', CAST(N'2017-01-02 00:00:00' AS SmallDateTime), N'3        ', N'2          ', N'Siêu thị E-mart')
INSERT [dbo].[NHANVIEN] ([MaNV], [TenNV], [GioiTinh], [NgaySinh], [CMND], [SDT], [DiaChi]) VALUES (N'TN1', N'Kiều Trinh', N'Nữ', CAST(N'1992-01-15 12:43:00' AS SmallDateTime), N'236879541', N'0165879356 ', N'334/72 Lê Đại Hành,quận 10,tp.HCM')
SET IDENTITY_INSERT [dbo].[THEKH] ON 

INSERT [dbo].[THEKH] ([MaThe], [TenKH], [SDT], [Ngaylapthe], [TenDV]) VALUES (22, N'Trần Hoàng', N'0908523696 ', CAST(N'2018-01-15 13:04:00' AS SmallDateTime), N'Thẻ KH')
SET IDENTITY_INSERT [dbo].[THEKH] OFF
INSERT [dbo].[VaiTro] ([Mavaitro], [Vaitro]) VALUES (1, N'Thu ngân')
INSERT [dbo].[VaiTro] ([Mavaitro], [Vaitro]) VALUES (2, N'Quản lý')
INSERT [dbo].[VaiTro] ([Mavaitro], [Vaitro]) VALUES (3, N'Phục vụ')
INSERT [dbo].[VaiTro] ([Mavaitro], [Vaitro]) VALUES (4, N'Giữ xe')
/****** Object:  Index [IX_HOADON]    Script Date: 1/16/2018 04:30:45 PM ******/
CREATE NONCLUSTERED INDEX [IX_HOADON] ON [dbo].[HOADON]
(
	[MaBan] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, SORT_IN_TEMPDB = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
GO
ALTER TABLE [dbo].[ChiTietHoaDon]  WITH CHECK ADD  CONSTRAINT [FK_ChiTietHoaDon_HOADON] FOREIGN KEY([MaHD])
REFERENCES [dbo].[HOADON] ([MaHD])
ON UPDATE CASCADE
GO
ALTER TABLE [dbo].[ChiTietHoaDon] CHECK CONSTRAINT [FK_ChiTietHoaDon_HOADON]
GO
ALTER TABLE [dbo].[ChiTietHoaDon]  WITH CHECK ADD  CONSTRAINT [FK_ChiTietHoaDon_MonAn] FOREIGN KEY([MaMH])
REFERENCES [dbo].[MonAn] ([MaMH])
ON UPDATE CASCADE
GO
ALTER TABLE [dbo].[ChiTietHoaDon] CHECK CONSTRAINT [FK_ChiTietHoaDon_MonAn]
GO
ALTER TABLE [dbo].[DOANHTHUTHEOMHANG]  WITH CHECK ADD  CONSTRAINT [FK_DOANHTHUTHEOMHANG_MonAn] FOREIGN KEY([MaMH])
REFERENCES [dbo].[MonAn] ([MaMH])
ON UPDATE CASCADE
GO
ALTER TABLE [dbo].[DOANHTHUTHEOMHANG] CHECK CONSTRAINT [FK_DOANHTHUTHEOMHANG_MonAn]
GO
ALTER TABLE [dbo].[HOADON]  WITH CHECK ADD  CONSTRAINT [FK_HOADON_BAN] FOREIGN KEY([MaBan])
REFERENCES [dbo].[BAN] ([MaBan])
ON UPDATE CASCADE
GO
ALTER TABLE [dbo].[HOADON] CHECK CONSTRAINT [FK_HOADON_BAN]
GO
ALTER TABLE [dbo].[HOADON]  WITH CHECK ADD  CONSTRAINT [FK_HOADON_NHANVIEN] FOREIGN KEY([MaNV])
REFERENCES [dbo].[NHANVIEN] ([MaNV])
ON UPDATE CASCADE
GO
ALTER TABLE [dbo].[HOADON] CHECK CONSTRAINT [FK_HOADON_NHANVIEN]
GO
ALTER TABLE [dbo].[MonAn]  WITH CHECK ADD  CONSTRAINT [FK_MonAn_LoaiMonAn] FOREIGN KEY([MaLoai])
REFERENCES [dbo].[LoaiMonAn] ([MaLoai])
ON UPDATE CASCADE
GO
ALTER TABLE [dbo].[MonAn] CHECK CONSTRAINT [FK_MonAn_LoaiMonAn]
GO
ALTER TABLE [dbo].[NGUOIDUNG]  WITH CHECK ADD  CONSTRAINT [FK_NGUOIDUNG_NHANVIEN] FOREIGN KEY([MaNV])
REFERENCES [dbo].[NHANVIEN] ([MaNV])
ON UPDATE CASCADE
ON DELETE CASCADE
GO
ALTER TABLE [dbo].[NGUOIDUNG] CHECK CONSTRAINT [FK_NGUOIDUNG_NHANVIEN]
GO
ALTER TABLE [dbo].[NGUOIDUNG]  WITH CHECK ADD  CONSTRAINT [FK_NGUOIDUNG_VaiTro] FOREIGN KEY([Mavaitro])
REFERENCES [dbo].[VaiTro] ([Mavaitro])
ON UPDATE CASCADE
GO
ALTER TABLE [dbo].[NGUOIDUNG] CHECK CONSTRAINT [FK_NGUOIDUNG_VaiTro]
GO
ALTER TABLE [dbo].[THEKH]  WITH CHECK ADD  CONSTRAINT [FK_THEKH_DICHVU] FOREIGN KEY([TenDV])
REFERENCES [dbo].[DICHVU] ([TenDV])
ON UPDATE CASCADE
ON DELETE CASCADE
GO
ALTER TABLE [dbo].[THEKH] CHECK CONSTRAINT [FK_THEKH_DICHVU]
GO
USE [master]
GO
ALTER DATABASE [DACNPM] SET  READ_WRITE 
GO
