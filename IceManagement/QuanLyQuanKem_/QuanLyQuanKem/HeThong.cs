﻿using System;
using QuanLyQuanKem.FormThuNgan;
using QuanLyQuanKem.FormQuanLy;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace QuanLyQuanKem
{
    public partial class HeThong : Form
    {
        public HeThong()
        {
            InitializeComponent();
            //Thiết lập vị trí của các control khi form chạy toàn màn hình
            ddbtnHeThong.Location = new Point(1500, 10);
            ddbtnHeThong.Size = new Size(19, 50);
            lblTenNV.Location = new Point(1400, 25);
            lblTenNV.Font = new Font(lblTenNV.Font.Name, 12f,FontStyle.Bold);
            pbUser.Location = new Point(1350, 25);
        }

        //Form LOAD
        private void HeThong_Load(object sender, EventArgs e)
        {
            DangNhap frmdn = new DangNhap();
            frmdn.sh = EnabledTileItem;
            frmdn.vs = visible;
            frmdn.cl = close;
            frmdn.ShowDialog();
        }

        #region Hàm thực thi form dùng delegate
        //Hàm thực thi cho người dùng là quản lý
        void visible(Boolean val,string tennv,string manv)
        {
            tlcHeThong.Visible = val;
            tliQLNV.Enabled = val;
            tliThongKe.Enabled = val;
            lblTenNV.Text = tennv;
            lblMaNV.Text = manv;
        }

        //Hàm thực thi cho người dùng là thu ngân
        void EnabledTileItem(Boolean val,string tennv,string manv)
        {
            tlcHeThong.Visible = val;
            tliQLNV.Enabled = !val;
            tliThongKe.Enabled = !val;
            lblTenNV.Text = tennv;
            lblMaNV.Text = manv;
        }

        //Hàm thực thi khi thoát chương trình
        void close()
        {
            DialogResult dr = MessageBox.Show("Bạn muốn thoát chương trình?", "Thông báo", MessageBoxButtons.YesNo, MessageBoxIcon.Question);
            if(dr==DialogResult.Yes)
                this.Close();
            else
            {
                DangNhap frmdn = new DangNhap();
                frmdn.sh = EnabledTileItem;
                frmdn.vs = visible;
                frmdn.cl = close;
                frmdn.ShowDialog();
            }
        }
        #endregion

        #region Xử lý các tileitem_Click,button_Click
        private void barbtnDangXuat_ItemClick(object sender, DevExpress.XtraBars.ItemClickEventArgs e)
        {
            DangNhap frmdn = new DangNhap();
            frmdn.sh = EnabledTileItem;
            frmdn.vs = visible;
            frmdn.cl = close;
            frmdn.ShowDialog();
            //System.Windows.Forms.Application.Restart();
        }
        private void barbtnThoat_ItemClick(object sender, DevExpress.XtraBars.ItemClickEventArgs e)
        {
            DialogResult dr = MessageBox.Show("Bạn muốn thoát chương trình?", "Thông báo", MessageBoxButtons.YesNo, MessageBoxIcon.Question);
            if (dr == DialogResult.Yes)
                this.Close();
        }
        private void tliDatBan_ItemClick(object sender, DevExpress.XtraEditors.TileItemEventArgs e)
        {
            DatBan frm = new DatBan(lblTenNV.Text,lblMaNV.Text);
            //frm.MdiParent = this;
            frm.Show();
        }
        private void tliTheKH_ItemClick_1(object sender, DevExpress.XtraEditors.TileItemEventArgs e)
        {
            TheKH frm = new TheKH();
            //frm.MdiParent = this;
            frm.Show();
        }

        private void tliGoiMon_ItemClick(object sender, DevExpress.XtraEditors.TileItemEventArgs e)
        {
            GoiMon frm = new GoiMon("","", lblTenNV.Text,lblMaNV.Text);
            //frm.MdiParent = this;
            frm.Show();
        }

        private void tliQLNV_ItemClick(object sender, DevExpress.XtraEditors.TileItemEventArgs e)
        {
            QLNhanVien frm = new QLNhanVien();
            //frm.MdiParent = this;
            frm.Show();
        }

        private void tliThongKe_ItemClick(object sender, DevExpress.XtraEditors.TileItemEventArgs e)
        {
            ThongKe frm = new ThongKe();
            //frm.MdiParent = this;
            frm.Show();
        }
        private void tliHelp_ItemClick(object sender, DevExpress.XtraEditors.TileItemEventArgs e)
        {
            Help.ShowHelp(this,"C:\\Users\\Milo\\Desktop\\Help\\Help.chm");
        }
        #endregion        
    }
}
