﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using QuanLyQuanKem.Classes;

namespace QuanLyQuanKem
{
    public partial class DangKy : Form
    {
        LoadSQL lsql = new LoadSQL();
        DataTable dt;
        public DangKy()
        {
            InitializeComponent();
        }

        private void DangKy_Load(object sender, EventArgs e)
        {
            cboTenNV.DataSource = lsql.layMaTen();            
            cboTenNV.DisplayMember = "tennv";
            cboTenNV.ValueMember = "manv";
            cboTenNV.Text = "Tên nhân viên";
            if (cboTenNV.Text == "Tên nhân viên")
            {
                txtMK.Enabled = false;
                txtTK.Enabled = false;
            }
        }

        //Chọn item trong combobox thì textbox Enabled=true
        private void cboTenNV_SelectedIndexChanged(object sender, EventArgs e)
        {
            dt = lsql.laythongtindangnhap();           
            foreach (DataRow row in dt.Rows)
                if (cboTenNV.SelectedValue.ToString() == row[5].ToString())
                    MessageBox.Show("Nhân viên này đã có tài khoản đăng nhập", "Thông báo", MessageBoxButtons.OK, MessageBoxIcon.Stop);
            txtMK.Enabled = true;
            txtTK.Enabled = true;
        }

        private void txtTK_Enter(object sender, EventArgs e)
        {
            txtTK.Text = "";
        }

        private void txtMK_Enter(object sender, EventArgs e)
        {
            txtMK.Text = "";
        }

        //Sự kiện khi nội dung 2 textbox thay đổi
        private void txtTK_TextChanged(object sender, EventArgs e)
        {
            if (txtMK.Text != "Mật khẩu" && txtTK.Text != "Tài khoản")
                spbtnDangKy.Enabled = true;
            else
                spbtnDangKy.Enabled = false;
        }

        //Button Đăng ký
        private void spbtnDangKy_Click(object sender, EventArgs e)
        {
            string mavaitro="";
            if (txtMK.Text == "")
                MessageBox.Show("Chưa nhập mật khẩu!", "Thông báo", MessageBoxButtons.OK, MessageBoxIcon.Stop);
            else if(txtTK.Text=="")
                MessageBox.Show("Chưa nhập tài khoản!", "Thông báo", MessageBoxButtons.OK, MessageBoxIcon.Stop);
            else
            {               
                if (cboTenNV.SelectedValue.ToString().Substring(0, 2) == "QL")
                    mavaitro = "2";
                if (cboTenNV.SelectedValue.ToString().Substring(0, 2) == "TN")
                    mavaitro = "1";
                lsql.dangky(txtTK.Text, txtMK.Text, cboTenNV.SelectedValue.ToString(), mavaitro);
                MessageBox.Show("Đăng ký thành công", "Thông báo", MessageBoxButtons.OK, MessageBoxIcon.None);
                this.Close();
            }
        }


    }
}
