﻿using QuanLyQuanKem.Classes;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using DevExpress;

namespace QuanLyQuanKem
{
    public partial class DatBan : Form
    {
        LoadSQL lsql=new LoadSQL();
        Button btn;
        public DatBan()
        {
            InitializeComponent();
            loadtable();
        }
        string tennv,manv;
        public DatBan(string TENNV,string MANV ):this()
        {
            tennv=TENNV;
            manv = MANV;
        }

        //Load danh sách bàn lên với ứng với từng button   
        void loadtable()
        {           
            List<btnBan> tablelist = lsql.loadtablelist();
            foreach (btnBan item in tablelist)
            {
                btn = new Button();
                btn.Font = new Font("Segeo UI", 10.2f);
                btn.Text = item.Tenban + "\n" + item.Trangthai;
                btn.Width = 225;
                btn.Height = 125;
                btn.FlatStyle = FlatStyle.Flat;
                btn.FlatAppearance.BorderSize = 0;
                btn.Click+=DatBan_Click;
                btn.Tag = item;
                switch (item.Trangthai)
                {
                    case "Trống":
                        btn.BackColor = Color.FromArgb(112, 195, 245);
                        break;
                    default:
                        btn.BackColor = Color.IndianRed;
                        break;
                }
                flpTable.Controls.Add(btn); //Đặt các button trong flowlayoutpanel
            }
        }

        //Các button bàn click
        private void DatBan_Click(object sender, EventArgs e)
        {
            string ten=((sender as Button).Tag as btnBan).Tenban;
            string ma = ((sender as Button).Tag as btnBan).Maban.ToString();
            string trangthai = ((sender as Button).Tag as btnBan).Trangthai;
            if (trangthai == "Đang dùng")
            {
                DialogResult dr= MessageBox.Show("Bạn có muốn dọn bàn "+ten+"?", "Thông báo", MessageBoxButtons.YesNo, MessageBoxIcon.Question);
                if(dr==DialogResult.Yes)
                {
                    lsql.update1DangDung(ma,"Trống");
                    flpTable.Controls.Clear();
                    loadtable();
                }
            }
            else
            {
                GoiMon frmgm = new GoiMon(ten, ma, tennv, manv);
                frmgm.Show();
                this.Hide();
            }
        }

        #region Di chuyển form
        Boolean flag; int x, y;
        private void panel1_MouseDown(object sender, MouseEventArgs e)
        {
            x = e.X;
            y = e.Y;
            flag = true;
        }

        private void DatBan_MouseUp(object sender, MouseEventArgs e)
        {
            flag = false;
        }

        private void DatBan_MouseMove(object sender, MouseEventArgs e)
        {
            if (flag == true)
                this.SetDesktopLocation(Cursor.Position.X - x, Cursor.Position.Y - y);
        }
        #endregion

        #region Đồng hồ đếm ngược để thiết lập trạng thái bàn
        int phut, giay;
        //Hàm đồng hồ đếm ngược
        private void timer1_Tick(object sender, EventArgs e)
        {
            if (phut > 0)
            {
                if (giay > 0)
                    giay--;
                else
                {
                    phut--;
                    giay = 59;
                }
            }
            else
            {
                if (giay > 0)
                    giay--;
                else
                {
                    timer1.Stop();
                    flpTable.Controls.Clear();
                    loadtable();                  
                }
            }
        }
        #endregion

        private void DatBan_Load(object sender, EventArgs e)
        {
            
        }

        //Button thoát form
        private void winUIbtnThoat_Click(object sender, EventArgs e)
        {
            this.Close();
        }

        //update tất cả các bàn Đang Dùng thành Trống
        private void winUIbtnClean_Click(object sender, EventArgs e)
        {
            DialogResult dr = MessageBox.Show("Dọn tất cả bàn?", "Thông báo", MessageBoxButtons.YesNo, MessageBoxIcon.Question);
            if (dr == DialogResult.Yes)
            {
                lsql.updateALLTrong();
                flpTable.Controls.Clear();
                loadtable();
            }
        }

        
    }
}
