﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using QuanLyQuanKem.Classes;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using QuanLyQuanKem.Report_Hóa_Đơn;

namespace QuanLyQuanKem.FormThuNgan
{
    public partial class ThanhToan : Form
    {
        LoadSQL lsql = new LoadSQL();
        public delegate void insertCTHD(string a,string b,string c);
        public insertCTHD ins;
        public ThanhToan()
        {
            InitializeComponent();
        }

        #region Hàm constructor
        string maban,manv,thanhtien,uudai,tongtien,tenban;
        public ThanhToan(string mb,string MA,string TT,string ud,string tt,string tb):this()
        {
            maban = mb;
            manv = MA;
            thanhtien=TT;
            uudai = ud;
            tongtien = tt;
            tenban = tb;
        }
        #endregion

        //Form Load
        private void ThanhToan_Load(object sender, EventArgs e)
        {
            txtThanhTien.Text = thanhtien;
            txtUuDai.Text = uudai+"%";
            txtTongTien.Text = tongtien;
        }

        #region Bắt lỗi
        //Nếu nhập chữ sẽ báo lỗi
        public bool IsNumber(string p)
        {
            foreach (Char c in p)
            {
                if (!Char.IsDigit(c))
                    MessageBox.Show("Nhập số!", "Thông báo", MessageBoxButtons.OK, MessageBoxIcon.Stop);
            }
            return true;
        }

        //Khi textbox Khách trả thay đổi thì tiền thối sẽ thay đổi theo
        private void txtKhachTra_TextChanged(object sender, EventArgs e)
        {
            if (txtKhachTra.Text == "")
                txtTienThoi.Text = "";
            else
            {
                IsNumber(txtKhachTra.Text);
                txtTienThoi.Text = (Int32.Parse(txtKhachTra.Text) - Int32.Parse(txtTongTien.Text)).ToString();
            }
        }
        #endregion

        //Button In hóa đơn
        private void spbtnInHD_Click(object sender, EventArgs e)
        {
            if (txtKhachTra.Text == "")
                MessageBox.Show("Chưa nhập tiền khách trả!", "Thông báo", MessageBoxButtons.OK, MessageBoxIcon.Stop);
            else
            {
                string ngay = DateTime.Now.ToString();
                string tienthoi = txtTienThoi.Text;
                string kt = txtKhachTra.Text;

                //Lưu vào table Hóa Đơn
                lsql.insertHOADON(maban, ngay, manv);

                //Thực thi hàm lưu vào Chi Tiết Hóa Đơn bên form GoiMon
                ins.Invoke(kt, tienthoi, ngay);

                //Report hóa đơn
                ReportHD frm = new ReportHD();
                frm.ShowDialog();

                //update trạng thái bàn Trống->Đang dùng
                lsql.update1DangDung(maban,"Đang dùng");

                this.Close();
            }
        }

    }
}
