﻿namespace QuanLyQuanKem.FormThuNgan
{
    partial class ThanhToan
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.lblUuDai = new System.Windows.Forms.Label();
            this.lblTongTien = new System.Windows.Forms.Label();
            this.lblThanhTien = new System.Windows.Forms.Label();
            this.label1 = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.txtThanhTien = new System.Windows.Forms.TextBox();
            this.txtUuDai = new System.Windows.Forms.TextBox();
            this.txtTongTien = new System.Windows.Forms.TextBox();
            this.txtKhachTra = new System.Windows.Forms.TextBox();
            this.txtTienThoi = new System.Windows.Forms.TextBox();
            this.label3 = new System.Windows.Forms.Label();
            this.spbtnInHD = new DevExpress.XtraEditors.SimpleButton();
            this.SuspendLayout();
            // 
            // lblUuDai
            // 
            this.lblUuDai.AutoSize = true;
            this.lblUuDai.Font = new System.Drawing.Font("Segoe UI", 10.2F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblUuDai.Location = new System.Drawing.Point(16, 96);
            this.lblUuDai.Name = "lblUuDai";
            this.lblUuDai.Size = new System.Drawing.Size(68, 23);
            this.lblUuDai.TabIndex = 35;
            this.lblUuDai.Text = "Ưu đãi:";
            // 
            // lblTongTien
            // 
            this.lblTongTien.AutoSize = true;
            this.lblTongTien.Font = new System.Drawing.Font("Segoe UI", 10.2F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblTongTien.Location = new System.Drawing.Point(16, 61);
            this.lblTongTien.Name = "lblTongTien";
            this.lblTongTien.Size = new System.Drawing.Size(92, 23);
            this.lblTongTien.TabIndex = 36;
            this.lblTongTien.Text = "Tổng tiền:";
            // 
            // lblThanhTien
            // 
            this.lblThanhTien.AutoSize = true;
            this.lblThanhTien.Font = new System.Drawing.Font("Segoe UI", 10.2F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblThanhTien.Location = new System.Drawing.Point(16, 25);
            this.lblThanhTien.Name = "lblThanhTien";
            this.lblThanhTien.Size = new System.Drawing.Size(100, 23);
            this.lblThanhTien.TabIndex = 37;
            this.lblThanhTien.Text = "Thành tiền:";
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Font = new System.Drawing.Font("Segoe UI", 10.2F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label1.Location = new System.Drawing.Point(16, 147);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(91, 23);
            this.label1.TabIndex = 36;
            this.label1.Text = "Khách trả:";
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Font = new System.Drawing.Font("Segoe UI", 10.2F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label2.Location = new System.Drawing.Point(16, 183);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(86, 23);
            this.label2.TabIndex = 36;
            this.label2.Text = "Tiền thối:";
            // 
            // txtThanhTien
            // 
            this.txtThanhTien.Location = new System.Drawing.Point(104, 22);
            this.txtThanhTien.Name = "txtThanhTien";
            this.txtThanhTien.ReadOnly = true;
            this.txtThanhTien.Size = new System.Drawing.Size(212, 30);
            this.txtThanhTien.TabIndex = 38;
            // 
            // txtUuDai
            // 
            this.txtUuDai.Location = new System.Drawing.Point(104, 96);
            this.txtUuDai.Name = "txtUuDai";
            this.txtUuDai.ReadOnly = true;
            this.txtUuDai.Size = new System.Drawing.Size(212, 30);
            this.txtUuDai.TabIndex = 38;
            // 
            // txtTongTien
            // 
            this.txtTongTien.Location = new System.Drawing.Point(104, 58);
            this.txtTongTien.Name = "txtTongTien";
            this.txtTongTien.ReadOnly = true;
            this.txtTongTien.Size = new System.Drawing.Size(212, 30);
            this.txtTongTien.TabIndex = 38;
            // 
            // txtKhachTra
            // 
            this.txtKhachTra.Location = new System.Drawing.Point(104, 144);
            this.txtKhachTra.Name = "txtKhachTra";
            this.txtKhachTra.Size = new System.Drawing.Size(212, 30);
            this.txtKhachTra.TabIndex = 1;
            this.txtKhachTra.TextChanged += new System.EventHandler(this.txtKhachTra_TextChanged);
            // 
            // txtTienThoi
            // 
            this.txtTienThoi.Location = new System.Drawing.Point(104, 180);
            this.txtTienThoi.Name = "txtTienThoi";
            this.txtTienThoi.ReadOnly = true;
            this.txtTienThoi.Size = new System.Drawing.Size(212, 30);
            this.txtTienThoi.TabIndex = 2;
            // 
            // label3
            // 
            this.label3.BackColor = System.Drawing.Color.Peru;
            this.label3.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.label3.ForeColor = System.Drawing.Color.Peru;
            this.label3.Location = new System.Drawing.Point(12, 133);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(314, 2);
            this.label3.TabIndex = 39;
            // 
            // spbtnInHD
            // 
            this.spbtnInHD.Appearance.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(112)))), ((int)(((byte)(195)))), ((int)(((byte)(245)))));
            this.spbtnInHD.Appearance.Font = new System.Drawing.Font("Segoe UI", 10.2F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.spbtnInHD.Appearance.Options.UseBackColor = true;
            this.spbtnInHD.Appearance.Options.UseFont = true;
            this.spbtnInHD.ButtonStyle = DevExpress.XtraEditors.Controls.BorderStyles.Style3D;
            this.spbtnInHD.Location = new System.Drawing.Point(110, 231);
            this.spbtnInHD.Name = "spbtnInHD";
            this.spbtnInHD.Size = new System.Drawing.Size(117, 52);
            this.spbtnInHD.TabIndex = 40;
            this.spbtnInHD.Text = "In hóa đơn";
            this.spbtnInHD.Click += new System.EventHandler(this.spbtnInHD_Click);
            // 
            // ThanhToan
            // 
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Inherit;
            this.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(112)))), ((int)(((byte)(195)))), ((int)(((byte)(245)))));
            this.ClientSize = new System.Drawing.Size(340, 313);
            this.Controls.Add(this.spbtnInHD);
            this.Controls.Add(this.label3);
            this.Controls.Add(this.txtTienThoi);
            this.Controls.Add(this.txtKhachTra);
            this.Controls.Add(this.txtTongTien);
            this.Controls.Add(this.txtUuDai);
            this.Controls.Add(this.txtThanhTien);
            this.Controls.Add(this.lblUuDai);
            this.Controls.Add(this.label2);
            this.Controls.Add(this.label1);
            this.Controls.Add(this.lblTongTien);
            this.Controls.Add(this.lblThanhTien);
            this.Font = new System.Drawing.Font("Segoe UI", 10.2F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.Name = "ThanhToan";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "Thanh toán";
            this.Load += new System.EventHandler(this.ThanhToan_Load);
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Label lblUuDai;
        private System.Windows.Forms.Label lblTongTien;
        private System.Windows.Forms.Label lblThanhTien;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.TextBox txtThanhTien;
        private System.Windows.Forms.TextBox txtUuDai;
        private System.Windows.Forms.TextBox txtTongTien;
        private System.Windows.Forms.TextBox txtKhachTra;
        private System.Windows.Forms.TextBox txtTienThoi;
        private System.Windows.Forms.Label label3;
        private DevExpress.XtraEditors.SimpleButton spbtnInHD;

    }
}