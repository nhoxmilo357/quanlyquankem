﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using QuanLyQuanKem.Classes;

namespace QuanLyQuanKem.FormThuNgan
{
    public partial class TheKH : Form
    {
        LoadSQL lsql = new LoadSQL();
        public TheKH()
        {
            InitializeComponent();
            loadTHE();
        }

        private void TheKH_Load(object sender, EventArgs e)
        {          
            dgvTheKH.ClearSelection();
            setTxTLbL();
        }

        //Load dữ liệu từ bảng thẻ khách hàng lên datagridview
        void loadTHE()
        {
            dgvTheKH.DataSource = lsql.layMaThe();
            dgvTheKH.Columns[5].Visible = false;
            dgvTheKH.Font = new Font("Segoe UI", 10.2f);
        }

        void setTxTLbL()
        {
            txtHoTen.Text = "";
            txtSDT.Text = "";
            dtpNgayLap.Text = DateTime.Now.ToString();
            lblTenThe.Visible = false;
        }

        //Xử lý datagridview
        private void dgvTheKH_CellClick(object sender, DataGridViewCellEventArgs e)
        {
            spbtnSua.Enabled = true;
            spbtnXoa.Enabled = true;

            //Hiện thông tin thẻ lên textbox,label
            if (dgvTheKH.Columns[e.ColumnIndex].GetType().ToString() == "System.Windows.Forms.DataGridViewTextBoxColumn")
            {
                lblTenThe.Visible = true;
                lblTenThe.Text = dgvTheKH.Rows[e.RowIndex].Cells[1].Value.ToString();
                txtHoTen.Text = dgvTheKH.Rows[e.RowIndex].Cells[2].Value.ToString();
                txtSDT.Text = dgvTheKH.Rows[e.RowIndex].Cells[3].Value.ToString().Trim();
                dtpNgayLap.Text = dgvTheKH.Rows[e.RowIndex].Cells[4].Value.ToString();
            }

            //Xóa thẻ
            if (dgvTheKH.Columns[e.ColumnIndex].GetType().ToString() == "System.Windows.Forms.DataGridViewButtonColumn")
            {
                string mathe=dgvTheKH.Rows[e.RowIndex].Cells[1].Value.ToString();
                string tenkh=dgvTheKH.Rows[e.RowIndex].Cells[2].Value.ToString();
                DialogResult dr = MessageBox.Show("Xóa thẻ " + mathe + " của " + tenkh, "Thông báo", MessageBoxButtons.YesNo, MessageBoxIcon.Question);
                if (dr == DialogResult.Yes)
                {
                    loadTHE();
                    setTxTLbL();
                    lsql.xoaTheKH(mathe);
                }
            }
        }

        #region Buttons
        //Button Thêm
        private void spbtnTHem_Click(object sender, EventArgs e)
        {
            if (spbtnTHem.Text == "Thêm")
            {
                setTxTLbL();
                dtpNgayLap.Text = DateTime.Now.ToString();
                spbtnTHem.Text = "Lưu";
                spbtnSua.Enabled = false;
                spbtnXoa.Enabled = false;
            }
            else
            {
                if (txtHoTen.Text == "" || txtSDT.Text == "")
                    MessageBox.Show("Chưa điền đầy đủ thông tin!", "Thông báo", MessageBoxButtons.OK, MessageBoxIcon.Stop);
                else
                {
                    if (txtSDT.Text.Length < 10 || txtSDT.Text.Length > 11)
                        MessageBox.Show("Số điện thoại có 10 hoặc 11 chữ số!", "Thông báo", MessageBoxButtons.OK, MessageBoxIcon.Error);
                    else
                    {
                        lsql.insertTheKH(txtHoTen.Text, txtSDT.Text, dtpNgayLap.Text);
                        setTxTLbL();
                        spbtnTHem.Text = "Thêm";
                        loadTHE();
                        dgvTheKH.Refresh();
                    }
                }
            }
        }

        //Xóa thẻ
        private void spbtnXoa_Click(object sender, EventArgs e)
        {
            int i = dgvTheKH.CurrentRow.Index;
            string mathe = dgvTheKH.Rows[i].Cells[1].Value.ToString();
            string tenkh = dgvTheKH.Rows[i].Cells[2].Value.ToString();
            DialogResult dr = MessageBox.Show("Xóa thẻ " + mathe + " của " + tenkh, "Thông báo", MessageBoxButtons.YesNo, MessageBoxIcon.Question);
            if (dr == DialogResult.Yes)
            {              
                lsql.xoaTheKH(mathe);
                loadTHE();
                setTxTLbL();
                spbtnXoa.Enabled = false;
            }
        }

        //Button Sửa
        private void spbtnSua_Click(object sender, EventArgs e)
        {
            string mathe = lblTenThe.Text;
            string tenkh = txtHoTen.Text;
            string sdt = txtSDT.Text;
            string ngay = dtpNgayLap.Text;
            if (spbtnSua.Text == "Sửa")
            {
                spbtnSua.Text = "Lưu";
                spbtnXoa.Enabled = false;
            }
            else
            {
                lsql.updateTheKH(mathe, tenkh, sdt, ngay);
                MessageBox.Show("Ok");
                spbtnSua.Text = "Sửa";
                loadTHE();
                spbtnSua.Enabled = false;
            }
        }
        #endregion

        #region Bắt lỗi
        //Nếu nhập chữ sẽ báo lỗi
        public bool IsNumber(string p)
        {
            foreach(Char c in p)
            {
                if (!Char.IsDigit(c))
                    MessageBox.Show("Nhập số!", "Thông báo", MessageBoxButtons.OK, MessageBoxIcon.Stop);
            }
            return true;
        }

        //Nếu nhập số sẽ báo lỗi
        public bool IsText(string p)
        {
            foreach (Char c in p)
            {
                if (Char.IsDigit(c))
                    MessageBox.Show("Không được nhập số vào họ tên!", "Thông báo", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
            return true;
        }

        //Bắt lỗi textbox Số điện thoại
        private void txtSDT_TextChanged(object sender, EventArgs e)
        {
            IsNumber(txtSDT.Text);
        }

        //Bắt lỗi textbox họ tên
        private void txtHoTen_TextChanged(object sender, EventArgs e)
        {
            IsText(txtHoTen.Text);
        }
        #endregion
        
    }
}
