﻿using QuanLyQuanKem.Classes;
using QuanLyQuanKem.FormThuNgan;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace QuanLyQuanKem
{
    public partial class GoiMon : Form
    {
        LoadSQL lsql = new LoadSQL();
        DataTable dt;
        public GoiMon()
        {
            InitializeComponent();
            loadKem();            
        }
        private void GoiMon_Load(object sender, EventArgs e)
        {
            dgvDatMon.Font = new Font("Segeo UI", 9.6f);
            lblBan.Text = tenban;
            lblTenNV.Text = tennv1;
            dt = lsql.layDichVu();
            DateTime now = DateTime.Now;
            foreach (DataRow row in dt.Rows)
            {
              //nếu ngày kết thúc hoặc ngày bắt đầu của dịch vụ trùng với ngày hệ thống,label số ưu đãi sẽ hiện ứng với số ưu đãi ngày đó
                if (row[0].ToString() == now.ToShortDateString() || row[1].ToString() == now.ToShortDateString())
                    lbluudai_float.Text = row[2].ToString();
            }

            //Nếu ưu đãi giảm sẵn 10% thì không cần nhập mã thẻ
            if (lbluudai_float.Text == "10")
                spbtnNhapMaThe.Enabled = false;
            else
                spbtnNhapMaThe.Enabled = true;

            //Nếu chưa chọn bàn sẽ không nhấn thanh toán được
            if (lblBan.Text == "")
                spbtnThanhToan.Enabled = false;
        }

        #region Hàm constructor để nhận dữ liệu từ form khác truyền đến
        string tenban,maban, tennv1,manv;
        public GoiMon(string TEN,string MA,string TENNV1,string MANV)
            : this()
        {
            tenban = TEN;
            maban = MA;
            tennv1 = TENNV1;
            manv = MANV;
        }
        #endregion

        //Load các loại kem lên flowpanel
        void loadKem()
        {
            List<btnKem> tablelist = lsql.loadKem("select MaMH,TenMH,DonGia,a.MaLoai,TenLoai from MonAn a,LoaiMonAn b where a.MaLoai=b.MaLoai");
            foreach (btnKem item in tablelist)
            {
                Button btn = new Button();
                btn.Font = new Font("Segeo UI", 10.2f);
                btn.Text = item.Tenmh + "\n" + item.Dongia;
                btn.Width = 142;
                btn.Height = 100;
                btn.FlatStyle = FlatStyle.Flat;
                btn.BackColor = Color.FromArgb(112, 195, 245);
                btn.FlatAppearance.BorderSize = 0;
                btn.Tag = item;
                btn.Click += GoiMon_Click;
                flpKem.Controls.Add(btn);
            }
        }

        #region Xử lý Gridview
        //Hiện thông tin món lên Gridview
        private void GoiMon_Click(object sender, EventArgs e)
        {
            string tenmon = ((sender as Button).Tag as btnKem).Tenmh;
            string soluong = "1";
            string dongia = (((sender as Button).Tag as btnKem).Dongia).ToString();
            string mamh = (((sender as Button).Tag as btnKem).Mamh).ToString();
            string loai = ((sender as Button).Tag as btnKem).Tenloai;
            string[] row = { tenmon, soluong, dongia, null, mamh, loai };
            dgvDatMon.Rows.Add(row);
        }
        //Sự kiện khi nội dung Gridview thay đổi
        private void dgvDatMon_RowStateChanged(object sender, DataGridViewRowStateChangedEventArgs e)
        {
            int a = 0, i;
            for (i = 0; i < dgvDatMon.RowCount; i++)
            {
                a = a + Convert.ToInt32(dgvDatMon.Rows[i].Cells[2].Value.ToString());
               
            }
            lblthanhtien_float.Text = a.ToString();
            if (lbluudai_float.Text == "0")
                lbltongtien_float.Text = lblthanhtien_float.Text;
            else
                lbltongtien_float.Text = (Int32.Parse(lblthanhtien_float.Text)-(Int32.Parse(lbluudai_float.Text) * Int32.Parse(lblthanhtien_float.Text)/100)).ToString();
           
            //Nếu tiền đặt món(chưa qua ưu đãi) > 200k có thể làm thẻ KH
            if (int.Parse(lblthanhtien_float.Text) > 200000)
                spbtnTheKH.Enabled = true;
            else spbtnTheKH.Enabled = false;

            //Nếu tiền đặt món khác 0 thì button thanh toán sẽ mở
            if (int.Parse(lblthanhtien_float.Text) == 0)
                spbtnThanhToan.Enabled = false;
            else
                spbtnThanhToan.Enabled = true;
        }
        //Xóa dòng trong Gridview
        private void dgvDatMon_CellClick(object sender, DataGridViewCellEventArgs e)
        {
            int num = e.RowIndex;
            if (dgvDatMon.Columns[e.ColumnIndex].GetType().ToString() == "System.Windows.Forms.DataGridViewButtonColumn")
                dgvDatMon.Rows.RemoveAt(num);
        }
        #endregion

        #region Xử lý các button kem
        private void acdCETrangChinh_Click(object sender, EventArgs e)
        {
            flpKem.Controls.Clear();
            List<btnKem> tablelist = lsql.loadKem("select MaMH,TenMH,DonGia,a.MaLoai,TenLoai from MonAn a,LoaiMonAn b where a.MaLoai=b.MaLoai");
            foreach (btnKem item in tablelist)
            {
                Button btn = new Button();
                btn.Font = new Font("Segeo UI", 10.2f);
                btn.Text = item.Tenmh + "\n" + item.Dongia;
                btn.Width = 142;
                btn.Height = 100;
                btn.FlatStyle = FlatStyle.Flat;
                btn.BackColor = Color.FromArgb(112, 195, 245);
                btn.FlatAppearance.BorderSize = 0;
                btn.Tag = item;
                btn.Click += GoiMon_Click;
                flpKem.Controls.Add(btn);
            }
        }

        private void acdCEKemCay_Click(object sender, EventArgs e)
        {
            flpKem.Controls.Clear();
            List<btnKem> tablelist = lsql.loadKem("select MaMH,TenMH,DonGia,a.MaLoai,TenLoai from MonAn a,LoaiMonAn b where a.MaLoai=b.MaLoai and a.MaLoai=1");
            foreach (btnKem item in tablelist)
            {
                Button btn = new Button();
                btn.Font = new Font("Segeo UI", 10.2f);
                btn.Text = item.Tenmh + "\n" + item.Dongia;
                btn.Width = 142;
                btn.Height = 100;
                btn.FlatStyle = FlatStyle.Flat;
                btn.BackColor = Color.FromArgb(112, 195, 245);
                btn.FlatAppearance.BorderSize = 0;
                btn.Tag = item;
                btn.Click += GoiMon_Click;
                flpKem.Controls.Add(btn);
            }
        }

        private void acdCEKemLy_Click(object sender, EventArgs e)
        {
            flpKem.Controls.Clear();
            List<btnKem> tablelist = lsql.loadKem("select MaMH,TenMH,DonGia,a.MaLoai,TenLoai from MonAn a,LoaiMonAn b where a.MaLoai=b.MaLoai and a.MaLoai=2");
            foreach (btnKem item in tablelist)
            {
                Button btn = new Button();
                btn.Font = new Font("Segeo UI", 10.2f);
                btn.Text = item.Tenmh + "\n" + item.Dongia;
                btn.Width = 142;
                btn.Height = 100;
                btn.FlatStyle = FlatStyle.Flat;
                btn.BackColor = Color.FromArgb(112, 195, 245);
                btn.FlatAppearance.BorderSize = 0;
                btn.Tag = item;
                btn.Click += GoiMon_Click;
                flpKem.Controls.Add(btn);
            }
        }

        private void acdCEKemCombo_Click(object sender, EventArgs e)
        {
            flpKem.Controls.Clear();
            List<btnKem> tablelist = lsql.loadKem("select MaMH,TenMH,DonGia,a.MaLoai,TenLoai from MonAn a,LoaiMonAn b where a.MaLoai=b.MaLoai and a.MaLoai=3");
            foreach (btnKem item in tablelist)
            {
                Button btn = new Button();
                btn.Font = new Font("Segeo UI", 10.2f);
                btn.Text = item.Tenmh + "\n" + item.Dongia;
                btn.Width = 142;
                btn.Height = 100;
                btn.FlatStyle = FlatStyle.Flat;
                btn.BackColor = Color.FromArgb(112, 195, 245);
                btn.FlatAppearance.BorderSize = 0;
                btn.Tag = item;
                btn.Click += GoiMon_Click;
                flpKem.Controls.Add(btn);
            }
        }

        private void acdCEBingsu_Click(object sender, EventArgs e)
        {
            flpKem.Controls.Clear();
            List<btnKem> tablelist = lsql.loadKem("select MaMH,TenMH,DonGia,a.MaLoai,TenLoai from MonAn a,LoaiMonAn b where a.MaLoai=b.MaLoai and a.MaLoai=4");
            foreach (btnKem item in tablelist)
            {
                Button btn = new Button();
                btn.Font = new Font("Segeo UI", 10.2f);
                btn.Text = item.Tenmh + "\n" + item.Dongia;
                btn.Width = 142;
                btn.Height = 100;
                btn.FlatStyle = FlatStyle.Flat;
                btn.BackColor = Color.FromArgb(112, 195, 245);
                btn.FlatAppearance.BorderSize = 0;
                btn.Tag = item;
                btn.Click += GoiMon_Click;
                flpKem.Controls.Add(btn);
            }
        }
        #endregion

        //Lưu vào chi tiết hóa đơn  
        void insertCTHD(string kt,string tienthoi,string ngaylap)
        {
            dt = lsql.laymahdmax();
            string ten = null;
            string sl = null;
            string dg = null;
            string ma = null;
            string max;
            string ud = lbluudai_float.Text += "%";
            for (int i = 0; i < dgvDatMon.RowCount; i++)
            {
                ten = dgvDatMon.Rows[i].Cells[0].Value.ToString();
                sl = dgvDatMon.Rows[i].Cells[1].Value.ToString();
                dg = dgvDatMon.Rows[i].Cells[2].Value.ToString();
                ma = dgvDatMon.Rows[i].Cells[4].Value.ToString();
                foreach (DataRow row in dt.Rows)
                {
                    max = row[0].ToString();
                    lsql.insertChiTietHD(max, tenban,ma, ten, sl, dg, lblthanhtien_float.Text,ud,lbltongtien_float.Text,kt,tienthoi, tennv1,ngaylap);
                }
            }             
        }

        //Button Thanh toán
        private void spbtnThanhToan_Click(object sender, EventArgs e)
        {
            //Không thể thanh toán khi chưa chọn mặt hàng
            if (dgvDatMon.RowCount.ToString() == "0")
                MessageBox.Show("Chưa chọn mặt hàng", "Thông báo", MessageBoxButtons.OK, MessageBoxIcon.Stop);
            else
            {
                //Lưu vào bảng Doanh thu
                for (int i = 0; i < dgvDatMon.RowCount; i++)
                {
                    string tenmh = dgvDatMon.Rows[i].Cells[0].Value.ToString();
                    string sl2 = dgvDatMon.Rows[i].Cells[1].Value.ToString();
                    string dongia = dgvDatMon.Rows[i].Cells[2].Value.ToString();
                    string loai = dgvDatMon.Rows[i].Cells[5].Value.ToString();
                    string mamh = dgvDatMon.Rows[i].Cells[4].Value.ToString();
                    lsql.insertDoanhThu(mamh, tenmh, sl2, dongia, loai);
                }
                ThanhToan frmtt = new ThanhToan(maban, manv, lblthanhtien_float.Text, lbluudai_float.Text, lbltongtien_float.Text,lblBan.Text);
                frmtt.ins = insertCTHD;
                frmtt.Show();
            }
        }

        //Show form đặt bàn
        private void spbtnDatBan_Click(object sender, EventArgs e)
        {
            DatBan frm = new DatBan(tennv1,manv);
            frm.Show();
            this.Close();
        }

        #region Xử lý thẻ KH
        // Show form thẻ KH
        private void spbtnTheKH_Click(object sender, EventArgs e)
        {
            TheKH frm = new TheKH();
            frm.ShowDialog();
        }

        //Nhập thẻ khách hàng nếu datagridview có mặt hàng
        private void spbtnNhapMaThe_Click(object sender, EventArgs e)
        {
            if (dgvDatMon.RowCount.ToString()=="0")
                MessageBox.Show("Chưa có mặt hàng");
            else
            {
                txtMaThe.Visible = true;
                spbtnNhapMaThe.Visible = false;
            }
        }

        //khi chọn textbox này,text của nó sẽ thành rỗng
        private void txtMaThe_Enter(object sender, EventArgs e)
        {
            txtMaThe.Text = "";
            txtMaThe.ForeColor = Color.Black;
        }

        //khi chọn control khác ngoài textbox này,nó sẽ ẩn đi
        private void txtMaThe_Leave(object sender, EventArgs e)
        {
            txtMaThe.Text = "Nhập mã thẻ";
            txtMaThe.ForeColor = Color.LightGray;
            txtMaThe.Visible = false;
            spbtnNhapMaThe.Visible = true;
            lbluudai_float.Text = "0";
        }

        //label hiện số ưu đãi sẽ thay đổi nếu nội dung textbox nhập thẻ trùng với mã thẻ trong dữ liệu
        private void txtMaThe_TextChanged(object sender, EventArgs e)
        {
            dt = lsql.layMaThe();
            foreach (DataRow row in dt.Rows)
                if (txtMaThe.Text == row[0].ToString())
                    lbluudai_float.Text = "10";
                else
                    lbluudai_float.Text = "0";
        }
        #endregion
    }
}
