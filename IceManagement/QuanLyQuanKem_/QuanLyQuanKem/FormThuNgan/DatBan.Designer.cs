﻿namespace QuanLyQuanKem
{
    partial class DatBan
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(DatBan));
            this.flpTable = new System.Windows.Forms.FlowLayoutPanel();
            this.panel2 = new System.Windows.Forms.Panel();
            this.panel3 = new System.Windows.Forms.Panel();
            this.winUIbtnClean = new DevExpress.XtraBars.Docking2010.WindowsUIButtonPanel();
            this.panel4 = new System.Windows.Forms.Panel();
            this.timer1 = new System.Windows.Forms.Timer(this.components);
            this.panel1 = new System.Windows.Forms.Panel();
            this.winUIbtnThoat = new DevExpress.XtraBars.Docking2010.WindowsUIButtonPanel();
            this.toolTip1 = new System.Windows.Forms.ToolTip(this.components);
            this.panel3.SuspendLayout();
            this.panel1.SuspendLayout();
            this.SuspendLayout();
            // 
            // flpTable
            // 
            this.flpTable.AutoScroll = true;
            this.flpTable.BackColor = System.Drawing.Color.Transparent;
            this.flpTable.FlowDirection = System.Windows.Forms.FlowDirection.RightToLeft;
            this.flpTable.Location = new System.Drawing.Point(38, 46);
            this.flpTable.Name = "flpTable";
            this.flpTable.RightToLeft = System.Windows.Forms.RightToLeft.Yes;
            this.flpTable.Size = new System.Drawing.Size(930, 410);
            this.flpTable.TabIndex = 0;
            // 
            // panel2
            // 
            this.panel2.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(112)))), ((int)(((byte)(195)))), ((int)(((byte)(245)))));
            this.panel2.Dock = System.Windows.Forms.DockStyle.Left;
            this.panel2.Location = new System.Drawing.Point(0, 30);
            this.panel2.Name = "panel2";
            this.panel2.Size = new System.Drawing.Size(30, 455);
            this.panel2.TabIndex = 2;
            this.panel2.MouseDown += new System.Windows.Forms.MouseEventHandler(this.panel1_MouseDown);
            this.panel2.MouseMove += new System.Windows.Forms.MouseEventHandler(this.DatBan_MouseMove);
            this.panel2.MouseUp += new System.Windows.Forms.MouseEventHandler(this.DatBan_MouseUp);
            // 
            // panel3
            // 
            this.panel3.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(112)))), ((int)(((byte)(195)))), ((int)(((byte)(245)))));
            this.panel3.Controls.Add(this.winUIbtnClean);
            this.panel3.Dock = System.Windows.Forms.DockStyle.Bottom;
            this.panel3.Location = new System.Drawing.Point(30, 455);
            this.panel3.Name = "panel3";
            this.panel3.Size = new System.Drawing.Size(970, 30);
            this.panel3.TabIndex = 3;
            this.panel3.MouseDown += new System.Windows.Forms.MouseEventHandler(this.panel1_MouseDown);
            this.panel3.MouseMove += new System.Windows.Forms.MouseEventHandler(this.DatBan_MouseMove);
            this.panel3.MouseUp += new System.Windows.Forms.MouseEventHandler(this.DatBan_MouseUp);
            // 
            // winUIbtnClean
            // 
            this.winUIbtnClean.BackgroundImage = ((System.Drawing.Image)(resources.GetObject("winUIbtnClean.BackgroundImage")));
            this.winUIbtnClean.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.winUIbtnClean.Buttons.AddRange(new DevExpress.XtraEditors.ButtonPanel.IBaseButton[] {
            new DevExpress.XtraBars.Docking2010.WindowsUIButton(),
            new DevExpress.XtraBars.Docking2010.WindowsUIButton()});
            this.winUIbtnClean.Location = new System.Drawing.Point(455, 0);
            this.winUIbtnClean.Name = "winUIbtnClean";
            this.winUIbtnClean.Size = new System.Drawing.Size(30, 30);
            this.winUIbtnClean.TabIndex = 0;
            this.winUIbtnClean.Text = "Dọn tất cả bàn";
            this.toolTip1.SetToolTip(this.winUIbtnClean, "Dọn tất cả bàn");
            this.winUIbtnClean.Click += new System.EventHandler(this.winUIbtnClean_Click);
            // 
            // panel4
            // 
            this.panel4.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(112)))), ((int)(((byte)(195)))), ((int)(((byte)(245)))));
            this.panel4.Dock = System.Windows.Forms.DockStyle.Right;
            this.panel4.Location = new System.Drawing.Point(970, 30);
            this.panel4.Name = "panel4";
            this.panel4.Size = new System.Drawing.Size(30, 425);
            this.panel4.TabIndex = 4;
            this.panel4.MouseDown += new System.Windows.Forms.MouseEventHandler(this.panel1_MouseDown);
            this.panel4.MouseMove += new System.Windows.Forms.MouseEventHandler(this.DatBan_MouseMove);
            this.panel4.MouseUp += new System.Windows.Forms.MouseEventHandler(this.DatBan_MouseUp);
            // 
            // timer1
            // 
            this.timer1.Interval = 1000;
            this.timer1.Tick += new System.EventHandler(this.timer1_Tick);
            // 
            // panel1
            // 
            this.panel1.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(112)))), ((int)(((byte)(195)))), ((int)(((byte)(245)))));
            this.panel1.Controls.Add(this.winUIbtnThoat);
            this.panel1.Dock = System.Windows.Forms.DockStyle.Top;
            this.panel1.Location = new System.Drawing.Point(0, 0);
            this.panel1.Name = "panel1";
            this.panel1.Size = new System.Drawing.Size(1000, 30);
            this.panel1.TabIndex = 1;
            this.panel1.MouseDown += new System.Windows.Forms.MouseEventHandler(this.panel1_MouseDown);
            this.panel1.MouseMove += new System.Windows.Forms.MouseEventHandler(this.DatBan_MouseMove);
            this.panel1.MouseUp += new System.Windows.Forms.MouseEventHandler(this.DatBan_MouseUp);
            // 
            // winUIbtnThoat
            // 
            this.winUIbtnThoat.BackgroundImage = ((System.Drawing.Image)(resources.GetObject("winUIbtnThoat.BackgroundImage")));
            this.winUIbtnThoat.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.winUIbtnThoat.Buttons.AddRange(new DevExpress.XtraEditors.ButtonPanel.IBaseButton[] {
            new DevExpress.XtraBars.Docking2010.WindowsUIButton(),
            new DevExpress.XtraBars.Docking2010.WindowsUIButton()});
            this.winUIbtnThoat.Location = new System.Drawing.Point(485, 0);
            this.winUIbtnThoat.Name = "winUIbtnThoat";
            this.winUIbtnThoat.Size = new System.Drawing.Size(30, 30);
            this.winUIbtnThoat.TabIndex = 0;
            this.winUIbtnThoat.Text = "Thoát";
            this.toolTip1.SetToolTip(this.winUIbtnThoat, "Thoát");
            this.winUIbtnThoat.Click += new System.EventHandler(this.winUIbtnThoat_Click);
            // 
            // DatBan
            // 
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Inherit;
            this.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(112)))), ((int)(((byte)(195)))), ((int)(((byte)(245)))));
            this.BackgroundImage = ((System.Drawing.Image)(resources.GetObject("$this.BackgroundImage")));
            this.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.ClientSize = new System.Drawing.Size(1000, 485);
            this.Controls.Add(this.panel4);
            this.Controls.Add(this.panel3);
            this.Controls.Add(this.panel2);
            this.Controls.Add(this.panel1);
            this.Controls.Add(this.flpTable);
            this.Cursor = System.Windows.Forms.Cursors.Default;
            this.Font = new System.Drawing.Font("Segoe UI", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.None;
            this.Name = "DatBan";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "Đặt bàn";
            this.Load += new System.EventHandler(this.DatBan_Load);
            this.MouseDown += new System.Windows.Forms.MouseEventHandler(this.panel1_MouseDown);
            this.MouseMove += new System.Windows.Forms.MouseEventHandler(this.DatBan_MouseMove);
            this.MouseUp += new System.Windows.Forms.MouseEventHandler(this.DatBan_MouseUp);
            this.panel3.ResumeLayout(false);
            this.panel1.ResumeLayout(false);
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.FlowLayoutPanel flpTable;
        private System.Windows.Forms.Panel panel2;
        private System.Windows.Forms.Panel panel3;
        private System.Windows.Forms.Panel panel4;
        private System.Windows.Forms.Timer timer1;
        private System.Windows.Forms.Panel panel1;
        private DevExpress.XtraBars.Docking2010.WindowsUIButtonPanel winUIbtnThoat;
        private DevExpress.XtraBars.Docking2010.WindowsUIButtonPanel winUIbtnClean;
        private System.Windows.Forms.ToolTip toolTip1;


    }
}