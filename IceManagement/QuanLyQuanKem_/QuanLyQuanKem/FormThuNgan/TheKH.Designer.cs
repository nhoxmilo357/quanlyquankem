﻿namespace QuanLyQuanKem.FormThuNgan
{
    partial class TheKH
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(TheKH));
            this.lblHoTen = new System.Windows.Forms.Label();
            this.lblSDT = new System.Windows.Forms.Label();
            this.txtHoTen = new System.Windows.Forms.TextBox();
            this.txtSDT = new System.Windows.Forms.TextBox();
            this.dgvTheKH = new System.Windows.Forms.DataGridView();
            this.MaThe = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.TenKH = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.SDT = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Ngaylapthe = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.clmXoa = new System.Windows.Forms.DataGridViewButtonColumn();
            this.lblMaThe = new System.Windows.Forms.Label();
            this.lblTenThe = new System.Windows.Forms.Label();
            this.lblNgayLap = new System.Windows.Forms.Label();
            this.dtpNgayLap = new System.Windows.Forms.DateTimePicker();
            this.pictureBox1 = new System.Windows.Forms.PictureBox();
            this.spbtnTHem = new DevExpress.XtraEditors.SimpleButton();
            this.spbtnXoa = new DevExpress.XtraEditors.SimpleButton();
            this.spbtnSua = new DevExpress.XtraEditors.SimpleButton();
            ((System.ComponentModel.ISupportInitialize)(this.dgvTheKH)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox1)).BeginInit();
            this.SuspendLayout();
            // 
            // lblHoTen
            // 
            this.lblHoTen.AutoSize = true;
            this.lblHoTen.Font = new System.Drawing.Font("Segoe UI", 10.2F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblHoTen.Location = new System.Drawing.Point(168, 94);
            this.lblHoTen.Name = "lblHoTen";
            this.lblHoTen.Size = new System.Drawing.Size(66, 23);
            this.lblHoTen.TabIndex = 0;
            this.lblHoTen.Text = "Họ tên:";
            // 
            // lblSDT
            // 
            this.lblSDT.AutoSize = true;
            this.lblSDT.Font = new System.Drawing.Font("Segoe UI", 10.2F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblSDT.Location = new System.Drawing.Point(449, 58);
            this.lblSDT.Name = "lblSDT";
            this.lblSDT.Size = new System.Drawing.Size(44, 23);
            this.lblSDT.TabIndex = 0;
            this.lblSDT.Text = "SĐT:";
            // 
            // txtHoTen
            // 
            this.txtHoTen.Font = new System.Drawing.Font("Segoe UI", 10.2F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtHoTen.Location = new System.Drawing.Point(241, 91);
            this.txtHoTen.Name = "txtHoTen";
            this.txtHoTen.Size = new System.Drawing.Size(186, 30);
            this.txtHoTen.TabIndex = 1;
            this.txtHoTen.TextChanged += new System.EventHandler(this.txtHoTen_TextChanged);
            // 
            // txtSDT
            // 
            this.txtSDT.Font = new System.Drawing.Font("Segoe UI", 10.2F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtSDT.Location = new System.Drawing.Point(535, 55);
            this.txtSDT.Name = "txtSDT";
            this.txtSDT.Size = new System.Drawing.Size(186, 30);
            this.txtSDT.TabIndex = 2;
            this.txtSDT.TextChanged += new System.EventHandler(this.txtSDT_TextChanged);
            // 
            // dgvTheKH
            // 
            this.dgvTheKH.AllowUserToAddRows = false;
            this.dgvTheKH.BackgroundColor = System.Drawing.Color.FromArgb(((int)(((byte)(112)))), ((int)(((byte)(195)))), ((int)(((byte)(245)))));
            this.dgvTheKH.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dgvTheKH.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.MaThe,
            this.TenKH,
            this.SDT,
            this.Ngaylapthe,
            this.clmXoa});
            this.dgvTheKH.Dock = System.Windows.Forms.DockStyle.Bottom;
            this.dgvTheKH.Location = new System.Drawing.Point(0, 254);
            this.dgvTheKH.Name = "dgvTheKH";
            this.dgvTheKH.ReadOnly = true;
            this.dgvTheKH.RowHeadersVisible = false;
            this.dgvTheKH.RowTemplate.Height = 24;
            this.dgvTheKH.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.FullRowSelect;
            this.dgvTheKH.Size = new System.Drawing.Size(763, 249);
            this.dgvTheKH.TabIndex = 3;
            this.dgvTheKH.TabStop = false;
            this.dgvTheKH.CellClick += new System.Windows.Forms.DataGridViewCellEventHandler(this.dgvTheKH_CellClick);
            // 
            // MaThe
            // 
            this.MaThe.DataPropertyName = "MaThe";
            this.MaThe.HeaderText = "Mã thẻ";
            this.MaThe.Name = "MaThe";
            this.MaThe.ReadOnly = true;
            // 
            // TenKH
            // 
            this.TenKH.DataPropertyName = "TenKH";
            this.TenKH.HeaderText = "Tên khách hàng";
            this.TenKH.Name = "TenKH";
            this.TenKH.ReadOnly = true;
            this.TenKH.Width = 250;
            // 
            // SDT
            // 
            this.SDT.DataPropertyName = "SDT";
            this.SDT.HeaderText = "Số điện thoại";
            this.SDT.Name = "SDT";
            this.SDT.ReadOnly = true;
            this.SDT.Width = 150;
            // 
            // Ngaylapthe
            // 
            this.Ngaylapthe.DataPropertyName = "Ngaylapthe";
            this.Ngaylapthe.HeaderText = "Ngày lập";
            this.Ngaylapthe.Name = "Ngaylapthe";
            this.Ngaylapthe.ReadOnly = true;
            this.Ngaylapthe.Width = 220;
            // 
            // clmXoa
            // 
            this.clmXoa.HeaderText = "";
            this.clmXoa.Name = "clmXoa";
            this.clmXoa.ReadOnly = true;
            this.clmXoa.Text = "Xóa";
            this.clmXoa.UseColumnTextForButtonValue = true;
            this.clmXoa.Width = 40;
            // 
            // lblMaThe
            // 
            this.lblMaThe.AutoSize = true;
            this.lblMaThe.Font = new System.Drawing.Font("Segoe UI", 10.2F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblMaThe.Location = new System.Drawing.Point(168, 58);
            this.lblMaThe.Name = "lblMaThe";
            this.lblMaThe.Size = new System.Drawing.Size(68, 23);
            this.lblMaThe.TabIndex = 0;
            this.lblMaThe.Text = "Mã thẻ:";
            // 
            // lblTenThe
            // 
            this.lblTenThe.AutoSize = true;
            this.lblTenThe.Font = new System.Drawing.Font("Segoe UI", 10.2F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblTenThe.Location = new System.Drawing.Point(237, 58);
            this.lblTenThe.Name = "lblTenThe";
            this.lblTenThe.Size = new System.Drawing.Size(19, 23);
            this.lblTenThe.TabIndex = 0;
            this.lblTenThe.Text = "0";
            this.lblTenThe.Visible = false;
            // 
            // lblNgayLap
            // 
            this.lblNgayLap.AutoSize = true;
            this.lblNgayLap.Font = new System.Drawing.Font("Segoe UI", 10.2F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblNgayLap.Location = new System.Drawing.Point(449, 100);
            this.lblNgayLap.Name = "lblNgayLap";
            this.lblNgayLap.Size = new System.Drawing.Size(82, 23);
            this.lblNgayLap.TabIndex = 0;
            this.lblNgayLap.Text = "Ngày lập:";
            // 
            // dtpNgayLap
            // 
            this.dtpNgayLap.CustomFormat = "MM/dd/yyyy   HH:mm tt";
            this.dtpNgayLap.Font = new System.Drawing.Font("Segoe UI", 10.2F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.dtpNgayLap.Format = System.Windows.Forms.DateTimePickerFormat.Custom;
            this.dtpNgayLap.Location = new System.Drawing.Point(535, 94);
            this.dtpNgayLap.Name = "dtpNgayLap";
            this.dtpNgayLap.Size = new System.Drawing.Size(221, 30);
            this.dtpNgayLap.TabIndex = 3;
            // 
            // pictureBox1
            // 
            this.pictureBox1.Image = ((System.Drawing.Image)(resources.GetObject("pictureBox1.Image")));
            this.pictureBox1.Location = new System.Drawing.Point(11, 20);
            this.pictureBox1.Name = "pictureBox1";
            this.pictureBox1.Size = new System.Drawing.Size(150, 137);
            this.pictureBox1.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.pictureBox1.TabIndex = 5;
            this.pictureBox1.TabStop = false;
            // 
            // spbtnTHem
            // 
            this.spbtnTHem.Appearance.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(112)))), ((int)(((byte)(195)))), ((int)(((byte)(245)))));
            this.spbtnTHem.Appearance.Font = new System.Drawing.Font("Segoe UI", 10.2F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.spbtnTHem.Appearance.Options.UseBackColor = true;
            this.spbtnTHem.Appearance.Options.UseFont = true;
            this.spbtnTHem.ButtonStyle = DevExpress.XtraEditors.Controls.BorderStyles.Style3D;
            this.spbtnTHem.Location = new System.Drawing.Point(477, 173);
            this.spbtnTHem.Name = "spbtnTHem";
            this.spbtnTHem.Size = new System.Drawing.Size(89, 43);
            this.spbtnTHem.TabIndex = 4;
            this.spbtnTHem.Text = "Thêm";
            this.spbtnTHem.Click += new System.EventHandler(this.spbtnTHem_Click);
            // 
            // spbtnXoa
            // 
            this.spbtnXoa.Appearance.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(112)))), ((int)(((byte)(195)))), ((int)(((byte)(245)))));
            this.spbtnXoa.Appearance.Font = new System.Drawing.Font("Segoe UI", 10.2F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.spbtnXoa.Appearance.Options.UseBackColor = true;
            this.spbtnXoa.Appearance.Options.UseFont = true;
            this.spbtnXoa.ButtonStyle = DevExpress.XtraEditors.Controls.BorderStyles.Style3D;
            this.spbtnXoa.Enabled = false;
            this.spbtnXoa.Location = new System.Drawing.Point(572, 173);
            this.spbtnXoa.Name = "spbtnXoa";
            this.spbtnXoa.Size = new System.Drawing.Size(89, 43);
            this.spbtnXoa.TabIndex = 5;
            this.spbtnXoa.Text = "Xóa";
            this.spbtnXoa.Click += new System.EventHandler(this.spbtnXoa_Click);
            // 
            // spbtnSua
            // 
            this.spbtnSua.Appearance.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(112)))), ((int)(((byte)(195)))), ((int)(((byte)(245)))));
            this.spbtnSua.Appearance.Font = new System.Drawing.Font("Segoe UI", 10.2F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.spbtnSua.Appearance.Options.UseBackColor = true;
            this.spbtnSua.Appearance.Options.UseFont = true;
            this.spbtnSua.ButtonStyle = DevExpress.XtraEditors.Controls.BorderStyles.Style3D;
            this.spbtnSua.Enabled = false;
            this.spbtnSua.Location = new System.Drawing.Point(667, 173);
            this.spbtnSua.Name = "spbtnSua";
            this.spbtnSua.Size = new System.Drawing.Size(89, 43);
            this.spbtnSua.TabIndex = 6;
            this.spbtnSua.Text = "Sửa";
            this.spbtnSua.Click += new System.EventHandler(this.spbtnSua_Click);
            // 
            // TheKH
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(8F, 20F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(112)))), ((int)(((byte)(195)))), ((int)(((byte)(245)))));
            this.ClientSize = new System.Drawing.Size(763, 503);
            this.Controls.Add(this.spbtnSua);
            this.Controls.Add(this.spbtnXoa);
            this.Controls.Add(this.spbtnTHem);
            this.Controls.Add(this.pictureBox1);
            this.Controls.Add(this.dtpNgayLap);
            this.Controls.Add(this.dgvTheKH);
            this.Controls.Add(this.txtSDT);
            this.Controls.Add(this.txtHoTen);
            this.Controls.Add(this.lblNgayLap);
            this.Controls.Add(this.lblSDT);
            this.Controls.Add(this.lblTenThe);
            this.Controls.Add(this.lblMaThe);
            this.Controls.Add(this.lblHoTen);
            this.Font = new System.Drawing.Font("Segoe UI", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.Name = "TheKH";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "Thẻ khách hàng";
            this.Load += new System.EventHandler(this.TheKH_Load);
            ((System.ComponentModel.ISupportInitialize)(this.dgvTheKH)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox1)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Label lblHoTen;
        private System.Windows.Forms.Label lblSDT;
        private System.Windows.Forms.TextBox txtHoTen;
        private System.Windows.Forms.TextBox txtSDT;
        private System.Windows.Forms.DataGridView dgvTheKH;
        private System.Windows.Forms.Label lblMaThe;
        private System.Windows.Forms.Label lblTenThe;
        private System.Windows.Forms.Label lblNgayLap;
        private System.Windows.Forms.DateTimePicker dtpNgayLap;
        private System.Windows.Forms.PictureBox pictureBox1;
        private DevExpress.XtraEditors.SimpleButton spbtnTHem;
        private DevExpress.XtraEditors.SimpleButton spbtnXoa;
        private DevExpress.XtraEditors.SimpleButton spbtnSua;
        private System.Windows.Forms.DataGridViewTextBoxColumn MaThe;
        private System.Windows.Forms.DataGridViewTextBoxColumn TenKH;
        private System.Windows.Forms.DataGridViewTextBoxColumn SDT;
        private System.Windows.Forms.DataGridViewTextBoxColumn Ngaylapthe;
        private System.Windows.Forms.DataGridViewButtonColumn clmXoa;
    }
}