﻿namespace QuanLyQuanKem
{
    partial class GoiMon
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(GoiMon));
            this.acdCEBingsu = new DevExpress.XtraBars.Navigation.AccordionControlElement();
            this.acdCEKemCombo = new DevExpress.XtraBars.Navigation.AccordionControlElement();
            this.acdCEKemLy = new DevExpress.XtraBars.Navigation.AccordionControlElement();
            this.acdCEKemCay = new DevExpress.XtraBars.Navigation.AccordionControlElement();
            this.accordionControl1 = new DevExpress.XtraBars.Navigation.AccordionControl();
            this.acdCETrangChinh = new DevExpress.XtraBars.Navigation.AccordionControlElement();
            this.flpKem = new System.Windows.Forms.FlowLayoutPanel();
            this.panel1 = new System.Windows.Forms.Panel();
            this.txtMaThe = new System.Windows.Forms.TextBox();
            this.dgvDatMon = new System.Windows.Forms.DataGridView();
            this.lblthanhtien_float = new System.Windows.Forms.Label();
            this.lblUuDai = new System.Windows.Forms.Label();
            this.lblTongTien = new System.Windows.Forms.Label();
            this.lblThanhTien = new System.Windows.Forms.Label();
            this.lblTenNV = new System.Windows.Forms.Label();
            this.lblBan = new System.Windows.Forms.Label();
            this.spbtnDatBan = new DevExpress.XtraEditors.SimpleButton();
            this.lblPhanTram = new System.Windows.Forms.Label();
            this.lbluudai_float = new System.Windows.Forms.Label();
            this.spbtnNhapMaThe = new DevExpress.XtraEditors.SimpleButton();
            this.spbtnTheKH = new DevExpress.XtraEditors.SimpleButton();
            this.spbtnThanhToan = new DevExpress.XtraEditors.SimpleButton();
            this.lbltongtien_float = new System.Windows.Forms.Label();
            this.clmTenMon = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.clmSoLuong = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.clmDonGia = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.clmButtonXoa = new System.Windows.Forms.DataGridViewButtonColumn();
            this.clmMaMH = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.clmTenLoai = new System.Windows.Forms.DataGridViewTextBoxColumn();
            ((System.ComponentModel.ISupportInitialize)(this.accordionControl1)).BeginInit();
            this.panel1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dgvDatMon)).BeginInit();
            this.SuspendLayout();
            // 
            // acdCEBingsu
            // 
            this.acdCEBingsu.Name = "acdCEBingsu";
            this.acdCEBingsu.Style = DevExpress.XtraBars.Navigation.ElementStyle.Item;
            this.acdCEBingsu.Text = "Bingsu";
            this.acdCEBingsu.Click += new System.EventHandler(this.acdCEBingsu_Click);
            // 
            // acdCEKemCombo
            // 
            this.acdCEKemCombo.Name = "acdCEKemCombo";
            this.acdCEKemCombo.Style = DevExpress.XtraBars.Navigation.ElementStyle.Item;
            this.acdCEKemCombo.Text = "Kem Combo";
            this.acdCEKemCombo.Click += new System.EventHandler(this.acdCEKemCombo_Click);
            // 
            // acdCEKemLy
            // 
            this.acdCEKemLy.Name = "acdCEKemLy";
            this.acdCEKemLy.Style = DevExpress.XtraBars.Navigation.ElementStyle.Item;
            this.acdCEKemLy.Text = "Kem ly - Kem dĩa";
            this.acdCEKemLy.Click += new System.EventHandler(this.acdCEKemLy_Click);
            // 
            // acdCEKemCay
            // 
            this.acdCEKemCay.Name = "acdCEKemCay";
            this.acdCEKemCay.Style = DevExpress.XtraBars.Navigation.ElementStyle.Item;
            this.acdCEKemCay.Text = "Kem cây";
            this.acdCEKemCay.Click += new System.EventHandler(this.acdCEKemCay_Click);
            // 
            // accordionControl1
            // 
            this.accordionControl1.AllowItemSelection = true;
            this.accordionControl1.AnimationType = DevExpress.XtraBars.Navigation.AnimationType.Spline;
            this.accordionControl1.Appearance.Item.Hovered.Font = new System.Drawing.Font("Segoe UI", 10.2F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.accordionControl1.Appearance.Item.Hovered.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(112)))), ((int)(((byte)(195)))), ((int)(((byte)(245)))));
            this.accordionControl1.Appearance.Item.Hovered.Options.UseFont = true;
            this.accordionControl1.Appearance.Item.Hovered.Options.UseForeColor = true;
            this.accordionControl1.Appearance.Item.Normal.Font = new System.Drawing.Font("Segoe UI", 10.2F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.accordionControl1.Appearance.Item.Normal.ForeColor = System.Drawing.Color.Black;
            this.accordionControl1.Appearance.Item.Normal.Options.UseFont = true;
            this.accordionControl1.Appearance.Item.Normal.Options.UseForeColor = true;
            this.accordionControl1.Appearance.Item.Pressed.Font = new System.Drawing.Font("Segoe UI", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.accordionControl1.Appearance.Item.Pressed.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(112)))), ((int)(((byte)(195)))), ((int)(((byte)(245)))));
            this.accordionControl1.Appearance.Item.Pressed.Options.UseFont = true;
            this.accordionControl1.Appearance.Item.Pressed.Options.UseForeColor = true;
            this.accordionControl1.Dock = System.Windows.Forms.DockStyle.Left;
            this.accordionControl1.Elements.AddRange(new DevExpress.XtraBars.Navigation.AccordionControlElement[] {
            this.acdCETrangChinh,
            this.acdCEKemCay,
            this.acdCEKemLy,
            this.acdCEKemCombo,
            this.acdCEBingsu});
            this.accordionControl1.Location = new System.Drawing.Point(0, 0);
            this.accordionControl1.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.accordionControl1.Name = "accordionControl1";
            this.accordionControl1.Size = new System.Drawing.Size(230, 513);
            this.accordionControl1.TabIndex = 6;
            this.accordionControl1.Text = "accordionControl1";
            // 
            // acdCETrangChinh
            // 
            this.acdCETrangChinh.Name = "acdCETrangChinh";
            this.acdCETrangChinh.Style = DevExpress.XtraBars.Navigation.ElementStyle.Item;
            this.acdCETrangChinh.Text = "Trang chính";
            this.acdCETrangChinh.Click += new System.EventHandler(this.acdCETrangChinh_Click);
            // 
            // flpKem
            // 
            this.flpKem.AutoScroll = true;
            this.flpKem.BackColor = System.Drawing.Color.Transparent;
            this.flpKem.Location = new System.Drawing.Point(230, 0);
            this.flpKem.Name = "flpKem";
            this.flpKem.Size = new System.Drawing.Size(462, 513);
            this.flpKem.TabIndex = 15;
            // 
            // panel1
            // 
            this.panel1.BackColor = System.Drawing.Color.Transparent;
            this.panel1.Controls.Add(this.txtMaThe);
            this.panel1.Controls.Add(this.dgvDatMon);
            this.panel1.Controls.Add(this.lblthanhtien_float);
            this.panel1.Controls.Add(this.lblUuDai);
            this.panel1.Controls.Add(this.lblTongTien);
            this.panel1.Controls.Add(this.lblThanhTien);
            this.panel1.Controls.Add(this.lblTenNV);
            this.panel1.Controls.Add(this.lblBan);
            this.panel1.Controls.Add(this.spbtnDatBan);
            this.panel1.Controls.Add(this.lblPhanTram);
            this.panel1.Controls.Add(this.lbluudai_float);
            this.panel1.Controls.Add(this.spbtnNhapMaThe);
            this.panel1.Controls.Add(this.spbtnTheKH);
            this.panel1.Controls.Add(this.spbtnThanhToan);
            this.panel1.Controls.Add(this.lbltongtien_float);
            this.panel1.Location = new System.Drawing.Point(692, 0);
            this.panel1.Name = "panel1";
            this.panel1.Size = new System.Drawing.Size(365, 513);
            this.panel1.TabIndex = 16;
            // 
            // txtMaThe
            // 
            this.txtMaThe.Font = new System.Drawing.Font("Segoe UI", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtMaThe.ForeColor = System.Drawing.Color.LightGray;
            this.txtMaThe.Location = new System.Drawing.Point(35, 415);
            this.txtMaThe.Name = "txtMaThe";
            this.txtMaThe.Size = new System.Drawing.Size(100, 27);
            this.txtMaThe.TabIndex = 0;
            this.txtMaThe.Text = "Nhập mã thẻ";
            this.txtMaThe.Visible = false;
            this.txtMaThe.TextChanged += new System.EventHandler(this.txtMaThe_TextChanged);
            this.txtMaThe.Enter += new System.EventHandler(this.txtMaThe_Enter);
            this.txtMaThe.Leave += new System.EventHandler(this.txtMaThe_Leave);
            // 
            // dgvDatMon
            // 
            this.dgvDatMon.AllowUserToAddRows = false;
            this.dgvDatMon.AllowUserToDeleteRows = false;
            this.dgvDatMon.BackgroundColor = System.Drawing.Color.FromArgb(((int)(((byte)(112)))), ((int)(((byte)(195)))), ((int)(((byte)(245)))));
            this.dgvDatMon.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dgvDatMon.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.clmTenMon,
            this.clmSoLuong,
            this.clmDonGia,
            this.clmButtonXoa,
            this.clmMaMH,
            this.clmTenLoai});
            this.dgvDatMon.Location = new System.Drawing.Point(0, 56);
            this.dgvDatMon.Name = "dgvDatMon";
            this.dgvDatMon.ReadOnly = true;
            this.dgvDatMon.RowHeadersVisible = false;
            this.dgvDatMon.RowHeadersWidth = 22;
            this.dgvDatMon.RowTemplate.Height = 24;
            this.dgvDatMon.Size = new System.Drawing.Size(365, 276);
            this.dgvDatMon.TabIndex = 38;
            this.dgvDatMon.CellClick += new System.Windows.Forms.DataGridViewCellEventHandler(this.dgvDatMon_CellClick);
            this.dgvDatMon.RowStateChanged += new System.Windows.Forms.DataGridViewRowStateChangedEventHandler(this.dgvDatMon_RowStateChanged);
            // 
            // lblthanhtien_float
            // 
            this.lblthanhtien_float.AutoSize = true;
            this.lblthanhtien_float.Font = new System.Drawing.Font("Segoe UI", 10.2F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblthanhtien_float.Location = new System.Drawing.Point(276, 345);
            this.lblthanhtien_float.Name = "lblthanhtien_float";
            this.lblthanhtien_float.Size = new System.Drawing.Size(19, 23);
            this.lblthanhtien_float.TabIndex = 35;
            this.lblthanhtien_float.Text = "0";
            // 
            // lblUuDai
            // 
            this.lblUuDai.AutoSize = true;
            this.lblUuDai.Font = new System.Drawing.Font("Segoe UI", 10.2F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblUuDai.Location = new System.Drawing.Point(147, 382);
            this.lblUuDai.Name = "lblUuDai";
            this.lblUuDai.Size = new System.Drawing.Size(68, 23);
            this.lblUuDai.TabIndex = 32;
            this.lblUuDai.Text = "Ưu đãi:";
            // 
            // lblTongTien
            // 
            this.lblTongTien.AutoSize = true;
            this.lblTongTien.Font = new System.Drawing.Font("Segoe UI", 10.2F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblTongTien.Location = new System.Drawing.Point(147, 419);
            this.lblTongTien.Name = "lblTongTien";
            this.lblTongTien.Size = new System.Drawing.Size(92, 23);
            this.lblTongTien.TabIndex = 33;
            this.lblTongTien.Text = "Tổng tiền:";
            // 
            // lblThanhTien
            // 
            this.lblThanhTien.AutoSize = true;
            this.lblThanhTien.Font = new System.Drawing.Font("Segoe UI", 10.2F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblThanhTien.Location = new System.Drawing.Point(147, 345);
            this.lblThanhTien.Name = "lblThanhTien";
            this.lblThanhTien.Size = new System.Drawing.Size(100, 23);
            this.lblThanhTien.TabIndex = 34;
            this.lblThanhTien.Text = "Thành tiền:";
            // 
            // lblTenNV
            // 
            this.lblTenNV.AutoSize = true;
            this.lblTenNV.Font = new System.Drawing.Font("Segoe UI", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblTenNV.Location = new System.Drawing.Point(303, 0);
            this.lblTenNV.Name = "lblTenNV";
            this.lblTenNV.Size = new System.Drawing.Size(56, 20);
            this.lblTenNV.TabIndex = 31;
            this.lblTenNV.Text = "Tên NV";
            // 
            // lblBan
            // 
            this.lblBan.AutoSize = true;
            this.lblBan.Font = new System.Drawing.Font("Segoe UI", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblBan.Location = new System.Drawing.Point(16, 11);
            this.lblBan.Name = "lblBan";
            this.lblBan.Size = new System.Drawing.Size(48, 28);
            this.lblBan.TabIndex = 31;
            this.lblBan.Text = "Bàn";
            // 
            // spbtnDatBan
            // 
            this.spbtnDatBan.Appearance.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(112)))), ((int)(((byte)(195)))), ((int)(((byte)(245)))));
            this.spbtnDatBan.Appearance.Font = new System.Drawing.Font("Segoe UI", 10.2F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.spbtnDatBan.Appearance.Options.UseBackColor = true;
            this.spbtnDatBan.Appearance.Options.UseFont = true;
            this.spbtnDatBan.ButtonStyle = DevExpress.XtraEditors.Controls.BorderStyles.Flat;
            this.spbtnDatBan.Location = new System.Drawing.Point(35, 455);
            this.spbtnDatBan.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.spbtnDatBan.Name = "spbtnDatBan";
            this.spbtnDatBan.Size = new System.Drawing.Size(94, 45);
            this.spbtnDatBan.TabIndex = 28;
            this.spbtnDatBan.Text = "Đặt bàn";
            this.spbtnDatBan.Click += new System.EventHandler(this.spbtnDatBan_Click);
            // 
            // lblPhanTram
            // 
            this.lblPhanTram.AutoSize = true;
            this.lblPhanTram.Font = new System.Drawing.Font("Segoe UI", 10.2F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblPhanTram.Location = new System.Drawing.Point(296, 382);
            this.lblPhanTram.Name = "lblPhanTram";
            this.lblPhanTram.Size = new System.Drawing.Size(24, 23);
            this.lblPhanTram.TabIndex = 36;
            this.lblPhanTram.Text = "%";
            // 
            // lbluudai_float
            // 
            this.lbluudai_float.AutoSize = true;
            this.lbluudai_float.Font = new System.Drawing.Font("Segoe UI", 10.2F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lbluudai_float.Location = new System.Drawing.Point(276, 382);
            this.lbluudai_float.Name = "lbluudai_float";
            this.lbluudai_float.Size = new System.Drawing.Size(19, 23);
            this.lbluudai_float.TabIndex = 36;
            this.lbluudai_float.Text = "0";
            // 
            // spbtnNhapMaThe
            // 
            this.spbtnNhapMaThe.Appearance.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(112)))), ((int)(((byte)(195)))), ((int)(((byte)(245)))));
            this.spbtnNhapMaThe.Appearance.Font = new System.Drawing.Font("Segoe UI", 10.2F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.spbtnNhapMaThe.Appearance.Options.UseBackColor = true;
            this.spbtnNhapMaThe.Appearance.Options.UseFont = true;
            this.spbtnNhapMaThe.ButtonStyle = DevExpress.XtraEditors.Controls.BorderStyles.Flat;
            this.spbtnNhapMaThe.Enabled = false;
            this.spbtnNhapMaThe.Location = new System.Drawing.Point(35, 406);
            this.spbtnNhapMaThe.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.spbtnNhapMaThe.Name = "spbtnNhapMaThe";
            this.spbtnNhapMaThe.Size = new System.Drawing.Size(94, 45);
            this.spbtnNhapMaThe.TabIndex = 29;
            this.spbtnNhapMaThe.Text = "Nhập mã thẻ";
            this.spbtnNhapMaThe.Click += new System.EventHandler(this.spbtnNhapMaThe_Click);
            // 
            // spbtnTheKH
            // 
            this.spbtnTheKH.Appearance.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(112)))), ((int)(((byte)(195)))), ((int)(((byte)(245)))));
            this.spbtnTheKH.Appearance.Font = new System.Drawing.Font("Segoe UI", 10.2F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.spbtnTheKH.Appearance.Options.UseBackColor = true;
            this.spbtnTheKH.Appearance.Options.UseFont = true;
            this.spbtnTheKH.ButtonStyle = DevExpress.XtraEditors.Controls.BorderStyles.Flat;
            this.spbtnTheKH.Enabled = false;
            this.spbtnTheKH.Location = new System.Drawing.Point(235, 455);
            this.spbtnTheKH.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.spbtnTheKH.Name = "spbtnTheKH";
            this.spbtnTheKH.Size = new System.Drawing.Size(94, 45);
            this.spbtnTheKH.TabIndex = 29;
            this.spbtnTheKH.Text = "Thẻ KH";
            this.spbtnTheKH.Click += new System.EventHandler(this.spbtnTheKH_Click);
            // 
            // spbtnThanhToan
            // 
            this.spbtnThanhToan.Appearance.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(112)))), ((int)(((byte)(195)))), ((int)(((byte)(245)))));
            this.spbtnThanhToan.Appearance.Font = new System.Drawing.Font("Segoe UI", 10.2F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.spbtnThanhToan.Appearance.Options.UseBackColor = true;
            this.spbtnThanhToan.Appearance.Options.UseFont = true;
            this.spbtnThanhToan.ButtonStyle = DevExpress.XtraEditors.Controls.BorderStyles.Flat;
            this.spbtnThanhToan.Location = new System.Drawing.Point(135, 455);
            this.spbtnThanhToan.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.spbtnThanhToan.Name = "spbtnThanhToan";
            this.spbtnThanhToan.Size = new System.Drawing.Size(94, 45);
            this.spbtnThanhToan.TabIndex = 30;
            this.spbtnThanhToan.Text = "Thanh toán";
            this.spbtnThanhToan.Click += new System.EventHandler(this.spbtnThanhToan_Click);
            // 
            // lbltongtien_float
            // 
            this.lbltongtien_float.AutoSize = true;
            this.lbltongtien_float.Font = new System.Drawing.Font("Segoe UI", 10.2F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lbltongtien_float.Location = new System.Drawing.Point(276, 419);
            this.lbltongtien_float.Name = "lbltongtien_float";
            this.lbltongtien_float.Size = new System.Drawing.Size(19, 23);
            this.lbltongtien_float.TabIndex = 37;
            this.lbltongtien_float.Text = "0";
            // 
            // clmTenMon
            // 
            this.clmTenMon.HeaderText = "Tên món";
            this.clmTenMon.Name = "clmTenMon";
            this.clmTenMon.ReadOnly = true;
            this.clmTenMon.Width = 150;
            // 
            // clmSoLuong
            // 
            this.clmSoLuong.HeaderText = "SL";
            this.clmSoLuong.Name = "clmSoLuong";
            this.clmSoLuong.ReadOnly = true;
            this.clmSoLuong.Width = 56;
            // 
            // clmDonGia
            // 
            this.clmDonGia.HeaderText = "Đơn giá";
            this.clmDonGia.Name = "clmDonGia";
            this.clmDonGia.ReadOnly = true;
            this.clmDonGia.Width = 120;
            // 
            // clmButtonXoa
            // 
            this.clmButtonXoa.HeaderText = "";
            this.clmButtonXoa.Name = "clmButtonXoa";
            this.clmButtonXoa.ReadOnly = true;
            this.clmButtonXoa.Text = "Xóa";
            this.clmButtonXoa.UseColumnTextForButtonValue = true;
            this.clmButtonXoa.Width = 36;
            // 
            // clmMaMH
            // 
            this.clmMaMH.HeaderText = "MaMH";
            this.clmMaMH.Name = "clmMaMH";
            this.clmMaMH.ReadOnly = true;
            this.clmMaMH.Visible = false;
            this.clmMaMH.Width = 60;
            // 
            // clmTenLoai
            // 
            this.clmTenLoai.HeaderText = "Loai";
            this.clmTenLoai.Name = "clmTenLoai";
            this.clmTenLoai.ReadOnly = true;
            this.clmTenLoai.Visible = false;
            this.clmTenLoai.Width = 150;
            // 
            // GoiMon
            // 
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Inherit;
            this.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(112)))), ((int)(((byte)(195)))), ((int)(((byte)(245)))));
            this.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.ClientSize = new System.Drawing.Size(1057, 513);
            this.Controls.Add(this.panel1);
            this.Controls.Add(this.flpKem);
            this.Controls.Add(this.accordionControl1);
            this.Font = new System.Drawing.Font("Segoe UI", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.Name = "GoiMon";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "Gọi món";
            this.Load += new System.EventHandler(this.GoiMon_Load);
            this.Click += new System.EventHandler(this.GoiMon_Click);
            ((System.ComponentModel.ISupportInitialize)(this.accordionControl1)).EndInit();
            this.panel1.ResumeLayout(false);
            this.panel1.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dgvDatMon)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        private DevExpress.XtraBars.Navigation.AccordionControlElement acdCEBingsu;
        private DevExpress.XtraBars.Navigation.AccordionControlElement acdCEKemCombo;
        private DevExpress.XtraBars.Navigation.AccordionControlElement acdCEKemLy;
        private DevExpress.XtraBars.Navigation.AccordionControlElement acdCEKemCay;
        private DevExpress.XtraBars.Navigation.AccordionControl accordionControl1;
        private System.Windows.Forms.FlowLayoutPanel flpKem;
        private DevExpress.XtraBars.Navigation.AccordionControlElement acdCETrangChinh;
        private System.Windows.Forms.Panel panel1;
        private System.Windows.Forms.DataGridView dgvDatMon;
        private System.Windows.Forms.Label lblthanhtien_float;
        private System.Windows.Forms.Label lblUuDai;
        private System.Windows.Forms.Label lblTongTien;
        private System.Windows.Forms.Label lblThanhTien;
        private System.Windows.Forms.Label lblBan;
        private System.Windows.Forms.Label lbluudai_float;
        private DevExpress.XtraEditors.SimpleButton spbtnTheKH;
        private DevExpress.XtraEditors.SimpleButton spbtnThanhToan;
        private System.Windows.Forms.Label lbltongtien_float;
        private System.Windows.Forms.Label lblPhanTram;
        private DevExpress.XtraEditors.SimpleButton spbtnDatBan;
        private System.Windows.Forms.Label lblTenNV;
        private System.Windows.Forms.TextBox txtMaThe;
        private DevExpress.XtraEditors.SimpleButton spbtnNhapMaThe;
        private System.Windows.Forms.DataGridViewTextBoxColumn clmTenMon;
        private System.Windows.Forms.DataGridViewTextBoxColumn clmSoLuong;
        private System.Windows.Forms.DataGridViewTextBoxColumn clmDonGia;
        private System.Windows.Forms.DataGridViewButtonColumn clmButtonXoa;
        private System.Windows.Forms.DataGridViewTextBoxColumn clmMaMH;
        private System.Windows.Forms.DataGridViewTextBoxColumn clmTenLoai;
    }
}