﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Data;

namespace QuanLyQuanKem.Classes
{
    public class btnBan
    {
        private byte maban;
        private string tenban;
        private string trangthai;
        private string phut;
        public btnBan(byte maban,string tenban,string trangthai,string phut)
        {
            this.Maban = maban;
            this.Tenban = tenban;
            this.Trangthai = trangthai;
            this.Phut = phut;
        }
        public btnBan(DataRow row)
        {
            this.Maban = (byte)row["Maban"];
            this.Tenban = row["TenBan"].ToString();
            this.Trangthai = row["TrangThai"].ToString();
            this.Phut = row["Phut"].ToString();
        }
        public string Phut
        {
            get { return phut; }
            set { phut = value; }
        }
        public string Trangthai
        {
            get { return trangthai; }
            set { trangthai = value; }
        }
        public string Tenban
        {
            get { return tenban; }
            set { tenban = value; }
        }
        public byte Maban
        {
            get { return maban; }
            set { maban = value; }
        }
    }
}
