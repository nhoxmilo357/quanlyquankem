﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace QuanLyQuanKem.Classes
{
    public class btnKem
    {
        private string mamh;
        private string tenmh;
        private int dongia;
        private int maloai;
        private string tenloai;
        public btnKem(string mamh, string tenmh, int dongia, int maloai, string tenloai)
        {
            this.Mamh = mamh;
            this.Tenmh = tenmh;
            this.Dongia = dongia;
            this.Maloai = maloai;
            this.Tenloai = tenloai;
        }
        public btnKem(DataRow riw)
        {
            this.Mamh = riw["MaMH"].ToString();
            this.Tenmh = riw["TenMH"].ToString();
            this.Dongia = (int)riw["DonGia"];
            this.Maloai = (byte)riw["MaLoai"];
            this.Tenloai = riw["TenLoai"].ToString();
        }
        public string Tenloai
        {
            get { return tenloai; }
            set { tenloai = value; }
        }
        public int Maloai
        {
            get { return maloai; }
            set { maloai = value; }
        }
        public int Dongia
        {
            get { return dongia; }
            set { dongia = value; }
        }
        public string Tenmh
        {
            get { return tenmh; }
            set { tenmh = value; }
        }
        public string Mamh
        {
            get { return mamh; }
            set { mamh = value; }
        }      
    }
}
