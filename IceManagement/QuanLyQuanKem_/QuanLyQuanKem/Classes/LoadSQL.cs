﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Data;

namespace QuanLyQuanKem.Classes
{
    public class LoadSQL
    {
        database db;
        DataTable data;
        public LoadSQL()
        {
            db = new database();
        }

        #region Update
        //update trạng thái bàn
        public void update1DangDung(string maban,string trangthai)
        {
            string sql = string.Format("update Ban set trangthai=N'"+trangthai+"' where maban=" + maban + "");
            db.TruyVan(sql);
        }
        public void updateALLTrong()
        {
            string sql = string.Format("update ban set trangthai=N'Trống' where trangthai=N'Đang dùng'");
            db.TruyVan(sql);
        }

        
        #endregion

        #region Load dữ liệu lên
        //Load Bàn
        public List<btnBan> loadtablelist()
        {
            List<btnBan> tl = new List<btnBan>();
            string sql = "select * from BAN";
            data = db.TruyVan(sql);
            foreach (DataRow row in data.Rows)
            {
                btnBan table = new btnBan(row);
                tl.Add(table);
            }
            return tl;
        }

        //Load Kem
        public List<btnKem> loadKem(string a)
        {
            List<btnKem> tl = new List<btnKem>();
            string sql = a;
            data = db.TruyVan(sql);
            foreach (DataRow row in data.Rows)
            {
                btnKem table = new btnKem(row);
                tl.Add(table);
            }
            return tl;
        }

        //Lấy ngày bắt đầu,ngày kết thúc (theo dạng MM/dd/yyyy) và ưu đãi
        public DataTable layDichVu()
        {
            string sql = "select convert(varchar,ngaybd,101),convert(varchar,ngaykt,101),uudai from DichVu";
            data = db.TruyVan(sql);
            return data;
        }       

        //Lấy vai trò
        public DataTable layvt()
        {
            string sql = "select * from vaitro";
            data = db.TruyVan(sql);
            return data;
        }
      
        #endregion

        #region Doanh thu
        //Thêm vào bảng Doanh Thu
        public void insertDoanhThu(string mamh,string tenmh,string sl,string dongia,string loai)
        {
            string sql = "insert into DOANHTHUTHEOMHANG (mamh,tenmh,slban,dongiatb,loai) values("+mamh+",N'"+tenmh+"',"+sl+","+dongia+",N'"+loai+"')";
            db.Lenh(sql);
        }

        //Lấy thông số doanh thu các mặt hàng đã bán
        public DataTable layDoanhThu()
        {
            string sql = "select tenmh,sum(slban),sum(dongiatb),Loai from DOANHTHUTHEOMHANG where TenMH=TenMH group by TenMH,Loai";
            data = db.TruyVan(sql);
            return data;
        }
        #endregion 

        #region Hóa đơn
        //Lấy mã hóa đơn lớn nhất trong bảng Hóa Đơn
        public DataTable laymahdmax()
        {
            string sql = "select max(MaHD) as maxmahd from HOADON";
            data = db.TruyVan(sql);
            return data;
        }

        //Lấy hóa đơn
        public DataTable layhoadon()
        {
            string sql = "select * from hoadon";
            data = db.TruyVan(sql);
            return data;
        }

        //Thêm hóa đơn
        public void insertHOADON(string maban, string ngay, string manv)
        {
            string sql = "insert into HOADON(maban,ngaylap,manv) values(" + maban + ",'" + ngay + "','" + manv + "')";
            db.Lenh(sql);
        } 
        #endregion

        #region Chi tiết hóa đơn
        //Lấy chi tiết hóa đơn
        public DataTable layCTHD()
        {
            string sql = "select * from chitiethoadon";
            data = db.TruyVan(sql);
            return data;
        }

        //Lấy chi tiết hóa đơn theo mã hóa đơn
        public DataTable layhd(string ma)
        {
            string sql = "select * from chitiethoadon where mahd=" + ma;
            data = db.TruyVan(sql);
            return data;
        }

        //Thêm chi tiết hóa đơn
        public void insertChiTietHD(string mahd, string tenban, string mamh, string tenmh, string sl, string gia, string thanhtien, string ud, string tt, string kt, string tienthoi, string tennv, string ngaylap)
        {
            string sql = "insert into chitiethoadon(mahd,tenban,mamh,tenmh,soluong,gia,thanhtien,uudai,tongtien,khachtra,tienthua,tennv,ngaylap) values(" + mahd + ",N'" + tenban + "'," + mamh + ",N'" + tenmh + "'," + sl + "," + gia + ","
                + thanhtien + ",'" + ud + "'," + tt + "," + kt + "," + tienthoi + ",N'" + tennv + "','" + ngaylap + "')";
            db.Lenh(sql);
        }
        #endregion

        #region Thẻ khách hàng
        //Lấy thẻ
        public DataTable layMaThe()
        {
            string sql = "select mathe,tenkh,sdt,ngaylapthe,a.tendv from thekh a,dichvu b where a.tendv=b.tendv";
            data = db.TruyVan(sql);
            return data;
        }

        //Thêm thẻ khách hàng
        public void insertTheKH(string tenkh, string sdt, string ngaylapthe)
        {
            string sql = "insert into THEKH(tenkh,sdt,ngaylapthe) values(N'" + tenkh + "'," + "'" + sdt + "','" + ngaylapthe + "')";
            db.Lenh(sql);
        }

        //Update thẻ khách hàng
        public void updateTheKH(string mathe, string tenkh, string sdt, string ngay)
        {
            string sql = string.Format("update thekh set tenkh=N'" + tenkh + "',sdt='" + sdt + "',ngaylapthe='" + ngay + "' where mathe=" + mathe);
            db.TruyVan(sql);
        }

        //Xóa thẻ
        public void xoaTheKH(string mathe)
        {
            string sql = string.Format("delete from thekh where mathe=" + mathe);
            db.Lenh(sql);
        }
        #endregion

        #region Quán lý nhân viên
        //Lấy toàn bộ table Nhân Viên
        public DataTable layNV()
        {
            string sql = "select * from nhanvien";
            data = db.TruyVan(sql);
            return data;
        }

        //Lấy "số lớn nhất + 1" trong mã nhân viên
        public DataTable laymaxMaNV(string ma)
        {
            string sql = "select Max(RIGHT(manv,1))+1 as manv from NHANVIEN where left(MaNV,2)='" + ma + "'";
            data = db.TruyVan(sql);
            return data;
        }

        //Thêm nhân viên
        public void insertNhanVien(string manv, string tennv, string gioitinh, string ngaysinh, string cmnd, string sdt, string diachi)
        {
            string sql = string.Format("insert into nhanvien values('{0}',N'{1}',N'{2}','{3}','{4}','{5}',N'{6}')", manv, tennv, gioitinh, ngaysinh, cmnd, sdt, diachi);
            db.Lenh(sql);
        }

        //Sửa thông tin nhân viên
        public void updateNV(string manv,string tennv,string gioitinh,string ngaysinh,string cmnd,string sdt,string diachi)
        {
            string sql = string.Format("update nhanvien set tennv=N'{0}',gioitinh=N'{1}',ngaysinh='{2}',cmnd='{3}',sdt='{4}',diachi=N'{5}' where manv='{6}'", tennv, gioitinh, ngaysinh, cmnd, sdt, diachi, manv);
            db.Lenh(sql);
        }

        //Xóa nhân viên
        public void deleteNV(string manv)
        {
            string sql = string.Format("delete from nhanvien where manv='" + manv + "'");
            db.TruyVan(sql);
        }
        #endregion 

        #region Đăng nhập - Đăng ký
        //Lấy thông tin tài khoản đăng nhập
        public DataTable laythongtindangnhap()
        {
            string sql = "select username,pass,tennv,VaiTro,a.mavaitro,a.manv from NGUOIDUNG a,NHANVIEN b,VaiTro c where a.MaNV=b.MaNV and a.Mavaitro=c.Mavaitro";
            data = db.TruyVan(sql);
            return data;
        }

        //Lấy mã và tên nhân viên thuộc Thu ngân và Quản lý
        public DataTable layMaTen()
        {
            string sql = "select manv,tennv from NHANVIEN where left(manv,2)='TN' or left(manv,2)='QL'";
            data = db.TruyVan(sql);
            return data;
        }

        //Đăng ký tài khoản nhân viên
        public void dangky(string tk,string mk,string manv,string mavt)
        {
            string sql = string.Format("insert into nguoidung values('{0}','{1}','{2}',{3})", tk, mk, manv, mavt);
            db.Lenh(sql);
        }
        #endregion
    }
}
