﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Data.SqlClient;
using System.Data;

namespace QuanLyQuanKem.Classes
{
    public class database
    {
        SqlConnection con;
        SqlDataAdapter da;
        public database()
        {
            string strcon = @"Data Source=DESKTOP-HES3LD0\SQLEXPRESS;Initial Catalog=DACNPM;Integrated Security=True";
            con = new SqlConnection(strcon);
        }
        public DataTable TruyVan(string query)
        {
            //con.Open();
            //SqlCommand cmd = new SqlCommand(query, con);
            DataTable dt = new DataTable();
            da = new SqlDataAdapter(query, con);
            da.Fill(dt);
            //con.Close();
            return dt;
        }
        public void Lenh(string query)
        {
            SqlCommand cmd = new SqlCommand(query, con);
            con.Open();
            cmd.ExecuteNonQuery();
            con.Close();
        }
    }
}
