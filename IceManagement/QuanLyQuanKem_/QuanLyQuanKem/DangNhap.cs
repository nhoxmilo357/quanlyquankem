﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using QuanLyQuanKem.Classes;

namespace QuanLyQuanKem
{
    public partial class DangNhap : Form
    {
        LoadSQL lsql = new LoadSQL();
        DataTable dt;
        public delegate void showtilecontrol(Boolean val,string mes,string mes1);
        public showtilecontrol sh;
        public delegate void visible(Boolean val,string mes,string mes1);
        public visible vs;
        public delegate void close();
        public close cl;
        public DangNhap()
        {
            InitializeComponent();
            dt = lsql.layvt();
            cmbVaiTro.DataSource = dt;
            cmbVaiTro.ValueMember = "mavaitro";
            cmbVaiTro.DisplayMember = "vaitro";
        }

        #region Di chuyển form
        Boolean flag; int x, y;
        private void pictureBox3_MouseDown(object sender, MouseEventArgs e)
        {
            flag = true;
            x = e.X;
            y = e.Y;
        }

        private void pictureBox3_MouseUp(object sender, MouseEventArgs e)
        {
            flag = false;
        }

        private void pictureBox3_MouseMove(object sender, MouseEventArgs e)
        {
            if (flag == true)
                this.SetDesktopLocation(Cursor.Position.X - x, Cursor.Position.Y - y);
        }
        #endregion

        #region textbox enter
        private void textBox2_Enter(object sender, EventArgs e)
        {
            txtTK.Text = "";
            txtTK.ForeColor = Color.Black;
        }
        private void textBox1_Enter(object sender, EventArgs e)
        {
            txtMK.Text = "";
            txtMK.ForeColor = Color.Black;
            txtMK.UseSystemPasswordChar = true;
        }
        #endregion

        //Form LOAD
        private void DangNhap_Load(object sender, EventArgs e)
        {
            vs.Invoke(false,"","");
        }

        //Simple Button Đăng nhập
        private void spbtnDangNhap_Click(object sender, EventArgs e)
        {
            dt = lsql.laythongtindangnhap();
            int flag = 0;
            foreach (DataRow row in dt.Rows)
            {
                if (txtTK.Text == row[0].ToString() && txtMK.Text == row[1].ToString() && row[4].ToString() == cmbVaiTro.SelectedValue.ToString())
                {
                    //Quản lý
                    vs.Invoke(true, row[2].ToString(), row[5].ToString());
                    if (row[3].ToString() == "Thu ngân")
                    {
                        //Thu ngân
                        sh.Invoke(true, row[2].ToString(), row[5].ToString());
                    }
                    this.Close();
                    flag = 1;
                    break;
                }
            }
            if (flag == 0)
            {
                MessageBox.Show("Đăng nhập thất bại");
            }          
        }

        //Simple Button Thoát
        private void spbtnThoat_Click(object sender, EventArgs e)
        {
            DialogResult dr = MessageBox.Show("Bạn muốn thoát đăng nhập?", "Thông báo", MessageBoxButtons.YesNo, MessageBoxIcon.Question);
            if (dr == DialogResult.Yes)
            {
                this.Close();
                cl.Invoke();
            }
            else
                this.Show();
        }


        //Nếu chọn vai trò 'Giữ xe' hoặc 'Phục vụ' sẽ không đăng nhập được
        private void cmbVaiTro_SelectedIndexChanged(object sender, EventArgs e)
        {
            if (cmbVaiTro.Text == "Phục vụ" || cmbVaiTro.Text == "Giữ xe")
            {
                MessageBox.Show("Vai trò này không được đăng nhập vào hệ thống!", "Thông báo", MessageBoxButtons.OK, MessageBoxIcon.Warning);
                spbtnDangNhap.Enabled = false;
            }
            else
                spbtnDangNhap.Enabled = true;
        }

        private void lilblDangKy_LinkClicked(object sender, LinkLabelLinkClickedEventArgs e)
        {
            DangKy frm = new DangKy();
            frm.ShowDialog();
        }
    }
}
