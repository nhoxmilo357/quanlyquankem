﻿namespace QuanLyQuanKem
{
    partial class DangNhap
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(DangNhap));
            this.labelControl2 = new DevExpress.XtraEditors.LabelControl();
            this.txtMK = new System.Windows.Forms.TextBox();
            this.txtTK = new System.Windows.Forms.TextBox();
            this.cmbVaiTro = new System.Windows.Forms.ComboBox();
            this.panel1 = new System.Windows.Forms.Panel();
            this.spbtnDangNhap = new DevExpress.XtraEditors.SimpleButton();
            this.spbtnThoat = new DevExpress.XtraEditors.SimpleButton();
            this.lilblDangKy = new System.Windows.Forms.LinkLabel();
            this.pictureBox1 = new System.Windows.Forms.PictureBox();
            this.pictureBox2 = new System.Windows.Forms.PictureBox();
            this.panel1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox2)).BeginInit();
            this.SuspendLayout();
            // 
            // labelControl2
            // 
            this.labelControl2.Appearance.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(112)))), ((int)(((byte)(195)))), ((int)(((byte)(245)))));
            this.labelControl2.Appearance.Font = new System.Drawing.Font("Segoe UI", 10.2F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.labelControl2.AutoSizeMode = DevExpress.XtraEditors.LabelAutoSizeMode.None;
            this.labelControl2.LineLocation = DevExpress.XtraEditors.LineLocation.Bottom;
            this.labelControl2.LineVisible = true;
            this.labelControl2.Location = new System.Drawing.Point(7, 19);
            this.labelControl2.Name = "labelControl2";
            this.labelControl2.Size = new System.Drawing.Size(58, 40);
            this.labelControl2.TabIndex = 0;
            this.labelControl2.Text = "Vai trò";
            // 
            // txtMK
            // 
            this.txtMK.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(112)))), ((int)(((byte)(195)))), ((int)(((byte)(245)))));
            this.txtMK.BorderStyle = System.Windows.Forms.BorderStyle.None;
            this.txtMK.Font = new System.Drawing.Font("Segoe UI", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtMK.ForeColor = System.Drawing.SystemColors.ControlText;
            this.txtMK.Location = new System.Drawing.Point(36, 130);
            this.txtMK.Name = "txtMK";
            this.txtMK.Size = new System.Drawing.Size(189, 27);
            this.txtMK.TabIndex = 3;
            this.txtMK.Text = "Mật khẩu";
            this.txtMK.Enter += new System.EventHandler(this.textBox1_Enter);
            // 
            // txtTK
            // 
            this.txtTK.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(112)))), ((int)(((byte)(195)))), ((int)(((byte)(245)))));
            this.txtTK.BorderStyle = System.Windows.Forms.BorderStyle.None;
            this.txtTK.Font = new System.Drawing.Font("Segoe UI", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtTK.ForeColor = System.Drawing.SystemColors.ControlText;
            this.txtTK.Location = new System.Drawing.Point(36, 84);
            this.txtTK.Name = "txtTK";
            this.txtTK.Size = new System.Drawing.Size(189, 27);
            this.txtTK.TabIndex = 2;
            this.txtTK.Text = "Tài khoản";
            this.txtTK.Enter += new System.EventHandler(this.textBox2_Enter);
            // 
            // cmbVaiTro
            // 
            this.cmbVaiTro.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(112)))), ((int)(((byte)(195)))), ((int)(((byte)(245)))));
            this.cmbVaiTro.Font = new System.Drawing.Font("Segoe UI", 10.2F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.cmbVaiTro.ForeColor = System.Drawing.SystemColors.ControlText;
            this.cmbVaiTro.FormattingEnabled = true;
            this.cmbVaiTro.Location = new System.Drawing.Point(71, 27);
            this.cmbVaiTro.Name = "cmbVaiTro";
            this.cmbVaiTro.Size = new System.Drawing.Size(154, 31);
            this.cmbVaiTro.TabIndex = 1;
            this.cmbVaiTro.SelectedIndexChanged += new System.EventHandler(this.cmbVaiTro_SelectedIndexChanged);
            // 
            // panel1
            // 
            this.panel1.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(112)))), ((int)(((byte)(195)))), ((int)(((byte)(245)))));
            this.panel1.Controls.Add(this.pictureBox1);
            this.panel1.Controls.Add(this.txtMK);
            this.panel1.Controls.Add(this.txtTK);
            this.panel1.Controls.Add(this.cmbVaiTro);
            this.panel1.Controls.Add(this.pictureBox2);
            this.panel1.Controls.Add(this.labelControl2);
            this.panel1.Location = new System.Drawing.Point(62, 41);
            this.panel1.Name = "panel1";
            this.panel1.Size = new System.Drawing.Size(235, 172);
            this.panel1.TabIndex = 0;
            // 
            // spbtnDangNhap
            // 
            this.spbtnDangNhap.Appearance.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(112)))), ((int)(((byte)(195)))), ((int)(((byte)(245)))));
            this.spbtnDangNhap.Appearance.Font = new System.Drawing.Font("Segoe UI", 10.2F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.spbtnDangNhap.Appearance.Options.UseBackColor = true;
            this.spbtnDangNhap.Appearance.Options.UseFont = true;
            this.spbtnDangNhap.AppearanceHovered.BackColor = System.Drawing.Color.SpringGreen;
            this.spbtnDangNhap.AppearanceHovered.Options.UseBackColor = true;
            this.spbtnDangNhap.ButtonStyle = DevExpress.XtraEditors.Controls.BorderStyles.NoBorder;
            this.spbtnDangNhap.Location = new System.Drawing.Point(62, 239);
            this.spbtnDangNhap.Name = "spbtnDangNhap";
            this.spbtnDangNhap.Size = new System.Drawing.Size(113, 46);
            this.spbtnDangNhap.TabIndex = 4;
            this.spbtnDangNhap.Text = "Đăng nhập";
            this.spbtnDangNhap.Click += new System.EventHandler(this.spbtnDangNhap_Click);
            // 
            // spbtnThoat
            // 
            this.spbtnThoat.Appearance.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(112)))), ((int)(((byte)(195)))), ((int)(((byte)(245)))));
            this.spbtnThoat.Appearance.Font = new System.Drawing.Font("Segoe UI", 10.2F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.spbtnThoat.Appearance.Options.UseBackColor = true;
            this.spbtnThoat.Appearance.Options.UseFont = true;
            this.spbtnThoat.AppearanceHovered.BackColor = System.Drawing.Color.IndianRed;
            this.spbtnThoat.AppearanceHovered.Options.UseBackColor = true;
            this.spbtnThoat.ButtonStyle = DevExpress.XtraEditors.Controls.BorderStyles.NoBorder;
            this.spbtnThoat.DialogResult = System.Windows.Forms.DialogResult.Cancel;
            this.spbtnThoat.Location = new System.Drawing.Point(184, 239);
            this.spbtnThoat.Name = "spbtnThoat";
            this.spbtnThoat.Size = new System.Drawing.Size(113, 46);
            this.spbtnThoat.TabIndex = 5;
            this.spbtnThoat.Text = "Thoát";
            this.spbtnThoat.Click += new System.EventHandler(this.spbtnThoat_Click);
            // 
            // lilblDangKy
            // 
            this.lilblDangKy.AutoSize = true;
            this.lilblDangKy.Font = new System.Drawing.Font("Segoe UI", 10.2F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lilblDangKy.Location = new System.Drawing.Point(118, 288);
            this.lilblDangKy.Name = "lilblDangKy";
            this.lilblDangKy.Size = new System.Drawing.Size(148, 23);
            this.lilblDangKy.TabIndex = 7;
            this.lilblDangKy.TabStop = true;
            this.lilblDangKy.Text = "Đăng ký tài khoản";
            this.lilblDangKy.LinkClicked += new System.Windows.Forms.LinkLabelLinkClickedEventHandler(this.lilblDangKy_LinkClicked);
            // 
            // pictureBox1
            // 
            this.pictureBox1.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(112)))), ((int)(((byte)(195)))), ((int)(((byte)(245)))));
            this.pictureBox1.Image = ((System.Drawing.Image)(resources.GetObject("pictureBox1.Image")));
            this.pictureBox1.Location = new System.Drawing.Point(7, 84);
            this.pictureBox1.Name = "pictureBox1";
            this.pictureBox1.Size = new System.Drawing.Size(27, 27);
            this.pictureBox1.TabIndex = 2;
            this.pictureBox1.TabStop = false;
            // 
            // pictureBox2
            // 
            this.pictureBox2.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(112)))), ((int)(((byte)(195)))), ((int)(((byte)(245)))));
            this.pictureBox2.Image = ((System.Drawing.Image)(resources.GetObject("pictureBox2.Image")));
            this.pictureBox2.Location = new System.Drawing.Point(7, 130);
            this.pictureBox2.Name = "pictureBox2";
            this.pictureBox2.Size = new System.Drawing.Size(27, 27);
            this.pictureBox2.TabIndex = 2;
            this.pictureBox2.TabStop = false;
            // 
            // DangNhap
            // 
            this.AcceptButton = this.spbtnDangNhap;
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Inherit;
            this.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(112)))), ((int)(((byte)(195)))), ((int)(((byte)(245)))));
            this.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.CancelButton = this.spbtnThoat;
            this.ClientSize = new System.Drawing.Size(362, 329);
            this.Controls.Add(this.lilblDangKy);
            this.Controls.Add(this.spbtnThoat);
            this.Controls.Add(this.spbtnDangNhap);
            this.Controls.Add(this.panel1);
            this.Font = new System.Drawing.Font("Segoe UI", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.None;
            this.Name = "DangNhap";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "Đăng nhập";
            this.Load += new System.EventHandler(this.DangNhap_Load);
            this.MouseDown += new System.Windows.Forms.MouseEventHandler(this.pictureBox3_MouseDown);
            this.MouseMove += new System.Windows.Forms.MouseEventHandler(this.pictureBox3_MouseMove);
            this.MouseUp += new System.Windows.Forms.MouseEventHandler(this.pictureBox3_MouseUp);
            this.panel1.ResumeLayout(false);
            this.panel1.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox2)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private DevExpress.XtraEditors.LabelControl labelControl2;
        private System.Windows.Forms.TextBox txtMK;
        private System.Windows.Forms.TextBox txtTK;
        private System.Windows.Forms.PictureBox pictureBox1;
        private System.Windows.Forms.PictureBox pictureBox2;
        private System.Windows.Forms.ComboBox cmbVaiTro;
        private System.Windows.Forms.Panel panel1;
        private DevExpress.XtraEditors.SimpleButton spbtnDangNhap;
        private DevExpress.XtraEditors.SimpleButton spbtnThoat;
        private System.Windows.Forms.LinkLabel lilblDangKy;
    }
}

