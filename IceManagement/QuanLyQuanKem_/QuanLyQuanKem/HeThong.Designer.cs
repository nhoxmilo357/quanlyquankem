﻿namespace QuanLyQuanKem
{
    partial class HeThong
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            DevExpress.XtraEditors.TileItemElement tileItemElement1 = new DevExpress.XtraEditors.TileItemElement();
            DevExpress.XtraEditors.TileItemElement tileItemElement2 = new DevExpress.XtraEditors.TileItemElement();
            DevExpress.XtraEditors.TileItemElement tileItemElement3 = new DevExpress.XtraEditors.TileItemElement();
            DevExpress.XtraEditors.TileItemElement tileItemElement4 = new DevExpress.XtraEditors.TileItemElement();
            DevExpress.XtraEditors.TileItemElement tileItemElement5 = new DevExpress.XtraEditors.TileItemElement();
            DevExpress.XtraEditors.TileItemElement tileItemElement6 = new DevExpress.XtraEditors.TileItemElement();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(HeThong));
            this.tileGroup1 = new DevExpress.XtraEditors.TileGroup();
            this.popupMenu1 = new DevExpress.XtraBars.PopupMenu(this.components);
            this.barbtnDangXuat = new DevExpress.XtraBars.BarButtonItem();
            this.barbtnThoat = new DevExpress.XtraBars.BarButtonItem();
            this.barManager1 = new DevExpress.XtraBars.BarManager(this.components);
            this.barDockControlTop = new DevExpress.XtraBars.BarDockControl();
            this.barDockControlBottom = new DevExpress.XtraBars.BarDockControl();
            this.barDockControlLeft = new DevExpress.XtraBars.BarDockControl();
            this.barDockControlRight = new DevExpress.XtraBars.BarDockControl();
            this.lblTenNV = new System.Windows.Forms.Label();
            this.ddbtnHeThong = new DevExpress.XtraEditors.DropDownButton();
            this.pbUser = new System.Windows.Forms.PictureBox();
            this.tlcHeThong = new DevExpress.XtraEditors.TileControl();
            this.tlgThuNgan = new DevExpress.XtraEditors.TileGroup();
            this.tliDatBan = new DevExpress.XtraEditors.TileItem();
            this.tliTheKH = new DevExpress.XtraEditors.TileItem();
            this.tliGoiMon = new DevExpress.XtraEditors.TileItem();
            this.tlgQuanLy = new DevExpress.XtraEditors.TileGroup();
            this.tliQLNV = new DevExpress.XtraEditors.TileItem();
            this.tliThongKe = new DevExpress.XtraEditors.TileItem();
            this.tileGroup3 = new DevExpress.XtraEditors.TileGroup();
            this.tliHelp = new DevExpress.XtraEditors.TileItem();
            this.lblMaNV = new System.Windows.Forms.Label();
            ((System.ComponentModel.ISupportInitialize)(this.popupMenu1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.barManager1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pbUser)).BeginInit();
            this.SuspendLayout();
            // 
            // tileGroup1
            // 
            this.tileGroup1.Name = "tileGroup1";
            // 
            // popupMenu1
            // 
            this.popupMenu1.LinksPersistInfo.AddRange(new DevExpress.XtraBars.LinkPersistInfo[] {
            new DevExpress.XtraBars.LinkPersistInfo(this.barbtnDangXuat),
            new DevExpress.XtraBars.LinkPersistInfo(this.barbtnThoat)});
            this.popupMenu1.Manager = this.barManager1;
            this.popupMenu1.Name = "popupMenu1";
            // 
            // barbtnDangXuat
            // 
            this.barbtnDangXuat.Caption = "Đăng xuất";
            this.barbtnDangXuat.Id = 1;
            this.barbtnDangXuat.ItemInMenuAppearance.Hovered.BackColor = System.Drawing.Color.Transparent;
            this.barbtnDangXuat.ItemInMenuAppearance.Hovered.Font = new System.Drawing.Font("Segoe UI", 10.2F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.barbtnDangXuat.ItemInMenuAppearance.Hovered.Options.UseBackColor = true;
            this.barbtnDangXuat.ItemInMenuAppearance.Hovered.Options.UseFont = true;
            this.barbtnDangXuat.ItemInMenuAppearance.Normal.BackColor = System.Drawing.Color.Transparent;
            this.barbtnDangXuat.ItemInMenuAppearance.Normal.Font = new System.Drawing.Font("Segoe UI", 7.8F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.barbtnDangXuat.ItemInMenuAppearance.Normal.Options.UseBackColor = true;
            this.barbtnDangXuat.ItemInMenuAppearance.Normal.Options.UseFont = true;
            this.barbtnDangXuat.Name = "barbtnDangXuat";
            this.barbtnDangXuat.ItemClick += new DevExpress.XtraBars.ItemClickEventHandler(this.barbtnDangXuat_ItemClick);
            // 
            // barbtnThoat
            // 
            this.barbtnThoat.Caption = "Thoát";
            this.barbtnThoat.Id = 0;
            this.barbtnThoat.ItemInMenuAppearance.Hovered.BackColor = System.Drawing.Color.Red;
            this.barbtnThoat.ItemInMenuAppearance.Hovered.Font = new System.Drawing.Font("Segoe UI", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.barbtnThoat.ItemInMenuAppearance.Hovered.Options.UseBackColor = true;
            this.barbtnThoat.ItemInMenuAppearance.Hovered.Options.UseFont = true;
            this.barbtnThoat.ItemInMenuAppearance.Normal.BackColor = System.Drawing.Color.Transparent;
            this.barbtnThoat.ItemInMenuAppearance.Normal.Font = new System.Drawing.Font("Segoe UI", 7.8F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.barbtnThoat.ItemInMenuAppearance.Normal.Options.UseBackColor = true;
            this.barbtnThoat.ItemInMenuAppearance.Normal.Options.UseFont = true;
            this.barbtnThoat.Name = "barbtnThoat";
            this.barbtnThoat.ItemClick += new DevExpress.XtraBars.ItemClickEventHandler(this.barbtnThoat_ItemClick);
            // 
            // barManager1
            // 
            this.barManager1.AllowGlyphSkinning = true;
            this.barManager1.DockControls.Add(this.barDockControlTop);
            this.barManager1.DockControls.Add(this.barDockControlBottom);
            this.barManager1.DockControls.Add(this.barDockControlLeft);
            this.barManager1.DockControls.Add(this.barDockControlRight);
            this.barManager1.Form = this;
            this.barManager1.Items.AddRange(new DevExpress.XtraBars.BarItem[] {
            this.barbtnThoat,
            this.barbtnDangXuat});
            this.barManager1.MaxItemId = 2;
            this.barManager1.TransparentEditors = true;
            // 
            // barDockControlTop
            // 
            this.barDockControlTop.CausesValidation = false;
            this.barDockControlTop.Dock = System.Windows.Forms.DockStyle.Top;
            this.barDockControlTop.Location = new System.Drawing.Point(0, 0);
            this.barDockControlTop.Size = new System.Drawing.Size(771, 0);
            // 
            // barDockControlBottom
            // 
            this.barDockControlBottom.CausesValidation = false;
            this.barDockControlBottom.Dock = System.Windows.Forms.DockStyle.Bottom;
            this.barDockControlBottom.Location = new System.Drawing.Point(0, 404);
            this.barDockControlBottom.Size = new System.Drawing.Size(771, 0);
            // 
            // barDockControlLeft
            // 
            this.barDockControlLeft.CausesValidation = false;
            this.barDockControlLeft.Dock = System.Windows.Forms.DockStyle.Left;
            this.barDockControlLeft.Location = new System.Drawing.Point(0, 0);
            this.barDockControlLeft.Size = new System.Drawing.Size(0, 404);
            // 
            // barDockControlRight
            // 
            this.barDockControlRight.CausesValidation = false;
            this.barDockControlRight.Dock = System.Windows.Forms.DockStyle.Right;
            this.barDockControlRight.Location = new System.Drawing.Point(771, 0);
            this.barDockControlRight.Size = new System.Drawing.Size(0, 404);
            // 
            // lblTenNV
            // 
            this.lblTenNV.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.lblTenNV.AutoSize = true;
            this.lblTenNV.BackColor = System.Drawing.Color.Transparent;
            this.lblTenNV.Font = new System.Drawing.Font("Segoe UI", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblTenNV.Location = new System.Drawing.Point(633, 24);
            this.lblTenNV.Name = "lblTenNV";
            this.lblTenNV.Size = new System.Drawing.Size(99, 20);
            this.lblTenNV.TabIndex = 17;
            this.lblTenNV.Text = "Tên nhân viên";
            // 
            // ddbtnHeThong
            // 
            this.ddbtnHeThong.AllowFocus = false;
            this.ddbtnHeThong.Appearance.BackColor = System.Drawing.Color.Transparent;
            this.ddbtnHeThong.Appearance.Options.UseBackColor = true;
            this.ddbtnHeThong.ButtonStyle = DevExpress.XtraEditors.Controls.BorderStyles.UltraFlat;
            this.ddbtnHeThong.DropDownControl = this.popupMenu1;
            this.ddbtnHeThong.Location = new System.Drawing.Point(736, 7);
            this.ddbtnHeThong.Name = "ddbtnHeThong";
            this.ddbtnHeThong.Size = new System.Drawing.Size(18, 41);
            this.ddbtnHeThong.TabIndex = 16;
            // 
            // pbUser
            // 
            this.pbUser.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.pbUser.BackColor = System.Drawing.Color.Transparent;
            this.pbUser.Image = ((System.Drawing.Image)(resources.GetObject("pbUser.Image")));
            this.pbUser.Location = new System.Drawing.Point(590, 16);
            this.pbUser.Name = "pbUser";
            this.pbUser.Size = new System.Drawing.Size(24, 26);
            this.pbUser.TabIndex = 15;
            this.pbUser.TabStop = false;
            // 
            // tlcHeThong
            // 
            this.tlcHeThong.AllowDragTilesBetweenGroups = false;
            this.tlcHeThong.AllowItemHover = true;
            this.tlcHeThong.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.tlcHeThong.AppearanceGroupText.Font = new System.Drawing.Font("Segoe UI", 19.8F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.tlcHeThong.AppearanceGroupText.Options.UseFont = true;
            this.tlcHeThong.AppearanceItem.Normal.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(112)))), ((int)(((byte)(195)))), ((int)(((byte)(245)))));
            this.tlcHeThong.AppearanceItem.Normal.Font = new System.Drawing.Font("Segoe UI", 10.2F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.tlcHeThong.AppearanceItem.Normal.ForeColor = System.Drawing.Color.Black;
            this.tlcHeThong.AppearanceItem.Normal.Options.UseBackColor = true;
            this.tlcHeThong.AppearanceItem.Normal.Options.UseFont = true;
            this.tlcHeThong.AppearanceItem.Normal.Options.UseForeColor = true;
            this.tlcHeThong.AppearanceText.Font = new System.Drawing.Font("Segoe UI", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.tlcHeThong.AppearanceText.Options.UseFont = true;
            this.tlcHeThong.BackColor = System.Drawing.Color.Transparent;
            this.tlcHeThong.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.tlcHeThong.DragSize = new System.Drawing.Size(0, 0);
            this.tlcHeThong.Groups.Add(this.tlgThuNgan);
            this.tlcHeThong.Groups.Add(this.tlgQuanLy);
            this.tlcHeThong.Groups.Add(this.tileGroup3);
            this.tlcHeThong.IndentBetweenGroups = 40;
            this.tlcHeThong.Location = new System.Drawing.Point(0, 54);
            this.tlcHeThong.MaxId = 33;
            this.tlcHeThong.Name = "tlcHeThong";
            this.tlcHeThong.ShowGroupText = true;
            this.tlcHeThong.Size = new System.Drawing.Size(771, 343);
            this.tlcHeThong.TabIndex = 14;
            this.tlcHeThong.Text = "tileControl1";
            // 
            // tlgThuNgan
            // 
            this.tlgThuNgan.Items.Add(this.tliDatBan);
            this.tlgThuNgan.Items.Add(this.tliTheKH);
            this.tlgThuNgan.Items.Add(this.tliGoiMon);
            this.tlgThuNgan.Name = "tlgThuNgan";
            this.tlgThuNgan.Text = "Thu ngân";
            // 
            // tliDatBan
            // 
            this.tliDatBan.BackgroundImage = ((System.Drawing.Image)(resources.GetObject("tliDatBan.BackgroundImage")));
            this.tliDatBan.BackgroundImageAlignment = DevExpress.XtraEditors.TileItemContentAlignment.MiddleCenter;
            tileItemElement1.Text = "Đặt Bàn";
            this.tliDatBan.Elements.Add(tileItemElement1);
            this.tliDatBan.Id = 27;
            this.tliDatBan.ItemSize = DevExpress.XtraEditors.TileItemSize.Large;
            this.tliDatBan.Name = "tliDatBan";
            this.tliDatBan.ItemClick += new DevExpress.XtraEditors.TileItemClickEventHandler(this.tliDatBan_ItemClick);
            // 
            // tliTheKH
            // 
            this.tliTheKH.BackgroundImage = ((System.Drawing.Image)(resources.GetObject("tliTheKH.BackgroundImage")));
            this.tliTheKH.BackgroundImageAlignment = DevExpress.XtraEditors.TileItemContentAlignment.MiddleCenter;
            tileItemElement2.Text = "Thẻ khách hàng";
            this.tliTheKH.Elements.Add(tileItemElement2);
            this.tliTheKH.Id = 28;
            this.tliTheKH.ItemSize = DevExpress.XtraEditors.TileItemSize.Large;
            this.tliTheKH.Name = "tliTheKH";
            this.tliTheKH.ItemClick += new DevExpress.XtraEditors.TileItemClickEventHandler(this.tliTheKH_ItemClick_1);
            // 
            // tliGoiMon
            // 
            this.tliGoiMon.BackgroundImage = ((System.Drawing.Image)(resources.GetObject("tliGoiMon.BackgroundImage")));
            this.tliGoiMon.BackgroundImageAlignment = DevExpress.XtraEditors.TileItemContentAlignment.MiddleCenter;
            tileItemElement3.Text = "Gọi món";
            this.tliGoiMon.Elements.Add(tileItemElement3);
            this.tliGoiMon.Id = 31;
            this.tliGoiMon.ItemSize = DevExpress.XtraEditors.TileItemSize.Large;
            this.tliGoiMon.Name = "tliGoiMon";
            this.tliGoiMon.ItemClick += new DevExpress.XtraEditors.TileItemClickEventHandler(this.tliGoiMon_ItemClick);
            // 
            // tlgQuanLy
            // 
            this.tlgQuanLy.Items.Add(this.tliQLNV);
            this.tlgQuanLy.Items.Add(this.tliThongKe);
            this.tlgQuanLy.Name = "tlgQuanLy";
            this.tlgQuanLy.Text = "Quản lý";
            // 
            // tliQLNV
            // 
            this.tliQLNV.BackgroundImage = ((System.Drawing.Image)(resources.GetObject("tliQLNV.BackgroundImage")));
            this.tliQLNV.BackgroundImageAlignment = DevExpress.XtraEditors.TileItemContentAlignment.MiddleCenter;
            tileItemElement4.Text = "Quản lý nhân viên";
            this.tliQLNV.Elements.Add(tileItemElement4);
            this.tliQLNV.Id = 29;
            this.tliQLNV.ItemSize = DevExpress.XtraEditors.TileItemSize.Large;
            this.tliQLNV.Name = "tliQLNV";
            this.tliQLNV.ItemClick += new DevExpress.XtraEditors.TileItemClickEventHandler(this.tliQLNV_ItemClick);
            // 
            // tliThongKe
            // 
            this.tliThongKe.BackgroundImage = ((System.Drawing.Image)(resources.GetObject("tliThongKe.BackgroundImage")));
            this.tliThongKe.BackgroundImageAlignment = DevExpress.XtraEditors.TileItemContentAlignment.MiddleCenter;
            tileItemElement5.Text = "Thống kê";
            this.tliThongKe.Elements.Add(tileItemElement5);
            this.tliThongKe.Id = 30;
            this.tliThongKe.ItemSize = DevExpress.XtraEditors.TileItemSize.Large;
            this.tliThongKe.Name = "tliThongKe";
            this.tliThongKe.ItemClick += new DevExpress.XtraEditors.TileItemClickEventHandler(this.tliThongKe_ItemClick);
            // 
            // tileGroup3
            // 
            this.tileGroup3.Items.Add(this.tliHelp);
            this.tileGroup3.Name = "tileGroup3";
            // 
            // tliHelp
            // 
            this.tliHelp.BackgroundImage = ((System.Drawing.Image)(resources.GetObject("tliHelp.BackgroundImage")));
            this.tliHelp.BackgroundImageAlignment = DevExpress.XtraEditors.TileItemContentAlignment.MiddleCenter;
            tileItemElement6.Text = "HELP";
            this.tliHelp.Elements.Add(tileItemElement6);
            this.tliHelp.Id = 32;
            this.tliHelp.ItemSize = DevExpress.XtraEditors.TileItemSize.Medium;
            this.tliHelp.Name = "tliHelp";
            this.tliHelp.ItemClick += new DevExpress.XtraEditors.TileItemClickEventHandler(this.tliHelp_ItemClick);
            // 
            // lblMaNV
            // 
            this.lblMaNV.AutoSize = true;
            this.lblMaNV.BackColor = System.Drawing.Color.Transparent;
            this.lblMaNV.Location = new System.Drawing.Point(481, 16);
            this.lblMaNV.Name = "lblMaNV";
            this.lblMaNV.Size = new System.Drawing.Size(97, 20);
            this.lblMaNV.TabIndex = 23;
            this.lblMaNV.Text = "mã nhân viên";
            this.lblMaNV.Visible = false;
            // 
            // HeThong
            // 
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Inherit;
            this.BackColor = System.Drawing.Color.PeachPuff;
            this.BackgroundImage = ((System.Drawing.Image)(resources.GetObject("$this.BackgroundImage")));
            this.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.ClientSize = new System.Drawing.Size(771, 404);
            this.Controls.Add(this.lblMaNV);
            this.Controls.Add(this.lblTenNV);
            this.Controls.Add(this.ddbtnHeThong);
            this.Controls.Add(this.pbUser);
            this.Controls.Add(this.tlcHeThong);
            this.Controls.Add(this.barDockControlLeft);
            this.Controls.Add(this.barDockControlRight);
            this.Controls.Add(this.barDockControlBottom);
            this.Controls.Add(this.barDockControlTop);
            this.Font = new System.Drawing.Font("Segoe UI", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.None;
            this.Name = "HeThong";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "Hệ thống";
            this.WindowState = System.Windows.Forms.FormWindowState.Maximized;
            this.Load += new System.EventHandler(this.HeThong_Load);
            ((System.ComponentModel.ISupportInitialize)(this.popupMenu1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.barManager1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pbUser)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private DevExpress.XtraEditors.TileGroup tileGroup1;
        private DevExpress.XtraBars.PopupMenu popupMenu1;
        private DevExpress.XtraBars.BarButtonItem barbtnThoat;
        private DevExpress.XtraBars.BarManager barManager1;
        private DevExpress.XtraBars.BarDockControl barDockControlTop;
        private DevExpress.XtraBars.BarDockControl barDockControlBottom;
        private DevExpress.XtraBars.BarDockControl barDockControlLeft;
        private DevExpress.XtraBars.BarDockControl barDockControlRight;
        private DevExpress.XtraBars.BarButtonItem barbtnDangXuat;
        private System.Windows.Forms.Label lblTenNV;
        private DevExpress.XtraEditors.DropDownButton ddbtnHeThong;
        private System.Windows.Forms.PictureBox pbUser;
        private DevExpress.XtraEditors.TileControl tlcHeThong;
        private DevExpress.XtraEditors.TileGroup tlgThuNgan;
        private DevExpress.XtraEditors.TileItem tliDatBan;
        private DevExpress.XtraEditors.TileItem tliTheKH;
        private DevExpress.XtraEditors.TileItem tliGoiMon;
        private DevExpress.XtraEditors.TileGroup tlgQuanLy;
        private DevExpress.XtraEditors.TileItem tliQLNV;
        private DevExpress.XtraEditors.TileItem tliThongKe;
        private DevExpress.XtraEditors.TileGroup tileGroup3;
        private DevExpress.XtraEditors.TileItem tliHelp;
        private System.Windows.Forms.Label lblMaNV;



    }
}