﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using QuanLyQuanKem.Classes;

namespace QuanLyQuanKem.FormQuanLy
{
    public partial class QLNhanVien : Form
    {
        LoadSQL lsql = new LoadSQL();
        DataTable dt;
        public QLNhanVien()
        {
            InitializeComponent();
        }

        //Set NUll
        void setNull()
        {
            cboVaiTro.Focus();
            lblTenMa.Text = "tên mã";
            txtCMND.Text = "";
            txtDiaChi.Text = "";
            txtHoTen.Text = "";
            txtSDT.Text = "";
            cboGioiTinh.Text = "";
            dtpNgaySinh.Text = DateTime.Now.ToString();
        }

        //Set Enabled
        void setEnabled(bool val)
        {
            cboVaiTro.Enabled = val;
            cboGioiTinh.Enabled = val;
            dtpNgaySinh.Enabled = val;
            txtHoTen.ReadOnly = !val;
            txtCMND.ReadOnly = !val;
            txtDiaChi.ReadOnly = !val;
            txtSDT.ReadOnly = !val;
        }

        //Hiển thị Listview
        void hienthiNV()
        {
            int i = 0;
            dt=lsql.layNV();
            foreach (DataRow row in dt.Rows)
            {
                i++;
                ListViewItem item = new ListViewItem(i.ToString());
                item.SubItems.Add(row[0].ToString());
                item.SubItems.Add(row[1].ToString());
                item.SubItems.Add(row[2].ToString());
                item.SubItems.Add(DateTime.Parse(row[3].ToString()).ToString("MM/dd/yyyy HH:mm tt"));
                item.SubItems.Add(row[4].ToString());
                item.SubItems.Add(row[5].ToString());
                item.SubItems.Add(row[6].ToString());
                lsvNV.Items.Add(item);
            }
        }    

        private void QLNhanVien_Load(object sender, EventArgs e)
        {
            //Combobox vai trò
            dt = lsql.layvt();
            cboVaiTro.DataSource = dt;
            cboVaiTro.DisplayMember = "vaitro";
            cboVaiTro.ValueMember = "mavaitro";

            //Combobox giới tính
            cboGioiTinh.Items.Add("Nam");
            cboGioiTinh.Items.Add("Nữ");

            //Hiển thị nhân viên
            hienthiNV();     
        }

        //Thiết lập mã nhân viên mới-tự động
        private void cboVaiTro_SelectedIndexChanged(object sender, EventArgs e)
        {
            if (cboVaiTro.Text == "Thu ngân")
            {
                string ma = "TN";
                dt = lsql.laymaxMaNV(ma);
            }
            if (cboVaiTro.Text == "Quản lý")
            {
                string ma = "QL";
                dt = lsql.laymaxMaNV(ma);
            }
            if (cboVaiTro.Text == "Phục vụ")
            {
                string ma = "PV";
                dt = lsql.laymaxMaNV(ma);
            }
            if (cboVaiTro.Text == "Giữ xe")
            {
                string ma = "GX";
                dt = lsql.laymaxMaNV(ma);
            }
            foreach (DataRow ro in dt.Rows)
            {
                lblTenMa.Text = ro[0].ToString();
                if (cboVaiTro.Text == "Thu ngân")
                {
                    string ma = "TN";
                    if (ro[0].ToString() == "")
                    {
                        ma = "TN1";
                        lblTenMa.Text = ma;
                    }
                    else
                        lblTenMa.Text = ma+=lblTenMa.Text;
                }
                if (cboVaiTro.Text == "Quản lý")
                {
                    string ma = "QL";
                    if (ro[0].ToString() == "")
                    {
                        ma = "QL1";
                        lblTenMa.Text = ma;
                    }
                    else
                        lblTenMa.Text = ma += lblTenMa.Text;
                }
                if (cboVaiTro.Text == "Phục vụ")
                {
                    string ma = "PV";
                    if (ro[0].ToString() == "")
                    {
                        ma = "PV1";
                        lblTenMa.Text = ma;
                    }
                    else
                        lblTenMa.Text = ma += lblTenMa.Text;
                }
                if (cboVaiTro.Text == "Giữ xe")
                {
                    string ma = "GX";
                    if (ro[0].ToString() == "")
                    {
                        ma = "GX1";
                        lblTenMa.Text = ma;
                    }
                    else
                        lblTenMa.Text = ma += lblTenMa.Text;
                }
            }
        }

        //Chọn dòng trong listview show thông tin lên 2 tab
        private void lsvNV_SelectedIndexChanged(object sender, EventArgs e)
        {           
            tpNV.SelectedPageIndex = 1;
            spbtnSua.Enabled = true;
            spbtnXoa.Enabled = true;
            for (int i = 0; i < lsvNV.SelectedItems.Count; i++)
            {
                //Tab Thông tin chung
                lblhoten_ttc.Text = lsvNV.SelectedItems[i].SubItems[2].Text;
                lblcmnd_ttc.Text = lsvNV.SelectedItems[i].SubItems[5].Text;
                lbldiachi_ttc.Text = lsvNV.SelectedItems[i].SubItems[7].Text;
                lblgioitinh_ttc.Text = lsvNV.SelectedItems[i].SubItems[3].Text;
                lblngaysinh_ttc.Text = lsvNV.SelectedItems[i].SubItems[4].Text;
                lblsdt_ttc.Text = lsvNV.SelectedItems[i].SubItems[6].Text;
                lblmanv_ttc.Text = lsvNV.SelectedItems[i].SubItems[1].Text;
                if (lblgioitinh_ttc.Text == "Nam")
                    btnPicture.ImageIndex = 0;
                else
                    btnPicture.ImageIndex = 2;

                //Tab Thêm nhân viên
                if (lsvNV.SelectedItems[i].SubItems[1].Text.Substring(0, 2) == "TN")
                    cboVaiTro.Text = "Thu ngân";
                if (lsvNV.SelectedItems[i].SubItems[1].Text.Substring(0, 2) == "QL")
                    cboVaiTro.Text = "Quản lý";
                if (lsvNV.SelectedItems[i].SubItems[1].Text.Substring(0, 2) == "PV")
                    cboVaiTro.Text = "Phục vụ";
                if (lsvNV.SelectedItems[i].SubItems[1].Text.Substring(0, 2) == "GX")
                    cboVaiTro.Text = "Giữ xe";
                txtHoTen.Text = lsvNV.SelectedItems[i].SubItems[2].Text.Trim();//Trim() loại bỏ hết khoảng trắng
                txtCMND.Text = lsvNV.SelectedItems[i].SubItems[5].Text.Trim();
                txtDiaChi.Text = lsvNV.SelectedItems[i].SubItems[7].Text;
                txtSDT.Text = lsvNV.SelectedItems[i].SubItems[6].Text.Trim();
                dtpNgaySinh.Text = lsvNV.SelectedItems[i].SubItems[4].Text;
                cboGioiTinh.Text = lsvNV.SelectedItems[i].SubItems[3].Text;
                lblTenMa.Text = lsvNV.SelectedItems[i].SubItems[1].Text;
            }          
        }

        #region Xử lý các BUTTON
        //Button mũi tên lên xuống
        private void btnLenXuong_Click(object sender, EventArgs e)
        {
            if (btnLenXuong.ImageIndex == 0)
            {
                tpNV.Height = 400;
                btnLenXuong.ImageIndex = 1;
            }
            else if (btnLenXuong.ImageIndex == 1)
            {
                tpNV.Height = 280;
                btnLenXuong.ImageIndex = 0;
            }
        }

        //Button Thêm
        private void spbtnThem_Click(object sender, EventArgs e)
        {
            dt = lsql.layNV();
            if (spbtnThem.Text == "Thêm")
            {
                spbtnThem.Text = "Lưu";
                setNull();
                setEnabled(true);
                spbtnSua.Enabled = false;
                spbtnXoa.Enabled = false;
            }
            else
            {
                if (txtHoTen.Text == "" || txtSDT.Text == "" || cboGioiTinh.Text == "")//Nếu họ tên,số điện thoại hoặc giới tính chưa nhập sẽ thông báo lỗi 
                    MessageBox.Show("Chưa điền đủ thông tin!", "Thông báo", MessageBoxButtons.OK, MessageBoxIcon.Stop);
                else
                {
                    foreach (DataRow row in dt.Rows)
                        if (lblTenMa.Text == "tên mã")//Nếu tên mã nhân viên trùng với mã nhân viên đã có sẽ thông báo lỗi
                            MessageBox.Show("Chọn lại vai trò!", "Thông báo", MessageBoxButtons.OK, MessageBoxIcon.Stop);
                        else
                        {
                            if (txtSDT.Text.Length < 10 || txtSDT.Text.Length > 11)//Nếu số điện thoại khác 10,11 chữ số sẽ báo lỗi
                            {
                                MessageBox.Show("Số điện thoại có 10 hoặc 11 chữ số!", "Thông báo", MessageBoxButtons.OK, MessageBoxIcon.Error);                                
                                break;
                            }
                            else if (txtCMND.Text.Length != 9)//Nếu số chứng minh khác 9 chữ số sẽ báo lỗi
                            {
                                MessageBox.Show("Số chứng minh thư có 9 chữ số!", "Thông báo", MessageBoxButtons.OK, MessageBoxIcon.Error);
                                break;
                            }
                            else
                            {
                                lsql.insertNhanVien(lblTenMa.Text, txtHoTen.Text, cboGioiTinh.Text, dtpNgaySinh.Text, txtCMND.Text, txtSDT.Text, txtDiaChi.Text);
                                spbtnThem.Text = "Thêm";
                                setNull();
                                setEnabled(false);
                                lsvNV.Items.Clear();
                                hienthiNV();
                                break;
                            }
                        }
                }
            }
        }

        //Button Sửa
        private void spbtnSua_Click(object sender, EventArgs e)
        {
            if(spbtnSua.Text=="Sửa")
            {
                setEnabled(true);
                spbtnSua.Text = "Lưu";
            }
            else
            {
                lsql.updateNV(lblTenMa.Text, txtHoTen.Text, cboGioiTinh.Text, dtpNgaySinh.Text, txtCMND.Text, txtSDT.Text, txtDiaChi.Text);
                setNull();
                setEnabled(false);
                spbtnSua.Enabled = false;
                spbtnXoa.Enabled = false;
                lsvNV.Items.Clear();
                hienthiNV();
            }
        }

        //Button Xóa
        private void spbtnXoa_Click(object sender, EventArgs e)
        {
            DialogResult dr = MessageBox.Show("Xóa nhân viên '" + txtHoTen.Text + "' mã '" + lblTenMa.Text+"'", "Thông báo", MessageBoxButtons.YesNo, MessageBoxIcon.Question);
            if(dr==DialogResult.Yes)
            {
                lsql.deleteNV(lblTenMa.Text);
                setNull();
                lsvNV.Items.Clear();
                hienthiNV();
            }
            else
            {
                spbtnSua.Enabled = false;
                spbtnXoa.Enabled = false;
                setNull();
            }
        }
        #endregion

        //Set null thông tin bên Tab Thông tin chung
        private void tpNV_MouseClick(object sender, MouseEventArgs e)
        {
            if(tpNV.SelectedPage==tnpThem)
            {
                lblmanv_ttc.Text = "";
                lblhoten_ttc.Text = "";
                lblngaysinh_ttc.Text = "";
                lblgioitinh_ttc.Text = "";
                lblcmnd_ttc.Text = "";
                lblsdt_ttc.Text = "";
                lbldiachi_ttc.Text = "";
                btnPicture.ImageIndex = 1;
            }
        }

        #region Bắt lỗi textbox
        //Nếu nhập chữ sẽ báo lỗi
        public bool IsNumber(string p)
        {
            foreach(Char c in p)
            {
                if (!Char.IsDigit(c))
                    MessageBox.Show("Nhập số!", "Thông báo", MessageBoxButtons.OK, MessageBoxIcon.Stop);
            }
            return true;
        }

        //Nếu nhập số sẽ báo lỗi
        public bool IsText(string p)
        {
            foreach (Char c in p)
            {
                if (Char.IsDigit(c))
                    MessageBox.Show("Không được nhập số vào họ tên!", "Thông báo", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
            return true;
        }

        //Bắt lỗi textbox số điện thoại
        private void txtSDT_TextChanged(object sender, EventArgs e)
        {
            IsNumber(txtSDT.Text);            
        }

        //Bắt lỗi textbox cmnd
        private void txtCMND_TextChanged(object sender, EventArgs e)
        {
            IsNumber(txtCMND.Text);
        }

        //Bắt lỗi textbox họ tên
        private void txtHoTen_TextChanged(object sender, EventArgs e)
        {
            IsText(txtHoTen.Text);
        }
        #endregion    
    }
}
