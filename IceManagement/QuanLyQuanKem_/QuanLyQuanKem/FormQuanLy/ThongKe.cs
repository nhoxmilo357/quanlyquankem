﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using QuanLyQuanKem.Classes;
using QuanLyQuanKem.Report;

namespace QuanLyQuanKem.FormQuanLy
{
    public partial class ThongKe : Form
    {
        LoadSQL lsql = new LoadSQL();
        DataTable dt;
        public ThongKe()
        {
            InitializeComponent();
        }
        private void ThongKe_Load(object sender, EventArgs e)
        {
            //Datagridview doanh thu theo mặt hàng
            dgvDOANHTHU.DataSource = lsql.layDoanhThu();
            dgvDOANHTHU.Font = new Font("Segeo UI", 10.2f);
            dgvDOANHTHU.Columns[0].HeaderText = "Tên mặt hàng";
            dgvDOANHTHU.Columns[0].Width = 270;
            dgvDOANHTHU.Columns[1].HeaderText = "Số lượng bán";
            dgvDOANHTHU.Columns[1].Width = 110;
            dgvDOANHTHU.Columns[2].HeaderText = "Tổng đơn giá";
            dgvDOANHTHU.Columns[2].Width = 190;
            dgvDOANHTHU.Columns[3].HeaderText = "Loại";
            dgvDOANHTHU.Columns[3].Width = 240;

            //Biểu đồ tròn
            chartControl1.Series[0].DataSource = lsql.layDoanhThu();
            chartControl1.Series[0].ArgumentDataMember = "tenmh";
            chartControl1.Series[0].ValueDataMembers.AddRange(new string[] {"column2"});
            //chartControl1.Series[0].BindToData(lsql.layDoanhThu(), "TenMH","[dongiatb]" );

            //Datagridview hóa đơn
            dgvLSHD.DataSource = lsql.layhoadon();
            dgvLSHD.Font = new Font("Segeo UI", 10.2f);
            dgvLSHD.Columns[0].HeaderText = "Mã hóa đơn";
            dgvLSHD.Columns[0].Width = 70;
            dgvLSHD.Columns[1].HeaderText = "Mã bàn";
            dgvLSHD.Columns[1].Width = 70;
            dgvLSHD.Columns[2].HeaderText = "Ngày lập";
            dgvLSHD.Columns[2].Width = 200;
            dgvLSHD.Columns[3].HeaderText = "Mã nhân viên";
            dgvLSHD.Columns[3].Width = 70;
            
            //Datagridview chi tiết hóa đơn
            dgvCTHD.DataSource = lsql.layCTHD();
            setColumnsWidth();
        }

        //Thiết lập tên và width của các columns trong gridview
        void setColumnsWidth()
        {
            dgvCTHD.Font = new Font("Segeo UI", 10.2f);
            dgvCTHD.Columns[0].HeaderText = "Mã hóa đơn";
            dgvCTHD.Columns[0].Width = 90;
            dgvCTHD.Columns[1].HeaderText = "Tên bàn";
            dgvCTHD.Columns[1].Width = 70;
            dgvCTHD.Columns[2].HeaderText = "Mã MH";
            dgvCTHD.Columns[2].Width = 80;
            dgvCTHD.Columns[3].HeaderText = "Tên mặt hàng";
            dgvCTHD.Columns[3].Width = 300;
            dgvCTHD.Columns[4].HeaderText = "Số lượng";
            dgvCTHD.Columns[4].Width = 70;
            dgvCTHD.Columns[5].HeaderText = "Giá";
            dgvCTHD.Columns[5].Width = 80;
            dgvCTHD.Columns[6].HeaderText = "Thành tiền";
            dgvCTHD.Columns[6].Width = 80;
            dgvCTHD.Columns[7].HeaderText = "Ưu đãi";
            dgvCTHD.Columns[7].Width = 60;
            dgvCTHD.Columns[8].HeaderText = "Tổng tiền";
            dgvCTHD.Columns[8].Width = 60;
            dgvCTHD.Columns[9].HeaderText = "Khách trả";
            dgvCTHD.Columns[9].Width = 60;
            dgvCTHD.Columns[10].HeaderText = "Tiền thừa";
            dgvCTHD.Columns[10].Width = 60;
            dgvCTHD.Columns[11].HeaderText = "Tên nhân viên";
            dgvCTHD.Columns[11].Width = 70;
        }

        //Hiện chi tiết hóa đơn theo hóa đơn được click
        private void dgvLSHD_CellClick(object sender, DataGridViewCellEventArgs e)
        {
            DataTable dt1 = lsql.layCTHD();
            int x = e.RowIndex;
                foreach(DataRow row in dt1.Rows)
                    if (dgvLSHD.Rows[x].Cells[0].Value.ToString()==row[0].ToString())
                    {
                        dt = lsql.layhd(dgvLSHD.Rows[x].Cells[0].Value.ToString());
                        dgvCTHD.DataSource = "";
                        dgvCTHD.DataSource = dt;
                        setColumnsWidth();
                    }
        }

        private void dgvLSHD_Leave(object sender, EventArgs e)
        {
            dgvCTHD.DataSource = lsql.layCTHD();
            setColumnsWidth();
        }

        #region Picturebox Help
        //Hiện các label hướng dẫn
        void setLabelVisible(Boolean val)
        {
            lblHelp.Text = "Chọn hóa đơn" + "\n" + "cần xem chi tiết";
            lblHoaDon.Visible = val;
            lblCTHD.Visible = val;
            lblHelp.Visible = val;
            pboxLeftArrow.Visible = val;
        }

        //Khi lướt vị trí chuột vào Pic
        private void pboxHelp_MouseHover(object sender, EventArgs e)
        {
            setLabelVisible(true);
        }

        //Khi rời vị trí chuột khỏi Pic
        private void pboxHelp_MouseLeave(object sender, EventArgs e)
        {
            setLabelVisible(false);
        }
        #endregion

        #region Buttons
        //Button report doanh thu
        private void btnReportDT_Click(object sender, EventArgs e)
        {
            ReportViewer frm = new ReportViewer();
            frm.ShowDialog();
        }

        //Button thu phóng
        private void btnThuPhong_Click_1(object sender, EventArgs e)
        {
            if (btnThuPhong.ImageIndex == 1)
            {
                chartControl1.Height = 528;
                btnThuPhong.Location = new Point(848, 505);
                btnThuPhong.ImageIndex = 0;
            }
            else
            {
                chartControl1.Height = 258;
                btnThuPhong.Location = new Point(848, 234);
                btnThuPhong.ImageIndex = 1;
            }
        }
        #endregion

    }
}
