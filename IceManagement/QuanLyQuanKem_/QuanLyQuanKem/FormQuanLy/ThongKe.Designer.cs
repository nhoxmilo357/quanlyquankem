﻿namespace QuanLyQuanKem.FormQuanLy
{
    partial class ThongKe
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(ThongKe));
            DevExpress.XtraCharts.Series series2 = new DevExpress.XtraCharts.Series();
            DevExpress.XtraCharts.PieSeriesLabel pieSeriesLabel2 = new DevExpress.XtraCharts.PieSeriesLabel();
            DevExpress.XtraCharts.PieSeriesView pieSeriesView2 = new DevExpress.XtraCharts.PieSeriesView();
            this.tabPane1 = new DevExpress.XtraBars.Navigation.TabPane();
            this.tabnavDTTMH = new DevExpress.XtraBars.Navigation.TabNavigationPage();
            this.btnThuPhong = new System.Windows.Forms.Button();
            this.imageList1 = new System.Windows.Forms.ImageList(this.components);
            this.chartControl1 = new DevExpress.XtraCharts.ChartControl();
            this.dgvDOANHTHU = new System.Windows.Forms.DataGridView();
            this.tabnavHOADON = new DevExpress.XtraBars.Navigation.TabNavigationPage();
            this.lblCTHD = new System.Windows.Forms.Label();
            this.lblHelp = new System.Windows.Forms.Label();
            this.lblHoaDon = new System.Windows.Forms.Label();
            this.pboxLeftArrow = new System.Windows.Forms.PictureBox();
            this.pboxHelp = new System.Windows.Forms.PictureBox();
            this.dgvLSHD = new System.Windows.Forms.DataGridView();
            this.dgvCTHD = new System.Windows.Forms.DataGridView();
            this.btnReportDT = new System.Windows.Forms.Button();
            ((System.ComponentModel.ISupportInitialize)(this.tabPane1)).BeginInit();
            this.tabPane1.SuspendLayout();
            this.tabnavDTTMH.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.chartControl1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(series2)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(pieSeriesLabel2)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(pieSeriesView2)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.dgvDOANHTHU)).BeginInit();
            this.tabnavHOADON.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pboxLeftArrow)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pboxHelp)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.dgvLSHD)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.dgvCTHD)).BeginInit();
            this.SuspendLayout();
            // 
            // tabPane1
            // 
            this.tabPane1.Appearance.BackColor = System.Drawing.Color.Peru;
            this.tabPane1.Appearance.Options.UseBackColor = true;
            this.tabPane1.AppearanceButton.Normal.BackColor = System.Drawing.Color.Transparent;
            this.tabPane1.AppearanceButton.Normal.Options.UseBackColor = true;
            this.tabPane1.Controls.Add(this.tabnavDTTMH);
            this.tabPane1.Controls.Add(this.tabnavHOADON);
            this.tabPane1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.tabPane1.Location = new System.Drawing.Point(0, 0);
            this.tabPane1.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.tabPane1.Name = "tabPane1";
            this.tabPane1.Pages.AddRange(new DevExpress.XtraBars.Navigation.NavigationPageBase[] {
            this.tabnavDTTMH,
            this.tabnavHOADON});
            this.tabPane1.RegularSize = new System.Drawing.Size(890, 573);
            this.tabPane1.SelectedPage = this.tabnavDTTMH;
            this.tabPane1.Size = new System.Drawing.Size(890, 573);
            this.tabPane1.TabIndex = 0;
            this.tabPane1.Text = "tabPane1";
            // 
            // tabnavDTTMH
            // 
            this.tabnavDTTMH.Appearance.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(112)))), ((int)(((byte)(195)))), ((int)(((byte)(245)))));
            this.tabnavDTTMH.Appearance.Options.UseBackColor = true;
            this.tabnavDTTMH.Caption = "Doanh thu theo mặt hàng";
            this.tabnavDTTMH.Controls.Add(this.btnReportDT);
            this.tabnavDTTMH.Controls.Add(this.btnThuPhong);
            this.tabnavDTTMH.Controls.Add(this.chartControl1);
            this.tabnavDTTMH.Controls.Add(this.dgvDOANHTHU);
            this.tabnavDTTMH.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.tabnavDTTMH.Name = "tabnavDTTMH";
            this.tabnavDTTMH.Size = new System.Drawing.Size(868, 516);
            // 
            // btnThuPhong
            // 
            this.btnThuPhong.BackColor = System.Drawing.Color.Transparent;
            this.btnThuPhong.ImageIndex = 1;
            this.btnThuPhong.ImageList = this.imageList1;
            this.btnThuPhong.Location = new System.Drawing.Point(848, 234);
            this.btnThuPhong.Name = "btnThuPhong";
            this.btnThuPhong.Size = new System.Drawing.Size(24, 24);
            this.btnThuPhong.TabIndex = 7;
            this.btnThuPhong.UseVisualStyleBackColor = false;
            this.btnThuPhong.Click += new System.EventHandler(this.btnThuPhong_Click_1);
            // 
            // imageList1
            // 
            this.imageList1.ImageStream = ((System.Windows.Forms.ImageListStreamer)(resources.GetObject("imageList1.ImageStream")));
            this.imageList1.TransparentColor = System.Drawing.Color.Transparent;
            this.imageList1.Images.SetKeyName(0, "chevron-up.png");
            this.imageList1.Images.SetKeyName(1, "thin-arrowheads-pointing-down.png");
            this.imageList1.Images.SetKeyName(2, "question.png");
            // 
            // chartControl1
            // 
            this.chartControl1.Dock = System.Windows.Forms.DockStyle.Top;
            this.chartControl1.Legend.Name = "Default Legend";
            this.chartControl1.Location = new System.Drawing.Point(0, 0);
            this.chartControl1.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.chartControl1.Name = "chartControl1";
            this.chartControl1.PaletteName = "Solstice";
            series2.ArgumentScaleType = DevExpress.XtraCharts.ScaleType.Qualitative;
            pieSeriesLabel2.FillStyle.FillMode = DevExpress.XtraCharts.FillMode.Empty;
            pieSeriesLabel2.LineVisibility = DevExpress.Utils.DefaultBoolean.True;
            pieSeriesLabel2.TextPattern = "{VP:0.00%}";
            series2.Label = pieSeriesLabel2;
            series2.LabelsVisibility = DevExpress.Utils.DefaultBoolean.True;
            series2.LegendTextPattern = "{A}--{V:0 VNĐ}";
            series2.Name = "Series 1";
            series2.View = pieSeriesView2;
            this.chartControl1.SeriesSerializable = new DevExpress.XtraCharts.Series[] {
        series2};
            this.chartControl1.Size = new System.Drawing.Size(868, 258);
            this.chartControl1.TabIndex = 6;
            // 
            // dgvDOANHTHU
            // 
            this.dgvDOANHTHU.AllowUserToAddRows = false;
            this.dgvDOANHTHU.AllowUserToDeleteRows = false;
            this.dgvDOANHTHU.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dgvDOANHTHU.Dock = System.Windows.Forms.DockStyle.Bottom;
            this.dgvDOANHTHU.Location = new System.Drawing.Point(0, 266);
            this.dgvDOANHTHU.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.dgvDOANHTHU.Name = "dgvDOANHTHU";
            this.dgvDOANHTHU.ReadOnly = true;
            this.dgvDOANHTHU.RowTemplate.Height = 24;
            this.dgvDOANHTHU.Size = new System.Drawing.Size(868, 250);
            this.dgvDOANHTHU.TabIndex = 5;
            // 
            // tabnavHOADON
            // 
            this.tabnavHOADON.Appearance.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(112)))), ((int)(((byte)(195)))), ((int)(((byte)(245)))));
            this.tabnavHOADON.Appearance.Options.UseBackColor = true;
            this.tabnavHOADON.Caption = "Lịch sử hóa đơn";
            this.tabnavHOADON.Controls.Add(this.lblCTHD);
            this.tabnavHOADON.Controls.Add(this.lblHelp);
            this.tabnavHOADON.Controls.Add(this.lblHoaDon);
            this.tabnavHOADON.Controls.Add(this.pboxLeftArrow);
            this.tabnavHOADON.Controls.Add(this.pboxHelp);
            this.tabnavHOADON.Controls.Add(this.dgvLSHD);
            this.tabnavHOADON.Controls.Add(this.dgvCTHD);
            this.tabnavHOADON.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.tabnavHOADON.Name = "tabnavHOADON";
            this.tabnavHOADON.Size = new System.Drawing.Size(868, 516);
            // 
            // lblCTHD
            // 
            this.lblCTHD.AutoSize = true;
            this.lblCTHD.Font = new System.Drawing.Font("Segoe UI", 10.2F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblCTHD.Location = new System.Drawing.Point(1, 229);
            this.lblCTHD.Name = "lblCTHD";
            this.lblCTHD.Size = new System.Drawing.Size(134, 23);
            this.lblCTHD.TabIndex = 3;
            this.lblCTHD.Text = "Chi tiết hóa đơn";
            this.lblCTHD.Visible = false;
            // 
            // lblHelp
            // 
            this.lblHelp.AutoSize = true;
            this.lblHelp.Font = new System.Drawing.Font("Segoe UI", 13.8F, System.Drawing.FontStyle.Italic, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblHelp.Location = new System.Drawing.Point(705, 80);
            this.lblHelp.Name = "lblHelp";
            this.lblHelp.Size = new System.Drawing.Size(332, 32);
            this.lblHelp.TabIndex = 3;
            this.lblHelp.Text = "Chọn hóa đơn cần xem chi tiết";
            this.lblHelp.Visible = false;
            // 
            // lblHoaDon
            // 
            this.lblHoaDon.AutoSize = true;
            this.lblHoaDon.Font = new System.Drawing.Font("Segoe UI", 10.2F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblHoaDon.Location = new System.Drawing.Point(137, 1);
            this.lblHoaDon.Name = "lblHoaDon";
            this.lblHoaDon.Size = new System.Drawing.Size(76, 23);
            this.lblHoaDon.TabIndex = 3;
            this.lblHoaDon.Text = "Hóa đơn";
            this.lblHoaDon.Visible = false;
            // 
            // pboxLeftArrow
            // 
            this.pboxLeftArrow.ErrorImage = null;
            this.pboxLeftArrow.Image = ((System.Drawing.Image)(resources.GetObject("pboxLeftArrow.Image")));
            this.pboxLeftArrow.ImageLocation = "";
            this.pboxLeftArrow.InitialImage = null;
            this.pboxLeftArrow.Location = new System.Drawing.Point(675, 97);
            this.pboxLeftArrow.Name = "pboxLeftArrow";
            this.pboxLeftArrow.Size = new System.Drawing.Size(24, 24);
            this.pboxLeftArrow.TabIndex = 2;
            this.pboxLeftArrow.TabStop = false;
            this.pboxLeftArrow.Visible = false;
            this.pboxLeftArrow.MouseLeave += new System.EventHandler(this.pboxHelp_MouseLeave);
            this.pboxLeftArrow.MouseHover += new System.EventHandler(this.pboxHelp_MouseHover);
            // 
            // pboxHelp
            // 
            this.pboxHelp.ErrorImage = null;
            this.pboxHelp.Image = ((System.Drawing.Image)(resources.GetObject("pboxHelp.Image")));
            this.pboxHelp.ImageLocation = "";
            this.pboxHelp.InitialImage = null;
            this.pboxHelp.Location = new System.Drawing.Point(848, 0);
            this.pboxHelp.Name = "pboxHelp";
            this.pboxHelp.Size = new System.Drawing.Size(24, 24);
            this.pboxHelp.TabIndex = 2;
            this.pboxHelp.TabStop = false;
            this.pboxHelp.MouseLeave += new System.EventHandler(this.pboxHelp_MouseLeave);
            this.pboxHelp.MouseHover += new System.EventHandler(this.pboxHelp_MouseHover);
            // 
            // dgvLSHD
            // 
            this.dgvLSHD.AllowUserToAddRows = false;
            this.dgvLSHD.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dgvLSHD.Location = new System.Drawing.Point(204, 0);
            this.dgvLSHD.Name = "dgvLSHD";
            this.dgvLSHD.ReadOnly = true;
            this.dgvLSHD.RowTemplate.Height = 24;
            this.dgvLSHD.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.FullRowSelect;
            this.dgvLSHD.Size = new System.Drawing.Size(451, 226);
            this.dgvLSHD.TabIndex = 1;
            this.dgvLSHD.CellClick += new System.Windows.Forms.DataGridViewCellEventHandler(this.dgvLSHD_CellClick);
            this.dgvLSHD.Leave += new System.EventHandler(this.dgvLSHD_Leave);
            // 
            // dgvCTHD
            // 
            this.dgvCTHD.AllowUserToAddRows = false;
            this.dgvCTHD.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dgvCTHD.Dock = System.Windows.Forms.DockStyle.Bottom;
            this.dgvCTHD.Location = new System.Drawing.Point(0, 244);
            this.dgvCTHD.Name = "dgvCTHD";
            this.dgvCTHD.ReadOnly = true;
            this.dgvCTHD.RowTemplate.Height = 24;
            this.dgvCTHD.Size = new System.Drawing.Size(868, 272);
            this.dgvCTHD.TabIndex = 0;
            // 
            // btnReportDT
            // 
            this.btnReportDT.Font = new System.Drawing.Font("Segoe UI", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnReportDT.Location = new System.Drawing.Point(773, 504);
            this.btnReportDT.Name = "btnReportDT";
            this.btnReportDT.Size = new System.Drawing.Size(75, 26);
            this.btnReportDT.TabIndex = 8;
            this.btnReportDT.Text = "Report";
            this.btnReportDT.UseVisualStyleBackColor = true;
            this.btnReportDT.Click += new System.EventHandler(this.btnReportDT_Click);
            // 
            // ThongKe
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(8F, 20F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.Color.Peru;
            this.ClientSize = new System.Drawing.Size(890, 573);
            this.Controls.Add(this.tabPane1);
            this.Font = new System.Drawing.Font("Segoe UI", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.Name = "ThongKe";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "Thống kê";
            this.Load += new System.EventHandler(this.ThongKe_Load);
            ((System.ComponentModel.ISupportInitialize)(this.tabPane1)).EndInit();
            this.tabPane1.ResumeLayout(false);
            this.tabnavDTTMH.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(pieSeriesLabel2)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(pieSeriesView2)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(series2)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.chartControl1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.dgvDOANHTHU)).EndInit();
            this.tabnavHOADON.ResumeLayout(false);
            this.tabnavHOADON.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pboxLeftArrow)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pboxHelp)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.dgvLSHD)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.dgvCTHD)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        private DevExpress.XtraBars.Navigation.TabPane tabPane1;
        private DevExpress.XtraBars.Navigation.TabNavigationPage tabnavDTTMH;
        private DevExpress.XtraBars.Navigation.TabNavigationPage tabnavHOADON;
        private System.Windows.Forms.DataGridView dgvCTHD;
        private System.Windows.Forms.ImageList imageList1;
        private System.Windows.Forms.DataGridView dgvLSHD;
        private DevExpress.XtraCharts.ChartControl chartControl1;
        private System.Windows.Forms.DataGridView dgvDOANHTHU;
        private System.Windows.Forms.Button btnThuPhong;
        private System.Windows.Forms.PictureBox pboxHelp;
        private System.Windows.Forms.Label lblHoaDon;
        private System.Windows.Forms.Label lblCTHD;
        private System.Windows.Forms.Label lblHelp;
        private System.Windows.Forms.PictureBox pboxLeftArrow;
        private System.Windows.Forms.Button btnReportDT;
    }
}