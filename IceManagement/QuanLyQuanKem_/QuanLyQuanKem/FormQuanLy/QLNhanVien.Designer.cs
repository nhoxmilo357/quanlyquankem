﻿namespace QuanLyQuanKem.FormQuanLy
{
    partial class QLNhanVien
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(QLNhanVien));
            this.lsvNV = new System.Windows.Forms.ListView();
            this.columnHeader1 = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.columnHeader2 = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.columnHeader3 = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.columnHeader4 = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.columnHeader6 = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.columnHeader5 = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.columnHeader7 = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.columnHeader8 = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.imageList1 = new System.Windows.Forms.ImageList(this.components);
            this.tpNV = new DevExpress.XtraBars.Navigation.TabPane();
            this.btnLenXuong = new System.Windows.Forms.Button();
            this.tnpThem = new DevExpress.XtraBars.Navigation.TabNavigationPage();
            this.spbtnXoa = new DevExpress.XtraEditors.SimpleButton();
            this.spbtnSua = new DevExpress.XtraEditors.SimpleButton();
            this.spbtnThem = new DevExpress.XtraEditors.SimpleButton();
            this.dtpNgaySinh = new System.Windows.Forms.DateTimePicker();
            this.cboVaiTro = new System.Windows.Forms.ComboBox();
            this.cboGioiTinh = new System.Windows.Forms.ComboBox();
            this.txtSDT = new System.Windows.Forms.TextBox();
            this.txtDiaChi = new System.Windows.Forms.TextBox();
            this.txtCMND = new System.Windows.Forms.TextBox();
            this.txtHoTen = new System.Windows.Forms.TextBox();
            this.lblVaiTro = new System.Windows.Forms.Label();
            this.lblGioiTinh = new System.Windows.Forms.Label();
            this.lblTenMa = new System.Windows.Forms.Label();
            this.lblMaNV = new System.Windows.Forms.Label();
            this.lblSDT = new System.Windows.Forms.Label();
            this.lblDiaChi = new System.Windows.Forms.Label();
            this.lblCMND = new System.Windows.Forms.Label();
            this.lblNgaySinh = new System.Windows.Forms.Label();
            this.lblHoTen = new System.Windows.Forms.Label();
            this.tnpThongTin = new DevExpress.XtraBars.Navigation.TabNavigationPage();
            this.btnPicture = new System.Windows.Forms.Button();
            this.imageList2 = new System.Windows.Forms.ImageList(this.components);
            this.lblgioitinh_ttc = new System.Windows.Forms.Label();
            this.lblGioiTinh_TT = new System.Windows.Forms.Label();
            this.lblsdt_ttc = new System.Windows.Forms.Label();
            this.lblSDT_TT = new System.Windows.Forms.Label();
            this.lbldiachi_ttc = new System.Windows.Forms.Label();
            this.lblDiaChi_TT = new System.Windows.Forms.Label();
            this.lblcmnd_ttc = new System.Windows.Forms.Label();
            this.lblCMND_TT = new System.Windows.Forms.Label();
            this.lblngaysinh_ttc = new System.Windows.Forms.Label();
            this.lblNgaySinh_TT = new System.Windows.Forms.Label();
            this.lblmanv_ttc = new System.Windows.Forms.Label();
            this.lblhoten_ttc = new System.Windows.Forms.Label();
            this.lblMaNV_TT = new System.Windows.Forms.Label();
            this.lblHoTen_TT = new System.Windows.Forms.Label();
            ((System.ComponentModel.ISupportInitialize)(this.tpNV)).BeginInit();
            this.tpNV.SuspendLayout();
            this.tnpThem.SuspendLayout();
            this.tnpThongTin.SuspendLayout();
            this.SuspendLayout();
            // 
            // lsvNV
            // 
            this.lsvNV.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(112)))), ((int)(((byte)(195)))), ((int)(((byte)(245)))));
            this.lsvNV.Columns.AddRange(new System.Windows.Forms.ColumnHeader[] {
            this.columnHeader1,
            this.columnHeader2,
            this.columnHeader3,
            this.columnHeader4,
            this.columnHeader6,
            this.columnHeader5,
            this.columnHeader7,
            this.columnHeader8});
            this.lsvNV.Dock = System.Windows.Forms.DockStyle.Top;
            this.lsvNV.Font = new System.Drawing.Font("Segoe UI", 10.2F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lsvNV.FullRowSelect = true;
            this.lsvNV.GridLines = true;
            this.lsvNV.Location = new System.Drawing.Point(0, 0);
            this.lsvNV.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.lsvNV.Name = "lsvNV";
            this.lsvNV.Size = new System.Drawing.Size(890, 249);
            this.lsvNV.TabIndex = 2;
            this.lsvNV.UseCompatibleStateImageBehavior = false;
            this.lsvNV.View = System.Windows.Forms.View.Details;
            this.lsvNV.SelectedIndexChanged += new System.EventHandler(this.lsvNV_SelectedIndexChanged);
            // 
            // columnHeader1
            // 
            this.columnHeader1.Text = "STT";
            this.columnHeader1.Width = 50;
            // 
            // columnHeader2
            // 
            this.columnHeader2.Text = "Mã NV";
            this.columnHeader2.Width = 70;
            // 
            // columnHeader3
            // 
            this.columnHeader3.Text = "Họ tên";
            this.columnHeader3.Width = 230;
            // 
            // columnHeader4
            // 
            this.columnHeader4.Text = "Giới tính";
            this.columnHeader4.Width = 80;
            // 
            // columnHeader6
            // 
            this.columnHeader6.Text = "Ngày sinh";
            this.columnHeader6.Width = 150;
            // 
            // columnHeader5
            // 
            this.columnHeader5.Text = "CMND";
            this.columnHeader5.Width = 80;
            // 
            // columnHeader7
            // 
            this.columnHeader7.Text = "SĐT";
            this.columnHeader7.Width = 100;
            // 
            // columnHeader8
            // 
            this.columnHeader8.Text = "Địa chỉ";
            this.columnHeader8.Width = 250;
            // 
            // imageList1
            // 
            this.imageList1.ImageStream = ((System.Windows.Forms.ImageListStreamer)(resources.GetObject("imageList1.ImageStream")));
            this.imageList1.TransparentColor = System.Drawing.Color.Transparent;
            this.imageList1.Images.SetKeyName(0, "chevron-up.png");
            this.imageList1.Images.SetKeyName(1, "thin-arrowheads-pointing-down.png");
            // 
            // tpNV
            // 
            this.tpNV.AllowGlyphSkinning = true;
            this.tpNV.AllowHtmlDraw = true;
            this.tpNV.AllowTransitionAnimation = DevExpress.Utils.DefaultBoolean.False;
            this.tpNV.Appearance.BackColor = System.Drawing.Color.Transparent;
            this.tpNV.Appearance.Options.UseBackColor = true;
            this.tpNV.Controls.Add(this.btnLenXuong);
            this.tpNV.Controls.Add(this.tnpThem);
            this.tpNV.Controls.Add(this.tnpThongTin);
            this.tpNV.Dock = System.Windows.Forms.DockStyle.Bottom;
            this.tpNV.Location = new System.Drawing.Point(0, 284);
            this.tpNV.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.tpNV.Name = "tpNV";
            this.tpNV.Pages.AddRange(new DevExpress.XtraBars.Navigation.NavigationPageBase[] {
            this.tnpThem,
            this.tnpThongTin});
            this.tpNV.RegularSize = new System.Drawing.Size(890, 280);
            this.tpNV.SelectedPage = this.tnpThem;
            this.tpNV.Size = new System.Drawing.Size(890, 280);
            this.tpNV.TabIndex = 7;
            this.tpNV.TransitionType = DevExpress.Utils.Animation.Transitions.Push;
            this.tpNV.MouseClick += new System.Windows.Forms.MouseEventHandler(this.tpNV_MouseClick);
            // 
            // btnLenXuong
            // 
            this.btnLenXuong.BackColor = System.Drawing.Color.Transparent;
            this.btnLenXuong.FlatAppearance.BorderSize = 0;
            this.btnLenXuong.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnLenXuong.ImageIndex = 0;
            this.btnLenXuong.ImageList = this.imageList1;
            this.btnLenXuong.Location = new System.Drawing.Point(870, 0);
            this.btnLenXuong.Name = "btnLenXuong";
            this.btnLenXuong.Size = new System.Drawing.Size(20, 23);
            this.btnLenXuong.TabIndex = 2;
            this.btnLenXuong.UseVisualStyleBackColor = false;
            this.btnLenXuong.Click += new System.EventHandler(this.btnLenXuong_Click);
            // 
            // tnpThem
            // 
            this.tnpThem.Appearance.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(112)))), ((int)(((byte)(195)))), ((int)(((byte)(245)))));
            this.tnpThem.Appearance.Options.UseBackColor = true;
            this.tnpThem.Caption = "Thêm nhân viên";
            this.tnpThem.Controls.Add(this.spbtnXoa);
            this.tnpThem.Controls.Add(this.spbtnSua);
            this.tnpThem.Controls.Add(this.spbtnThem);
            this.tnpThem.Controls.Add(this.dtpNgaySinh);
            this.tnpThem.Controls.Add(this.cboVaiTro);
            this.tnpThem.Controls.Add(this.cboGioiTinh);
            this.tnpThem.Controls.Add(this.txtSDT);
            this.tnpThem.Controls.Add(this.txtDiaChi);
            this.tnpThem.Controls.Add(this.txtCMND);
            this.tnpThem.Controls.Add(this.txtHoTen);
            this.tnpThem.Controls.Add(this.lblVaiTro);
            this.tnpThem.Controls.Add(this.lblGioiTinh);
            this.tnpThem.Controls.Add(this.lblTenMa);
            this.tnpThem.Controls.Add(this.lblMaNV);
            this.tnpThem.Controls.Add(this.lblSDT);
            this.tnpThem.Controls.Add(this.lblDiaChi);
            this.tnpThem.Controls.Add(this.lblCMND);
            this.tnpThem.Controls.Add(this.lblNgaySinh);
            this.tnpThem.Controls.Add(this.lblHoTen);
            this.tnpThem.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.tnpThem.Name = "tnpThem";
            this.tnpThem.Size = new System.Drawing.Size(868, 224);
            // 
            // spbtnXoa
            // 
            this.spbtnXoa.Appearance.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(112)))), ((int)(((byte)(195)))), ((int)(((byte)(245)))));
            this.spbtnXoa.Appearance.Font = new System.Drawing.Font("Segoe UI", 10.2F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.spbtnXoa.Appearance.Options.UseBackColor = true;
            this.spbtnXoa.Appearance.Options.UseFont = true;
            this.spbtnXoa.ButtonStyle = DevExpress.XtraEditors.Controls.BorderStyles.Style3D;
            this.spbtnXoa.Enabled = false;
            this.spbtnXoa.Location = new System.Drawing.Point(753, 147);
            this.spbtnXoa.Name = "spbtnXoa";
            this.spbtnXoa.Size = new System.Drawing.Size(75, 49);
            this.spbtnXoa.TabIndex = 10;
            this.spbtnXoa.Text = "Xóa";
            this.spbtnXoa.Click += new System.EventHandler(this.spbtnXoa_Click);
            // 
            // spbtnSua
            // 
            this.spbtnSua.Appearance.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(112)))), ((int)(((byte)(195)))), ((int)(((byte)(245)))));
            this.spbtnSua.Appearance.Font = new System.Drawing.Font("Segoe UI", 10.2F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.spbtnSua.Appearance.Options.UseBackColor = true;
            this.spbtnSua.Appearance.Options.UseFont = true;
            this.spbtnSua.ButtonStyle = DevExpress.XtraEditors.Controls.BorderStyles.Style3D;
            this.spbtnSua.Enabled = false;
            this.spbtnSua.Location = new System.Drawing.Point(753, 85);
            this.spbtnSua.Name = "spbtnSua";
            this.spbtnSua.Size = new System.Drawing.Size(75, 49);
            this.spbtnSua.TabIndex = 9;
            this.spbtnSua.Text = "Sửa";
            this.spbtnSua.Click += new System.EventHandler(this.spbtnSua_Click);
            // 
            // spbtnThem
            // 
            this.spbtnThem.AllowGlyphSkinning = DevExpress.Utils.DefaultBoolean.True;
            this.spbtnThem.Appearance.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(112)))), ((int)(((byte)(195)))), ((int)(((byte)(245)))));
            this.spbtnThem.Appearance.Font = new System.Drawing.Font("Segoe UI", 10.2F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.spbtnThem.Appearance.Options.UseBackColor = true;
            this.spbtnThem.Appearance.Options.UseFont = true;
            this.spbtnThem.ButtonStyle = DevExpress.XtraEditors.Controls.BorderStyles.Style3D;
            this.spbtnThem.Location = new System.Drawing.Point(753, 24);
            this.spbtnThem.Name = "spbtnThem";
            this.spbtnThem.Size = new System.Drawing.Size(75, 49);
            this.spbtnThem.TabIndex = 8;
            this.spbtnThem.Text = "Thêm";
            this.spbtnThem.Click += new System.EventHandler(this.spbtnThem_Click);
            // 
            // dtpNgaySinh
            // 
            this.dtpNgaySinh.CustomFormat = "MM/dd/yyyy  HH:mm tt";
            this.dtpNgaySinh.Enabled = false;
            this.dtpNgaySinh.Font = new System.Drawing.Font("Segoe UI", 10.2F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.dtpNgaySinh.Format = System.Windows.Forms.DateTimePickerFormat.Custom;
            this.dtpNgaySinh.Location = new System.Drawing.Point(135, 118);
            this.dtpNgaySinh.Name = "dtpNgaySinh";
            this.dtpNgaySinh.Size = new System.Drawing.Size(200, 30);
            this.dtpNgaySinh.TabIndex = 4;
            // 
            // cboVaiTro
            // 
            this.cboVaiTro.Enabled = false;
            this.cboVaiTro.Font = new System.Drawing.Font("Segoe UI", 10.2F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.cboVaiTro.FormattingEnabled = true;
            this.cboVaiTro.Location = new System.Drawing.Point(135, 21);
            this.cboVaiTro.Name = "cboVaiTro";
            this.cboVaiTro.Size = new System.Drawing.Size(121, 31);
            this.cboVaiTro.TabIndex = 1;
            this.cboVaiTro.SelectedIndexChanged += new System.EventHandler(this.cboVaiTro_SelectedIndexChanged);
            // 
            // cboGioiTinh
            // 
            this.cboGioiTinh.Enabled = false;
            this.cboGioiTinh.Font = new System.Drawing.Font("Segoe UI", 10.2F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.cboGioiTinh.FormattingEnabled = true;
            this.cboGioiTinh.Location = new System.Drawing.Point(505, 117);
            this.cboGioiTinh.Name = "cboGioiTinh";
            this.cboGioiTinh.Size = new System.Drawing.Size(121, 31);
            this.cboGioiTinh.TabIndex = 5;
            // 
            // txtSDT
            // 
            this.txtSDT.Font = new System.Drawing.Font("Segoe UI", 10.2F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtSDT.Location = new System.Drawing.Point(505, 67);
            this.txtSDT.Name = "txtSDT";
            this.txtSDT.ReadOnly = true;
            this.txtSDT.Size = new System.Drawing.Size(121, 30);
            this.txtSDT.TabIndex = 3;
            this.txtSDT.TextChanged += new System.EventHandler(this.txtSDT_TextChanged);
            // 
            // txtDiaChi
            // 
            this.txtDiaChi.Font = new System.Drawing.Font("Segoe UI", 10.2F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtDiaChi.Location = new System.Drawing.Point(135, 169);
            this.txtDiaChi.Name = "txtDiaChi";
            this.txtDiaChi.ReadOnly = true;
            this.txtDiaChi.Size = new System.Drawing.Size(200, 30);
            this.txtDiaChi.TabIndex = 6;
            // 
            // txtCMND
            // 
            this.txtCMND.Font = new System.Drawing.Font("Segoe UI", 10.2F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtCMND.Location = new System.Drawing.Point(505, 169);
            this.txtCMND.Name = "txtCMND";
            this.txtCMND.ReadOnly = true;
            this.txtCMND.Size = new System.Drawing.Size(121, 30);
            this.txtCMND.TabIndex = 7;
            this.txtCMND.TextChanged += new System.EventHandler(this.txtCMND_TextChanged);
            // 
            // txtHoTen
            // 
            this.txtHoTen.Font = new System.Drawing.Font("Segoe UI", 10.2F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtHoTen.Location = new System.Drawing.Point(135, 67);
            this.txtHoTen.Name = "txtHoTen";
            this.txtHoTen.ReadOnly = true;
            this.txtHoTen.Size = new System.Drawing.Size(200, 30);
            this.txtHoTen.TabIndex = 2;
            this.txtHoTen.TextChanged += new System.EventHandler(this.txtHoTen_TextChanged);
            // 
            // lblVaiTro
            // 
            this.lblVaiTro.AutoSize = true;
            this.lblVaiTro.Font = new System.Drawing.Font("Segoe UI", 10.2F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblVaiTro.Location = new System.Drawing.Point(35, 24);
            this.lblVaiTro.Name = "lblVaiTro";
            this.lblVaiTro.Size = new System.Drawing.Size(64, 23);
            this.lblVaiTro.TabIndex = 0;
            this.lblVaiTro.Text = "Vai trò:";
            // 
            // lblGioiTinh
            // 
            this.lblGioiTinh.AutoSize = true;
            this.lblGioiTinh.Font = new System.Drawing.Font("Segoe UI", 10.2F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblGioiTinh.Location = new System.Drawing.Point(384, 120);
            this.lblGioiTinh.Name = "lblGioiTinh";
            this.lblGioiTinh.Size = new System.Drawing.Size(79, 23);
            this.lblGioiTinh.TabIndex = 0;
            this.lblGioiTinh.Text = "Giới tính:";
            // 
            // lblTenMa
            // 
            this.lblTenMa.AutoSize = true;
            this.lblTenMa.Font = new System.Drawing.Font("Segoe UI", 10.2F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblTenMa.Location = new System.Drawing.Point(501, 24);
            this.lblTenMa.Name = "lblTenMa";
            this.lblTenMa.Size = new System.Drawing.Size(64, 23);
            this.lblTenMa.TabIndex = 0;
            this.lblTenMa.Text = "tên mã";
            // 
            // lblMaNV
            // 
            this.lblMaNV.AutoSize = true;
            this.lblMaNV.Font = new System.Drawing.Font("Segoe UI", 10.2F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblMaNV.Location = new System.Drawing.Point(384, 24);
            this.lblMaNV.Name = "lblMaNV";
            this.lblMaNV.Size = new System.Drawing.Size(67, 23);
            this.lblMaNV.TabIndex = 0;
            this.lblMaNV.Text = "Mã NV:";
            // 
            // lblSDT
            // 
            this.lblSDT.AutoSize = true;
            this.lblSDT.Font = new System.Drawing.Font("Segoe UI", 10.2F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblSDT.Location = new System.Drawing.Point(384, 67);
            this.lblSDT.Name = "lblSDT";
            this.lblSDT.Size = new System.Drawing.Size(115, 23);
            this.lblSDT.TabIndex = 0;
            this.lblSDT.Text = "Số điện thoại:";
            // 
            // lblDiaChi
            // 
            this.lblDiaChi.AutoSize = true;
            this.lblDiaChi.Font = new System.Drawing.Font("Segoe UI", 10.2F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblDiaChi.Location = new System.Drawing.Point(35, 172);
            this.lblDiaChi.Name = "lblDiaChi";
            this.lblDiaChi.Size = new System.Drawing.Size(66, 23);
            this.lblDiaChi.TabIndex = 0;
            this.lblDiaChi.Text = "Địa chỉ:";
            // 
            // lblCMND
            // 
            this.lblCMND.AutoSize = true;
            this.lblCMND.Font = new System.Drawing.Font("Segoe UI", 10.2F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblCMND.Location = new System.Drawing.Point(384, 172);
            this.lblCMND.Name = "lblCMND";
            this.lblCMND.Size = new System.Drawing.Size(65, 23);
            this.lblCMND.TabIndex = 0;
            this.lblCMND.Text = "CMND:";
            // 
            // lblNgaySinh
            // 
            this.lblNgaySinh.AutoSize = true;
            this.lblNgaySinh.Font = new System.Drawing.Font("Segoe UI", 10.2F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblNgaySinh.Location = new System.Drawing.Point(35, 123);
            this.lblNgaySinh.Name = "lblNgaySinh";
            this.lblNgaySinh.Size = new System.Drawing.Size(90, 23);
            this.lblNgaySinh.TabIndex = 0;
            this.lblNgaySinh.Text = "Ngày sinh:";
            // 
            // lblHoTen
            // 
            this.lblHoTen.AutoSize = true;
            this.lblHoTen.Font = new System.Drawing.Font("Segoe UI", 10.2F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblHoTen.Location = new System.Drawing.Point(35, 70);
            this.lblHoTen.Name = "lblHoTen";
            this.lblHoTen.Size = new System.Drawing.Size(66, 23);
            this.lblHoTen.TabIndex = 0;
            this.lblHoTen.Text = "Họ tên:";
            // 
            // tnpThongTin
            // 
            this.tnpThongTin.Appearance.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(112)))), ((int)(((byte)(195)))), ((int)(((byte)(245)))));
            this.tnpThongTin.Appearance.Options.UseBackColor = true;
            this.tnpThongTin.Caption = "Thông tin chung";
            this.tnpThongTin.Controls.Add(this.btnPicture);
            this.tnpThongTin.Controls.Add(this.lblgioitinh_ttc);
            this.tnpThongTin.Controls.Add(this.lblGioiTinh_TT);
            this.tnpThongTin.Controls.Add(this.lblsdt_ttc);
            this.tnpThongTin.Controls.Add(this.lblSDT_TT);
            this.tnpThongTin.Controls.Add(this.lbldiachi_ttc);
            this.tnpThongTin.Controls.Add(this.lblDiaChi_TT);
            this.tnpThongTin.Controls.Add(this.lblcmnd_ttc);
            this.tnpThongTin.Controls.Add(this.lblCMND_TT);
            this.tnpThongTin.Controls.Add(this.lblngaysinh_ttc);
            this.tnpThongTin.Controls.Add(this.lblNgaySinh_TT);
            this.tnpThongTin.Controls.Add(this.lblmanv_ttc);
            this.tnpThongTin.Controls.Add(this.lblhoten_ttc);
            this.tnpThongTin.Controls.Add(this.lblMaNV_TT);
            this.tnpThongTin.Controls.Add(this.lblHoTen_TT);
            this.tnpThongTin.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.tnpThongTin.Name = "tnpThongTin";
            this.tnpThongTin.Size = new System.Drawing.Size(868, 224);
            // 
            // btnPicture
            // 
            this.btnPicture.FlatAppearance.BorderSize = 0;
            this.btnPicture.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnPicture.ImageIndex = 1;
            this.btnPicture.ImageList = this.imageList2;
            this.btnPicture.Location = new System.Drawing.Point(16, 15);
            this.btnPicture.Name = "btnPicture";
            this.btnPicture.Size = new System.Drawing.Size(136, 136);
            this.btnPicture.TabIndex = 7;
            this.btnPicture.UseVisualStyleBackColor = true;
            // 
            // imageList2
            // 
            this.imageList2.ImageStream = ((System.Windows.Forms.ImageListStreamer)(resources.GetObject("imageList2.ImageStream")));
            this.imageList2.TransparentColor = System.Drawing.Color.Transparent;
            this.imageList2.Images.SetKeyName(0, "waiter.png");
            this.imageList2.Images.SetKeyName(1, "waiterjk.png");
            this.imageList2.Images.SetKeyName(2, "waitress.png");
            // 
            // lblgioitinh_ttc
            // 
            this.lblgioitinh_ttc.AutoSize = true;
            this.lblgioitinh_ttc.Font = new System.Drawing.Font("Segoe UI", 10.2F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblgioitinh_ttc.Location = new System.Drawing.Point(326, 154);
            this.lblgioitinh_ttc.Name = "lblgioitinh_ttc";
            this.lblgioitinh_ttc.Size = new System.Drawing.Size(73, 23);
            this.lblgioitinh_ttc.TabIndex = 1;
            this.lblgioitinh_ttc.Text = "giới tính";
            // 
            // lblGioiTinh_TT
            // 
            this.lblGioiTinh_TT.AutoSize = true;
            this.lblGioiTinh_TT.Font = new System.Drawing.Font("Segoe UI", 10.2F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblGioiTinh_TT.Location = new System.Drawing.Point(198, 154);
            this.lblGioiTinh_TT.Name = "lblGioiTinh_TT";
            this.lblGioiTinh_TT.Size = new System.Drawing.Size(85, 23);
            this.lblGioiTinh_TT.TabIndex = 1;
            this.lblGioiTinh_TT.Text = "Giới tính:";
            // 
            // lblsdt_ttc
            // 
            this.lblsdt_ttc.AutoSize = true;
            this.lblsdt_ttc.Font = new System.Drawing.Font("Segoe UI", 10.2F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblsdt_ttc.Location = new System.Drawing.Point(326, 217);
            this.lblsdt_ttc.Name = "lblsdt_ttc";
            this.lblsdt_ttc.Size = new System.Drawing.Size(109, 23);
            this.lblsdt_ttc.TabIndex = 2;
            this.lblsdt_ttc.Text = "số điện thoại";
            // 
            // lblSDT_TT
            // 
            this.lblSDT_TT.AutoSize = true;
            this.lblSDT_TT.Font = new System.Drawing.Font("Segoe UI", 10.2F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblSDT_TT.Location = new System.Drawing.Point(198, 217);
            this.lblSDT_TT.Name = "lblSDT_TT";
            this.lblSDT_TT.Size = new System.Drawing.Size(121, 23);
            this.lblSDT_TT.TabIndex = 2;
            this.lblSDT_TT.Text = "Số điện thoại:";
            // 
            // lbldiachi_ttc
            // 
            this.lbldiachi_ttc.AutoSize = true;
            this.lbldiachi_ttc.Font = new System.Drawing.Font("Segoe UI", 10.2F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lbldiachi_ttc.Location = new System.Drawing.Point(326, 250);
            this.lbldiachi_ttc.Name = "lbldiachi_ttc";
            this.lbldiachi_ttc.Size = new System.Drawing.Size(60, 23);
            this.lbldiachi_ttc.TabIndex = 3;
            this.lbldiachi_ttc.Text = "địa chỉ";
            // 
            // lblDiaChi_TT
            // 
            this.lblDiaChi_TT.AutoSize = true;
            this.lblDiaChi_TT.Font = new System.Drawing.Font("Segoe UI", 10.2F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblDiaChi_TT.Location = new System.Drawing.Point(198, 250);
            this.lblDiaChi_TT.Name = "lblDiaChi_TT";
            this.lblDiaChi_TT.Size = new System.Drawing.Size(70, 23);
            this.lblDiaChi_TT.TabIndex = 3;
            this.lblDiaChi_TT.Text = "Địa chỉ:";
            // 
            // lblcmnd_ttc
            // 
            this.lblcmnd_ttc.AutoSize = true;
            this.lblcmnd_ttc.Font = new System.Drawing.Font("Segoe UI", 10.2F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblcmnd_ttc.Location = new System.Drawing.Point(326, 186);
            this.lblcmnd_ttc.Name = "lblcmnd_ttc";
            this.lblcmnd_ttc.Size = new System.Drawing.Size(53, 23);
            this.lblcmnd_ttc.TabIndex = 4;
            this.lblcmnd_ttc.Text = "cmnd";
            // 
            // lblCMND_TT
            // 
            this.lblCMND_TT.AutoSize = true;
            this.lblCMND_TT.Font = new System.Drawing.Font("Segoe UI", 10.2F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblCMND_TT.Location = new System.Drawing.Point(198, 186);
            this.lblCMND_TT.Name = "lblCMND_TT";
            this.lblCMND_TT.Size = new System.Drawing.Size(68, 23);
            this.lblCMND_TT.TabIndex = 4;
            this.lblCMND_TT.Text = "CMND:";
            // 
            // lblngaysinh_ttc
            // 
            this.lblngaysinh_ttc.AutoSize = true;
            this.lblngaysinh_ttc.Font = new System.Drawing.Font("Segoe UI", 10.2F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblngaysinh_ttc.Location = new System.Drawing.Point(326, 121);
            this.lblngaysinh_ttc.Name = "lblngaysinh_ttc";
            this.lblngaysinh_ttc.Size = new System.Drawing.Size(83, 23);
            this.lblngaysinh_ttc.TabIndex = 5;
            this.lblngaysinh_ttc.Text = "ngày sinh";
            // 
            // lblNgaySinh_TT
            // 
            this.lblNgaySinh_TT.AutoSize = true;
            this.lblNgaySinh_TT.Font = new System.Drawing.Font("Segoe UI", 10.2F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblNgaySinh_TT.Location = new System.Drawing.Point(198, 121);
            this.lblNgaySinh_TT.Name = "lblNgaySinh_TT";
            this.lblNgaySinh_TT.Size = new System.Drawing.Size(94, 23);
            this.lblNgaySinh_TT.TabIndex = 5;
            this.lblNgaySinh_TT.Text = "Ngày sinh:";
            // 
            // lblmanv_ttc
            // 
            this.lblmanv_ttc.AutoSize = true;
            this.lblmanv_ttc.Font = new System.Drawing.Font("Segoe UI", 10.2F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblmanv_ttc.Location = new System.Drawing.Point(326, 61);
            this.lblmanv_ttc.Name = "lblmanv_ttc";
            this.lblmanv_ttc.Size = new System.Drawing.Size(114, 23);
            this.lblmanv_ttc.TabIndex = 6;
            this.lblmanv_ttc.Text = "mã nhân viên";
            // 
            // lblhoten_ttc
            // 
            this.lblhoten_ttc.AutoSize = true;
            this.lblhoten_ttc.Font = new System.Drawing.Font("Segoe UI", 10.2F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblhoten_ttc.Location = new System.Drawing.Point(326, 92);
            this.lblhoten_ttc.Name = "lblhoten_ttc";
            this.lblhoten_ttc.Size = new System.Drawing.Size(60, 23);
            this.lblhoten_ttc.TabIndex = 6;
            this.lblhoten_ttc.Text = "họ tên";
            // 
            // lblMaNV_TT
            // 
            this.lblMaNV_TT.AutoSize = true;
            this.lblMaNV_TT.Font = new System.Drawing.Font("Segoe UI", 10.2F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblMaNV_TT.Location = new System.Drawing.Point(198, 61);
            this.lblMaNV_TT.Name = "lblMaNV_TT";
            this.lblMaNV_TT.Size = new System.Drawing.Size(122, 23);
            this.lblMaNV_TT.TabIndex = 6;
            this.lblMaNV_TT.Text = "Mã nhân viên:";
            // 
            // lblHoTen_TT
            // 
            this.lblHoTen_TT.AutoSize = true;
            this.lblHoTen_TT.Font = new System.Drawing.Font("Segoe UI", 10.2F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblHoTen_TT.Location = new System.Drawing.Point(198, 92);
            this.lblHoTen_TT.Name = "lblHoTen_TT";
            this.lblHoTen_TT.Size = new System.Drawing.Size(69, 23);
            this.lblHoTen_TT.TabIndex = 6;
            this.lblHoTen_TT.Text = "Họ tên:";
            // 
            // QLNhanVien
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(8F, 20F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(112)))), ((int)(((byte)(195)))), ((int)(((byte)(245)))));
            this.ClientSize = new System.Drawing.Size(890, 564);
            this.Controls.Add(this.tpNV);
            this.Controls.Add(this.lsvNV);
            this.Font = new System.Drawing.Font("Segoe UI", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.Name = "QLNhanVien";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "Quản lý nhân viên";
            this.Load += new System.EventHandler(this.QLNhanVien_Load);
            ((System.ComponentModel.ISupportInitialize)(this.tpNV)).EndInit();
            this.tpNV.ResumeLayout(false);
            this.tnpThem.ResumeLayout(false);
            this.tnpThem.PerformLayout();
            this.tnpThongTin.ResumeLayout(false);
            this.tnpThongTin.PerformLayout();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.ListView lsvNV;
        private System.Windows.Forms.ColumnHeader columnHeader1;
        private System.Windows.Forms.ColumnHeader columnHeader2;
        private System.Windows.Forms.ColumnHeader columnHeader3;
        private System.Windows.Forms.ColumnHeader columnHeader4;
        private System.Windows.Forms.ColumnHeader columnHeader5;
        private System.Windows.Forms.ColumnHeader columnHeader6;
        private System.Windows.Forms.ColumnHeader columnHeader7;
        private System.Windows.Forms.ColumnHeader columnHeader8;
        private System.Windows.Forms.ImageList imageList1;
        private DevExpress.XtraBars.Navigation.TabPane tpNV;
        private System.Windows.Forms.Button btnLenXuong;
        private DevExpress.XtraBars.Navigation.TabNavigationPage tnpThem;
        private System.Windows.Forms.DateTimePicker dtpNgaySinh;
        private System.Windows.Forms.ComboBox cboGioiTinh;
        private System.Windows.Forms.TextBox txtSDT;
        private System.Windows.Forms.TextBox txtDiaChi;
        private System.Windows.Forms.TextBox txtCMND;
        private System.Windows.Forms.TextBox txtHoTen;
        private System.Windows.Forms.Label lblGioiTinh;
        private System.Windows.Forms.Label lblSDT;
        private System.Windows.Forms.Label lblDiaChi;
        private System.Windows.Forms.Label lblCMND;
        private System.Windows.Forms.Label lblNgaySinh;
        private System.Windows.Forms.Label lblHoTen;
        private DevExpress.XtraBars.Navigation.TabNavigationPage tnpThongTin;
        private System.Windows.Forms.Label lblGioiTinh_TT;
        private System.Windows.Forms.Label lblSDT_TT;
        private System.Windows.Forms.Label lblDiaChi_TT;
        private System.Windows.Forms.Label lblCMND_TT;
        private System.Windows.Forms.Label lblNgaySinh_TT;
        private System.Windows.Forms.Label lblHoTen_TT;
        private DevExpress.XtraEditors.SimpleButton spbtnXoa;
        private DevExpress.XtraEditors.SimpleButton spbtnSua;
        private DevExpress.XtraEditors.SimpleButton spbtnThem;
        private System.Windows.Forms.ComboBox cboVaiTro;
        private System.Windows.Forms.Label lblVaiTro;
        private System.Windows.Forms.Label lblTenMa;
        private System.Windows.Forms.Label lblMaNV;
        private System.Windows.Forms.Label lblgioitinh_ttc;
        private System.Windows.Forms.Label lblsdt_ttc;
        private System.Windows.Forms.Label lbldiachi_ttc;
        private System.Windows.Forms.Label lblcmnd_ttc;
        private System.Windows.Forms.Label lblngaysinh_ttc;
        private System.Windows.Forms.Label lblhoten_ttc;
        private System.Windows.Forms.Label lblmanv_ttc;
        private System.Windows.Forms.Label lblMaNV_TT;
        private System.Windows.Forms.Button btnPicture;
        private System.Windows.Forms.ImageList imageList2;
    }
}