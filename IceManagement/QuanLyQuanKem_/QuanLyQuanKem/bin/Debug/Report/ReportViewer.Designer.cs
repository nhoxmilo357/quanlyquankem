﻿namespace QuanLyQuanKem.Report
{
    partial class ReportViewer
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            Microsoft.Reporting.WinForms.ReportDataSource reportDataSource1 = new Microsoft.Reporting.WinForms.ReportDataSource();
            this.dOANHTHUTHEOMHANGBindingSource = new System.Windows.Forms.BindingSource(this.components);
            this.datasetDoanhthu = new QuanLyQuanKem.Report.DatasetDoanhthu();
            this.reportViewer1 = new Microsoft.Reporting.WinForms.ReportViewer();
            this.dOANHTHUTHEOMHANGTableAdapter = new QuanLyQuanKem.Report.DatasetDoanhthuTableAdapters.DOANHTHUTHEOMHANGTableAdapter();
            ((System.ComponentModel.ISupportInitialize)(this.dOANHTHUTHEOMHANGBindingSource)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.datasetDoanhthu)).BeginInit();
            this.SuspendLayout();
            // 
            // dOANHTHUTHEOMHANGBindingSource
            // 
            this.dOANHTHUTHEOMHANGBindingSource.DataMember = "DOANHTHUTHEOMHANG";
            this.dOANHTHUTHEOMHANGBindingSource.DataSource = this.datasetDoanhthu;
            // 
            // datasetDoanhthu
            // 
            this.datasetDoanhthu.DataSetName = "DatasetDoanhthu";
            this.datasetDoanhthu.SchemaSerializationMode = System.Data.SchemaSerializationMode.IncludeSchema;
            // 
            // reportViewer1
            // 
            this.reportViewer1.Dock = System.Windows.Forms.DockStyle.Fill;
            reportDataSource1.Name = "DataSet1";
            reportDataSource1.Value = this.dOANHTHUTHEOMHANGBindingSource;
            this.reportViewer1.LocalReport.DataSources.Add(reportDataSource1);
            this.reportViewer1.LocalReport.ReportEmbeddedResource = "QuanLyQuanKem.Report.Report1.rdlc";
            this.reportViewer1.Location = new System.Drawing.Point(0, 0);
            this.reportViewer1.Name = "reportViewer1";
            this.reportViewer1.Size = new System.Drawing.Size(955, 423);
            this.reportViewer1.TabIndex = 0;
            // 
            // dOANHTHUTHEOMHANGTableAdapter
            // 
            this.dOANHTHUTHEOMHANGTableAdapter.ClearBeforeFill = true;
            // 
            // ReportViewer
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(8F, 16F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(955, 423);
            this.Controls.Add(this.reportViewer1);
            this.Name = "ReportViewer";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "ReportViewer";
            this.Load += new System.EventHandler(this.ReportViewer_Load);
            ((System.ComponentModel.ISupportInitialize)(this.dOANHTHUTHEOMHANGBindingSource)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.datasetDoanhthu)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        private Microsoft.Reporting.WinForms.ReportViewer reportViewer1;
        private DatasetDoanhthu datasetDoanhthu;
        private System.Windows.Forms.BindingSource dOANHTHUTHEOMHANGBindingSource;
        private DatasetDoanhthuTableAdapters.DOANHTHUTHEOMHANGTableAdapter dOANHTHUTHEOMHANGTableAdapter;

    }
}