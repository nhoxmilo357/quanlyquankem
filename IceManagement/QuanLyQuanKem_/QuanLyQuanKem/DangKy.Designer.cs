﻿namespace QuanLyQuanKem
{
    partial class DangKy
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.cboTenNV = new System.Windows.Forms.ComboBox();
            this.txtMK = new System.Windows.Forms.TextBox();
            this.txtTK = new System.Windows.Forms.TextBox();
            this.spbtnDangKy = new DevExpress.XtraEditors.SimpleButton();
            this.SuspendLayout();
            // 
            // cboTenNV
            // 
            this.cboTenNV.ForeColor = System.Drawing.Color.Black;
            this.cboTenNV.FormattingEnabled = true;
            this.cboTenNV.Location = new System.Drawing.Point(63, 22);
            this.cboTenNV.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.cboTenNV.Name = "cboTenNV";
            this.cboTenNV.Size = new System.Drawing.Size(268, 31);
            this.cboTenNV.TabIndex = 0;
            this.cboTenNV.SelectedIndexChanged += new System.EventHandler(this.cboTenNV_SelectedIndexChanged);
            // 
            // txtMK
            // 
            this.txtMK.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(112)))), ((int)(((byte)(195)))), ((int)(((byte)(245)))));
            this.txtMK.BorderStyle = System.Windows.Forms.BorderStyle.None;
            this.txtMK.Font = new System.Drawing.Font("Segoe UI", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtMK.ForeColor = System.Drawing.SystemColors.ControlText;
            this.txtMK.Location = new System.Drawing.Point(63, 136);
            this.txtMK.Name = "txtMK";
            this.txtMK.Size = new System.Drawing.Size(268, 27);
            this.txtMK.TabIndex = 1;
            this.txtMK.Text = "Mật khẩu";
            this.txtMK.TextChanged += new System.EventHandler(this.txtTK_TextChanged);
            this.txtMK.Enter += new System.EventHandler(this.txtMK_Enter);
            // 
            // txtTK
            // 
            this.txtTK.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(112)))), ((int)(((byte)(195)))), ((int)(((byte)(245)))));
            this.txtTK.BorderStyle = System.Windows.Forms.BorderStyle.None;
            this.txtTK.Font = new System.Drawing.Font("Segoe UI", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtTK.ForeColor = System.Drawing.SystemColors.ControlText;
            this.txtTK.Location = new System.Drawing.Point(63, 79);
            this.txtTK.Name = "txtTK";
            this.txtTK.Size = new System.Drawing.Size(268, 27);
            this.txtTK.TabIndex = 1;
            this.txtTK.Text = "Tài khoản";
            this.txtTK.TextChanged += new System.EventHandler(this.txtTK_TextChanged);
            this.txtTK.Enter += new System.EventHandler(this.txtTK_Enter);
            // 
            // spbtnDangKy
            // 
            this.spbtnDangKy.Appearance.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(112)))), ((int)(((byte)(195)))), ((int)(((byte)(245)))));
            this.spbtnDangKy.Appearance.Font = new System.Drawing.Font("Segoe UI", 10.2F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.spbtnDangKy.Appearance.Options.UseBackColor = true;
            this.spbtnDangKy.Appearance.Options.UseFont = true;
            this.spbtnDangKy.AppearanceHovered.BackColor = System.Drawing.Color.Chartreuse;
            this.spbtnDangKy.AppearanceHovered.Font = new System.Drawing.Font("Segoe UI", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.spbtnDangKy.AppearanceHovered.Options.UseBackColor = true;
            this.spbtnDangKy.AppearanceHovered.Options.UseFont = true;
            this.spbtnDangKy.ButtonStyle = DevExpress.XtraEditors.Controls.BorderStyles.NoBorder;
            this.spbtnDangKy.Enabled = false;
            this.spbtnDangKy.Location = new System.Drawing.Point(129, 213);
            this.spbtnDangKy.Name = "spbtnDangKy";
            this.spbtnDangKy.Size = new System.Drawing.Size(134, 40);
            this.spbtnDangKy.TabIndex = 2;
            this.spbtnDangKy.Text = "Đăng ký";
            this.spbtnDangKy.Click += new System.EventHandler(this.spbtnDangKy_Click);
            // 
            // DangKy
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(9F, 23F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(112)))), ((int)(((byte)(195)))), ((int)(((byte)(245)))));
            this.ClientSize = new System.Drawing.Size(388, 298);
            this.Controls.Add(this.spbtnDangKy);
            this.Controls.Add(this.txtTK);
            this.Controls.Add(this.txtMK);
            this.Controls.Add(this.cboTenNV);
            this.Font = new System.Drawing.Font("Segoe UI", 10.2F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.ForeColor = System.Drawing.SystemColors.AppWorkspace;
            this.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.Name = "DangKy";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "Đăng ký tài khoản";
            this.Load += new System.EventHandler(this.DangKy_Load);
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.ComboBox cboTenNV;
        private System.Windows.Forms.TextBox txtMK;
        private System.Windows.Forms.TextBox txtTK;
        private DevExpress.XtraEditors.SimpleButton spbtnDangKy;
    }
}