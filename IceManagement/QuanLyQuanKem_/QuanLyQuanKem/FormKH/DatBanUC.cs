﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using QuanLyQuanKem.Classes;

namespace QuanLyQuanKem.FormKH
{
    public partial class DatBanUC : UserControl
    {
        LoadSQL lsql = new LoadSQL();
        public DatBanUC()
        {
            InitializeComponent();
            loadtable();
        }
        public void loadtable()
        {
            List<btnBan> tablelist = lsql.loadtablelist();
            foreach (btnBan item in tablelist)
            {
                Button btn = new Button();
                btn.Text = item.Tenban + "\n" + item.Trangthai;
                btn.Width = 355;
                btn.Height = 200;
                btn.FlatStyle = FlatStyle.Flat;
                btn.FlatAppearance.BorderSize = 0;
                btn.Tag = item;
                switch (item.Trangthai)
                {
                    case "Trống":
                        btn.BackColor = Color.FromArgb(112, 195, 245);
                        break;
                    default:
                        btn.BackColor = Color.IndianRed;
                        break;
                }
                flpBan.Controls.Add(btn);
            }
        }
    }
}
