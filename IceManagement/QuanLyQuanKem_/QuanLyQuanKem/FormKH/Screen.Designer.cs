﻿namespace QuanLyQuanKem.FormKH
{
    partial class Screen
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(Screen));
            this.panel1 = new System.Windows.Forms.Panel();
            this.label3 = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.pictureBox1 = new System.Windows.Forms.PictureBox();
            this.winUIbtnXemChoNgoi = new DevExpress.XtraBars.Docking2010.WindowsUIButtonPanel();
            this.winUIbtnXemThucDon = new DevExpress.XtraBars.Docking2010.WindowsUIButtonPanel();
            this.label1 = new System.Windows.Forms.Label();
            this.thucDonUC1 = new QuanLyQuanKem.FormKH.ThucDonUC();
            this.datBanUC1 = new QuanLyQuanKem.FormKH.DatBanUC();
            this.thucDonUC2 = new QuanLyQuanKem.FormKH.ThucDonUC();
            this.panel1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox1)).BeginInit();
            this.SuspendLayout();
            // 
            // panel1
            // 
            this.panel1.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.panel1.BackColor = System.Drawing.Color.Transparent;
            this.panel1.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.panel1.Controls.Add(this.label3);
            this.panel1.Controls.Add(this.label2);
            this.panel1.Controls.Add(this.pictureBox1);
            this.panel1.Location = new System.Drawing.Point(0, 0);
            this.panel1.Name = "panel1";
            this.panel1.Size = new System.Drawing.Size(1000, 106);
            this.panel1.TabIndex = 2;
            this.panel1.MouseDown += new System.Windows.Forms.MouseEventHandler(this.Screen_MouseDown);
            this.panel1.MouseMove += new System.Windows.Forms.MouseEventHandler(this.Screen_MouseMove);
            this.panel1.MouseUp += new System.Windows.Forms.MouseEventHandler(this.Screen_MouseUp);
            // 
            // label3
            // 
            this.label3.Anchor = System.Windows.Forms.AnchorStyles.Top;
            this.label3.AutoSize = true;
            this.label3.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(112)))), ((int)(((byte)(195)))), ((int)(((byte)(245)))));
            this.label3.Font = new System.Drawing.Font("Script MT Bold", 12F, ((System.Drawing.FontStyle)((System.Drawing.FontStyle.Bold | System.Drawing.FontStyle.Italic))), System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label3.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(237)))), ((int)(((byte)(32)))), ((int)(((byte)(75)))));
            this.label3.Location = new System.Drawing.Point(519, 75);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(55, 24);
            this.label3.TabIndex = 2;
            this.label3.Text = "Răng";
            // 
            // label2
            // 
            this.label2.Anchor = System.Windows.Forms.AnchorStyles.Top;
            this.label2.AutoSize = true;
            this.label2.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(112)))), ((int)(((byte)(195)))), ((int)(((byte)(245)))));
            this.label2.Font = new System.Drawing.Font("Script MT Bold", 12F, ((System.Drawing.FontStyle)((System.Drawing.FontStyle.Bold | System.Drawing.FontStyle.Italic))), System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label2.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(237)))), ((int)(((byte)(32)))), ((int)(((byte)(75)))));
            this.label2.Location = new System.Drawing.Point(419, 75);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(51, 24);
            this.label2.TabIndex = 1;
            this.label2.Text = "Buốt";
            // 
            // pictureBox1
            // 
            this.pictureBox1.Anchor = System.Windows.Forms.AnchorStyles.Top;
            this.pictureBox1.Image = ((System.Drawing.Image)(resources.GetObject("pictureBox1.Image")));
            this.pictureBox1.Location = new System.Drawing.Point(415, 0);
            this.pictureBox1.Name = "pictureBox1";
            this.pictureBox1.Size = new System.Drawing.Size(163, 106);
            this.pictureBox1.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.pictureBox1.TabIndex = 0;
            this.pictureBox1.TabStop = false;
            this.pictureBox1.Click += new System.EventHandler(this.pictureBox1_Click);
            this.pictureBox1.MouseDown += new System.Windows.Forms.MouseEventHandler(this.Screen_MouseDown);
            this.pictureBox1.MouseMove += new System.Windows.Forms.MouseEventHandler(this.Screen_MouseMove);
            this.pictureBox1.MouseUp += new System.Windows.Forms.MouseEventHandler(this.Screen_MouseUp);
            // 
            // winUIbtnXemChoNgoi
            // 
            this.winUIbtnXemChoNgoi.Anchor = System.Windows.Forms.AnchorStyles.Top;
            this.winUIbtnXemChoNgoi.AppearanceButton.Hovered.Font = new System.Drawing.Font("Segoe UI", 13.8F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.winUIbtnXemChoNgoi.AppearanceButton.Hovered.Options.UseFont = true;
            this.winUIbtnXemChoNgoi.AppearanceButton.Normal.Font = new System.Drawing.Font("Segoe UI", 10.8F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.winUIbtnXemChoNgoi.AppearanceButton.Normal.Options.UseFont = true;
            this.winUIbtnXemChoNgoi.BackColor = System.Drawing.Color.Transparent;
            this.winUIbtnXemChoNgoi.Buttons.AddRange(new DevExpress.XtraEditors.ButtonPanel.IBaseButton[] {
            new DevExpress.XtraBars.Docking2010.WindowsUIButton("Xem chỗ ngồi", DevExpress.XtraBars.Docking2010.ButtonStyle.PushButton)});
            this.winUIbtnXemChoNgoi.Font = new System.Drawing.Font("Segoe UI", 10.8F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.winUIbtnXemChoNgoi.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(237)))), ((int)(((byte)(32)))), ((int)(((byte)(75)))));
            this.winUIbtnXemChoNgoi.Location = new System.Drawing.Point(321, 112);
            this.winUIbtnXemChoNgoi.Name = "winUIbtnXemChoNgoi";
            this.winUIbtnXemChoNgoi.Size = new System.Drawing.Size(158, 74);
            this.winUIbtnXemChoNgoi.TabIndex = 4;
            this.winUIbtnXemChoNgoi.Text = "windowsUIButtonPanel1";
            this.winUIbtnXemChoNgoi.Click += new System.EventHandler(this.winUIbtnXemChoNgoi_Click);
            // 
            // winUIbtnXemThucDon
            // 
            this.winUIbtnXemThucDon.Anchor = System.Windows.Forms.AnchorStyles.Top;
            this.winUIbtnXemThucDon.AppearanceButton.Hovered.Font = new System.Drawing.Font("Segoe UI", 13.8F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.winUIbtnXemThucDon.AppearanceButton.Hovered.Options.UseFont = true;
            this.winUIbtnXemThucDon.AppearanceButton.Normal.Font = new System.Drawing.Font("Segoe UI", 10.8F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.winUIbtnXemThucDon.AppearanceButton.Normal.Options.UseFont = true;
            this.winUIbtnXemThucDon.BackColor = System.Drawing.Color.Transparent;
            this.winUIbtnXemThucDon.Buttons.AddRange(new DevExpress.XtraEditors.ButtonPanel.IBaseButton[] {
            new DevExpress.XtraBars.Docking2010.WindowsUIButton("Xem thực đơn", DevExpress.XtraBars.Docking2010.ButtonStyle.PushButton)});
            this.winUIbtnXemThucDon.Font = new System.Drawing.Font("Segoe UI", 10.8F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.winUIbtnXemThucDon.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(237)))), ((int)(((byte)(32)))), ((int)(((byte)(75)))));
            this.winUIbtnXemThucDon.Location = new System.Drawing.Point(514, 112);
            this.winUIbtnXemThucDon.Name = "winUIbtnXemThucDon";
            this.winUIbtnXemThucDon.Size = new System.Drawing.Size(162, 74);
            this.winUIbtnXemThucDon.TabIndex = 5;
            this.winUIbtnXemThucDon.Text = "windowsUIButtonPanel1";
            this.winUIbtnXemThucDon.Click += new System.EventHandler(this.winUIbtnXemThucDon_Click);
            // 
            // label1
            // 
            this.label1.Anchor = System.Windows.Forms.AnchorStyles.Top;
            this.label1.BackColor = System.Drawing.Color.Peru;
            this.label1.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.label1.Location = new System.Drawing.Point(495, 115);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(2, 74);
            this.label1.TabIndex = 9;
            this.label1.Text = "label1";
            // 
            // thucDonUC1
            // 
            this.thucDonUC1.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.thucDonUC1.BackColor = System.Drawing.Color.Transparent;
            this.thucDonUC1.Font = new System.Drawing.Font("Segoe UI", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.thucDonUC1.Location = new System.Drawing.Point(0, 192);
            this.thucDonUC1.Name = "thucDonUC1";
            this.thucDonUC1.Size = new System.Drawing.Size(1000, 429);
            this.thucDonUC1.TabIndex = 8;
            this.thucDonUC1.Visible = false;
            // 
            // datBanUC1
            // 
            this.datBanUC1.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.datBanUC1.BackColor = System.Drawing.Color.Transparent;
            this.datBanUC1.Location = new System.Drawing.Point(0, 192);
            this.datBanUC1.Name = "datBanUC1";
            this.datBanUC1.Size = new System.Drawing.Size(1000, 429);
            this.datBanUC1.TabIndex = 7;
            this.datBanUC1.Visible = false;
            // 
            // thucDonUC2
            // 
            this.thucDonUC2.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.thucDonUC2.BackColor = System.Drawing.Color.Transparent;
            this.thucDonUC2.Font = new System.Drawing.Font("Segoe UI", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.thucDonUC2.Location = new System.Drawing.Point(0, 192);
            this.thucDonUC2.Name = "thucDonUC2";
            this.thucDonUC2.Size = new System.Drawing.Size(1000, 429);
            this.thucDonUC2.TabIndex = 8;
            this.thucDonUC2.Visible = false;
            // 
            // Screen
            // 
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Inherit;
            this.BackColor = System.Drawing.Color.Peru;
            this.BackgroundImage = ((System.Drawing.Image)(resources.GetObject("$this.BackgroundImage")));
            this.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.ClientSize = new System.Drawing.Size(1000, 650);
            this.Controls.Add(this.label1);
            this.Controls.Add(this.thucDonUC2);
            this.Controls.Add(this.thucDonUC1);
            this.Controls.Add(this.datBanUC1);
            this.Controls.Add(this.winUIbtnXemChoNgoi);
            this.Controls.Add(this.winUIbtnXemThucDon);
            this.Controls.Add(this.panel1);
            this.Font = new System.Drawing.Font("Segoe UI", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.None;
            this.Name = "Screen";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "Screen";
            this.WindowState = System.Windows.Forms.FormWindowState.Maximized;
            this.Load += new System.EventHandler(this.Screen_Load);
            this.MouseDown += new System.Windows.Forms.MouseEventHandler(this.Screen_MouseDown);
            this.MouseMove += new System.Windows.Forms.MouseEventHandler(this.Screen_MouseMove);
            this.MouseUp += new System.Windows.Forms.MouseEventHandler(this.Screen_MouseUp);
            this.panel1.ResumeLayout(false);
            this.panel1.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox1)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.Panel panel1;
        private System.Windows.Forms.PictureBox pictureBox1;
        private DevExpress.XtraBars.Docking2010.WindowsUIButtonPanel winUIbtnXemChoNgoi;
        private DevExpress.XtraBars.Docking2010.WindowsUIButtonPanel winUIbtnXemThucDon;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.Label label2;
        private DatBanUC datBanUC1;
        private ThucDonUC thucDonUC1;
        private System.Windows.Forms.Label label1;
        private ThucDonUC thucDonUC2;


    }
}