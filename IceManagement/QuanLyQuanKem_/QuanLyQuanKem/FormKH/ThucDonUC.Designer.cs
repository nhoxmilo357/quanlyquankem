﻿namespace QuanLyQuanKem.FormKH
{
    partial class ThucDonUC
    {
        /// <summary> 
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary> 
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Component Designer generated code

        /// <summary> 
        /// Required method for Designer support - do not modify 
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.btnBingsu = new System.Windows.Forms.Button();
            this.btnKemCombo = new System.Windows.Forms.Button();
            this.btnKemLy = new System.Windows.Forms.Button();
            this.btnKemCay = new System.Windows.Forms.Button();
            this.panel1 = new System.Windows.Forms.Panel();
            this.thucDon_BingsuUC1 = new QuanLyQuanKem.FormKH.ThucDon_BingsuUC();
            this.thucDon_KemComboUC1 = new QuanLyQuanKem.FormKH.ThucDon_KemComboUC();
            this.thucDon_KemLyUC1 = new QuanLyQuanKem.FormKH.ThucDon_KemLyUC();
            this.thucDon_KemCayUC1 = new QuanLyQuanKem.FormKH.ThucDon_KemCayUC();
            this.SuspendLayout();
            // 
            // btnBingsu
            // 
            this.btnBingsu.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(112)))), ((int)(((byte)(195)))), ((int)(((byte)(245)))));
            this.btnBingsu.FlatAppearance.BorderSize = 0;
            this.btnBingsu.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnBingsu.Font = new System.Drawing.Font("Segoe UI", 10.2F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnBingsu.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(237)))), ((int)(((byte)(32)))), ((int)(((byte)(75)))));
            this.btnBingsu.Location = new System.Drawing.Point(751, 0);
            this.btnBingsu.Name = "btnBingsu";
            this.btnBingsu.Size = new System.Drawing.Size(250, 40);
            this.btnBingsu.TabIndex = 10;
            this.btnBingsu.Text = "Bingsu";
            this.btnBingsu.UseVisualStyleBackColor = false;
            this.btnBingsu.Click += new System.EventHandler(this.btnBingsu_Click);
            // 
            // btnKemCombo
            // 
            this.btnKemCombo.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(112)))), ((int)(((byte)(195)))), ((int)(((byte)(245)))));
            this.btnKemCombo.FlatAppearance.BorderSize = 0;
            this.btnKemCombo.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnKemCombo.Font = new System.Drawing.Font("Segoe UI", 10.2F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnKemCombo.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(237)))), ((int)(((byte)(32)))), ((int)(((byte)(75)))));
            this.btnKemCombo.Location = new System.Drawing.Point(501, 0);
            this.btnKemCombo.Name = "btnKemCombo";
            this.btnKemCombo.Size = new System.Drawing.Size(250, 40);
            this.btnKemCombo.TabIndex = 11;
            this.btnKemCombo.Text = "Kem Combo";
            this.btnKemCombo.UseVisualStyleBackColor = false;
            this.btnKemCombo.Click += new System.EventHandler(this.btnKemCombo_Click);
            // 
            // btnKemLy
            // 
            this.btnKemLy.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(112)))), ((int)(((byte)(195)))), ((int)(((byte)(245)))));
            this.btnKemLy.FlatAppearance.BorderSize = 0;
            this.btnKemLy.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnKemLy.Font = new System.Drawing.Font("Segoe UI", 10.2F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnKemLy.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(237)))), ((int)(((byte)(32)))), ((int)(((byte)(75)))));
            this.btnKemLy.Location = new System.Drawing.Point(250, 0);
            this.btnKemLy.Name = "btnKemLy";
            this.btnKemLy.Size = new System.Drawing.Size(250, 40);
            this.btnKemLy.TabIndex = 12;
            this.btnKemLy.Text = "Kem ly - Kem dĩa";
            this.btnKemLy.UseVisualStyleBackColor = false;
            this.btnKemLy.Click += new System.EventHandler(this.btnKemLy_Click);
            // 
            // btnKemCay
            // 
            this.btnKemCay.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(112)))), ((int)(((byte)(195)))), ((int)(((byte)(245)))));
            this.btnKemCay.FlatAppearance.BorderSize = 0;
            this.btnKemCay.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnKemCay.Font = new System.Drawing.Font("Segoe UI", 10.2F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnKemCay.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(237)))), ((int)(((byte)(32)))), ((int)(((byte)(75)))));
            this.btnKemCay.Location = new System.Drawing.Point(0, 0);
            this.btnKemCay.Name = "btnKemCay";
            this.btnKemCay.Size = new System.Drawing.Size(250, 40);
            this.btnKemCay.TabIndex = 13;
            this.btnKemCay.Text = "Kem cây";
            this.btnKemCay.UseVisualStyleBackColor = false;
            this.btnKemCay.Click += new System.EventHandler(this.btnKemCay_Click);
            // 
            // panel1
            // 
            this.panel1.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(237)))), ((int)(((byte)(32)))), ((int)(((byte)(75)))));
            this.panel1.Location = new System.Drawing.Point(0, 35);
            this.panel1.Name = "panel1";
            this.panel1.Size = new System.Drawing.Size(250, 5);
            this.panel1.TabIndex = 18;
            this.panel1.Visible = false;
            // 
            // thucDon_BingsuUC1
            // 
            this.thucDon_BingsuUC1.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.thucDon_BingsuUC1.Font = new System.Drawing.Font("Segoe UI", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.thucDon_BingsuUC1.Location = new System.Drawing.Point(24, 40);
            this.thucDon_BingsuUC1.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.thucDon_BingsuUC1.Name = "thucDon_BingsuUC1";
            this.thucDon_BingsuUC1.Size = new System.Drawing.Size(951, 421);
            this.thucDon_BingsuUC1.TabIndex = 17;
            this.thucDon_BingsuUC1.Visible = false;
            // 
            // thucDon_KemComboUC1
            // 
            this.thucDon_KemComboUC1.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.thucDon_KemComboUC1.Font = new System.Drawing.Font("Segoe UI", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.thucDon_KemComboUC1.Location = new System.Drawing.Point(24, 40);
            this.thucDon_KemComboUC1.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.thucDon_KemComboUC1.Name = "thucDon_KemComboUC1";
            this.thucDon_KemComboUC1.Size = new System.Drawing.Size(951, 421);
            this.thucDon_KemComboUC1.TabIndex = 16;
            this.thucDon_KemComboUC1.Visible = false;
            // 
            // thucDon_KemLyUC1
            // 
            this.thucDon_KemLyUC1.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.thucDon_KemLyUC1.Font = new System.Drawing.Font("Segoe UI", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.thucDon_KemLyUC1.Location = new System.Drawing.Point(24, 40);
            this.thucDon_KemLyUC1.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.thucDon_KemLyUC1.Name = "thucDon_KemLyUC1";
            this.thucDon_KemLyUC1.Size = new System.Drawing.Size(951, 421);
            this.thucDon_KemLyUC1.TabIndex = 15;
            this.thucDon_KemLyUC1.Visible = false;
            // 
            // thucDon_KemCayUC1
            // 
            this.thucDon_KemCayUC1.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.thucDon_KemCayUC1.Font = new System.Drawing.Font("Segoe UI", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.thucDon_KemCayUC1.Location = new System.Drawing.Point(24, 40);
            this.thucDon_KemCayUC1.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.thucDon_KemCayUC1.Name = "thucDon_KemCayUC1";
            this.thucDon_KemCayUC1.Size = new System.Drawing.Size(951, 421);
            this.thucDon_KemCayUC1.TabIndex = 14;
            this.thucDon_KemCayUC1.Visible = false;
            // 
            // ThucDonUC
            // 
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Inherit;
            this.BackColor = System.Drawing.Color.Transparent;
            this.Controls.Add(this.panel1);
            this.Controls.Add(this.thucDon_BingsuUC1);
            this.Controls.Add(this.thucDon_KemComboUC1);
            this.Controls.Add(this.thucDon_KemLyUC1);
            this.Controls.Add(this.thucDon_KemCayUC1);
            this.Controls.Add(this.btnBingsu);
            this.Controls.Add(this.btnKemCombo);
            this.Controls.Add(this.btnKemLy);
            this.Controls.Add(this.btnKemCay);
            this.Font = new System.Drawing.Font("Segoe UI", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.Name = "ThucDonUC";
            this.Size = new System.Drawing.Size(1000, 461);
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.Button btnBingsu;
        private System.Windows.Forms.Button btnKemCombo;
        private System.Windows.Forms.Button btnKemLy;
        private System.Windows.Forms.Button btnKemCay;
        private ThucDon_KemCayUC thucDon_KemCayUC1;
        private ThucDon_KemLyUC thucDon_KemLyUC1;
        private ThucDon_KemComboUC thucDon_KemComboUC1;
        private ThucDon_BingsuUC thucDon_BingsuUC1;
        private System.Windows.Forms.Panel panel1;

    }
}
