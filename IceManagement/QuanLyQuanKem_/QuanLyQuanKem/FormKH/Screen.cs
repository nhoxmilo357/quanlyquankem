﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace QuanLyQuanKem.FormKH
{
    public partial class Screen : Form
    {
        public Screen()
        {
            InitializeComponent();
        }

        //Button xem chỗ ngồi
        private void winUIbtnXemChoNgoi_Click(object sender, EventArgs e)
        {
            datBanUC1.Visible = true;
            datBanUC1.BringToFront();
        }

        //Button xem thực đơn
        private void winUIbtnXemThucDon_Click(object sender, EventArgs e)
        {
            thucDonUC1.Visible = true;
            thucDonUC1.BringToFront();
        }

        #region Di chuyển form
        Boolean flag; int x, y;
        private void Screen_MouseDown(object sender, MouseEventArgs e)
        {
            flag = true;
            x = e.X;
            y = e.Y;
        }

        private void Screen_MouseUp(object sender, MouseEventArgs e)
        {
            flag = false;
        }

        private void Screen_MouseMove(object sender, MouseEventArgs e)
        {
            if (flag == true)
                this.SetDesktopLocation(Cursor.Position.X - x, Cursor.Position.Y - y);
        }
        #endregion
        private void Screen_Load(object sender, EventArgs e)
        {

        }

        //Picture click
        private void pictureBox1_Click(object sender, EventArgs e)
        {
            this.Close();
        }
    }
}
