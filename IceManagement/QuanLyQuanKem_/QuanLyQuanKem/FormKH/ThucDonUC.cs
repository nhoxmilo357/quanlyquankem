﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace QuanLyQuanKem.FormKH
{
    public partial class ThucDonUC : UserControl
    {
        public ThucDonUC()
        {
            InitializeComponent();
            btnKemCay.Size = new Size(384, 40);
            btnKemLy.Size = new Size(384, 40);
            btnKemLy.Location = new Point(384, 0);
            btnKemCombo.Size = new Size(384, 40);
            btnKemCombo.Location = new Point(768, 0);
            btnBingsu.Size = new Size(384, 40);
            btnBingsu.Location = new Point(1152, 0);
            panel1.Size = new Size(384, 5);

        }

        private void btnKemCay_Click(object sender, EventArgs e)
        {
            thucDon_KemCayUC1.Visible = true;
            thucDon_KemCayUC1.BringToFront();
            panel1.Visible = true;
            panel1.Location = new Point(0, 35);
        }

        private void btnKemLy_Click(object sender, EventArgs e)
        {
            thucDon_KemLyUC1.Visible = true;
            thucDon_KemLyUC1.BringToFront();
            panel1.Visible = true;
            panel1.Location = new Point(384, 35);
        }

        private void btnKemCombo_Click(object sender, EventArgs e)
        {
            thucDon_KemComboUC1.Visible = true;
            thucDon_KemComboUC1.BringToFront();
            panel1.Visible = true;
            panel1.Location = new Point(768, 35);
        }

        private void btnBingsu_Click(object sender, EventArgs e)
        {
            thucDon_BingsuUC1.Visible = true;
            thucDon_BingsuUC1.BringToFront();
            panel1.Visible = true;
            panel1.Location = new Point(1152, 35);
        }

    }
}
